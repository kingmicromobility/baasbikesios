//
//  Trip.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Session.h"
#import "RouteMapPolyline.h"

@interface Trip : NSObject
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) NSUUID *sessionGuid;
@property (nonatomic, strong) Session *session;
@property (nonatomic, strong) NSUUID *startedEvent;
@property (nonatomic, strong) NSUUID *endedEvent;
@property (nonatomic, strong) NSMutableArray *route;
@property (nonatomic, strong) RouteMapPolyline *routeMapPolyline;

- (instancetype)initWithDictionary:(NSDictionary *)tripDictionary;

- (instancetype)initWithDictionary:(NSDictionary *)tripDictionary
                        andSession:(Session *)Session;

- (instancetype)initWithSession:(Session *)session;

- (BOOL)isInProgress;

- (void)startTracking;

- (void)stopTracking;

- (void)addToMapView:(GMSMapView *)mapView;

@end
