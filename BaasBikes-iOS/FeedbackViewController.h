//
//  FeedbackViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "FeedbackTableViewController.h"

@interface FeedbackViewController : BaseViewController

- (IBAction)didPressBackButton:(id)sender;

- (IBAction)didPressSubmit:(id)sender;

@property int rating;

@property (weak, nonatomic) FeedbackTableViewController *ftv;

- (void)sendFeedbackWithRating:(int)star_rating details:(NSArray *)detailSet rideableFlag:(BOOL)rideable;

@end
