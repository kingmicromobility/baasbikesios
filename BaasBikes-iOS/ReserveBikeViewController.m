//
//  ReserveBikeViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "ReserveBikeViewController.h"
#import "MapViewController.h"
#import "Constants.h"
#import "Mixpanel.h"
#import "InstructionsViewController.h"

@interface ReserveBikeViewController ()

@end

@implementation ReserveBikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didPressCancel:(id)sender {
    [self dismissSelfAndClearRentalState];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [bike deselect];
    [[Mixpanel sharedInstance] track:@"bike.selection.canceled" properties:[bike propertiesAsDictionary]];
}

- (IBAction)didPressReserve:(id)sender {
    [self startLoadingWithMessage:@"Reserving bike"];
    [[NSNotificationCenter defaultCenter] addObserverForName:USER_LOGGED_IN_NOTIFICATION
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification){
                                                      [self stopLoading];
                                                      }];
    [[NSNotificationCenter defaultCenter] addObserverForName:USER_NEEDS_AUTH_NOTIFICATION
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification){
                                                      [self stopLoading];
                                                  }];
    ApplicationState *appState = [ApplicationState sharedInstance];
    [appState.currentBike reserveWithSuccess:^(Reservation *reservation) {
        appState.currentReservation = reservation;
        [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_BIKE_RESERVED_VC];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self stopLoading];
        [[Mixpanel sharedInstance] track:@"reservation.started" properties:[reservation propertiesAsDictionary]];
        [[Mixpanel sharedInstance] timeEvent:@"session.started"];
        [[Mixpanel sharedInstance].people increment:@{@"reservation count": @1}];

        if ([[[NSUserDefaults standardUserDefaults] objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]) {
            [self showInstructions];
        }
    } andFailure:^(NSError *error) {
        MainViewController *mainVC = (MainViewController *)self.navigationController.parentViewController;
        if ([error.localizedDescription isEqualToString:@"Request failed: payment required (402)"]) {
            [mainVC showAlertWithTitle:@"Check Payment Method"
                        andDescription:@"We had trouble verifying your credit card. Please add a new one in your account settings."];
        } else if (![appState.currentBike.status isEqualToString:@"available"]) {
            [mainVC showAlertWithTitle:@"Bike Not Available"
                        andDescription:@"Snap! While you've been looking at this bike, someone else reserved it."];
        } else {
            [mainVC showAlertWithTitle:@"Something Went Wrong"
                        andDescription:@"We were unable to reserve that bike for you. Please try again in a few moments."];
        }
        NSLog(@"Error beginning reservation %@", error);
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self dismissSelfAndClearRentalState];
        [self stopLoading];
        [[Mixpanel sharedInstance] track:@"reservation.bikeNoLongerAvailable" properties:[appState.currentBike propertiesAsDictionary]];
        [appState clearRentalState];
    }];
}

- (void)showInstructions {
    Bike *bike = [ApplicationState sharedInstance].currentReservation.bike;
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"You’ve reserved this bike for 15 minutes. Go unlock it!"
                    buttonText:@"CONTINUE"
               backgroundImage:[UIImage imageNamed:@"instructions_after_reserve"]
                     backBlock:nil
                   actionBlock:[self instructionsActionBlock]];
    [self.navigationController.parentViewController presentViewController:instructionsVC
                                                                 animated:YES
                                                               completion:^{
                                                                   [[Mixpanel sharedInstance] track:@"reservation.instructions.viewed"];
                                                               }];
}

- (void (^)())instructionsActionBlock {
    return ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    };
}

@end
