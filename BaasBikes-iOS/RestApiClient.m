//
//  RestApiClient.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RestApiClient.h"
#import "Constants.h"
#import "Environment.h"

@implementation RestApiClient

# pragma mark - Lifecycle

+ (instancetype)sharedInstance {
    static RestApiClient *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[RestApiClient alloc] init];
    });

    return this;
}

# pragma mark - Public Methods

- (void)list:(NSString *)uri
      params:(NSDictionary *)params
     success:(void(^)(NSDictionary *))successBlock
       error:(void(^)(NSError *))errorBlock {

    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager GET:[self urlFromUri:uri]
      parameters:params
         success:[self listSuccess:successBlock]
         failure:[self generalFailure:errorBlock]];
}

- (void (^)(AFHTTPRequestOperation *op, id resp))listSuccess:(void(^)(NSDictionary *))successBlock {
    __block void(^success)(NSDictionary *) = successBlock;
    return ^void(AFHTTPRequestOperation *op, id resp) { success(resp); };
}



- (void)show:(NSString *)uri
        uuid:(NSUUID *)uuid
      params:(NSDictionary *)params
     success:(void(^)(NSDictionary *))successBlock
       error:(void(^)(NSError *))errorBlock {

    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager GET:[self urlFromUri:uri withUUID:uuid]
      parameters:params
         success:[self showSuccess:successBlock]
         failure:[self generalFailure:errorBlock]];
}

- (void (^)(AFHTTPRequestOperation *op, id resp))showSuccess:(void(^)(NSDictionary *))successBlock {
    __block void(^success)(NSDictionary *) = successBlock;
    return ^void(AFHTTPRequestOperation *op, id resp) { success(resp); };
}



- (void)create:(NSString *)uri
        params:(NSDictionary *)params
       success:(void(^)(NSDictionary *))successBlock
         error:(void(^)(NSError *))errorBlock {

    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager POST:[self urlFromUri:uri]
       parameters:params
          success:[self createSuccess:successBlock]
          failure:[self generalFailure:errorBlock]];
}

- (void (^)(AFHTTPRequestOperation *op, id resp))createSuccess:(void(^)(NSDictionary *))successBlock {
    __block void(^success)(NSDictionary *) = successBlock;
    return ^void(AFHTTPRequestOperation *op, id resp) { success(resp); };
}



- (void)update:(NSString *)uri
          uuid:(NSUUID *)uuid
        params:(NSDictionary *)params
       success:(void(^)(NSDictionary *))successBlock
         error:(void(^)(NSError *))errorBlock {

    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager PATCH:[self urlFromUri:uri withUUID:uuid]
        parameters:params
           success:[self updateSuccess:successBlock]
           failure:[self generalFailure:errorBlock]];
}

- (void (^)(AFHTTPRequestOperation *op, id resp))updateSuccess:(void(^)(NSDictionary *))successBlock {
    __block void(^success)(NSDictionary *) = successBlock;
    return ^void(AFHTTPRequestOperation *op, id resp) { success(resp); };
}



- (AFHTTPRequestOperationManager *)getRequestManager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *jwtToken = [userDefaults objectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    if (jwtToken) {
        [manager.requestSerializer setValue:jwtToken
                         forHTTPHeaderField:AUTH_TOKEN_HTTP_HEADER_KEY];
    }
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    return manager;
}

- (void (^)(AFHTTPRequestOperation *op, NSError *error))generalFailure:(void(^)(NSError *))failureBlock {
    __block void(^failure)(NSError *) = failureBlock;
    return ^void(AFHTTPRequestOperation *op, NSError *error) {
        if ([self isAuthenticationError:op.response.statusCode]) {
            NSLog(@"[RestApiClient] User Needs to authenticate. Issuing auth notification");
            NSLog(@"[RestApiClient] Request was: %@", op.request.URL);
        } else {
            NSLog(@"Error: %@", error);
            NSLog(@"%@", op.responseString);
            NSLog(@"%@", op.responseObject);
            failure(error);
        }
    };
}


# pragma mark - Private Methods

- (NSString *)urlFromUri:(NSString *)uri {
    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *api_url = config[@"BAAS_API_URL"];
    return [NSString stringWithFormat:@"%@%@", api_url, uri];
}

- (NSString *)urlFromUri:(NSString *)uri withUUID:(NSUUID *)uuid {
    NSString *base = [self urlFromUri:uri];
    return [NSString stringWithFormat:@"%@%@/", base, uuid.UUIDString.lowercaseString];
}

- (BOOL)isAuthenticationError:(NSInteger)statusCode {
    if (statusCode == 403) {
        [[NSNotificationCenter defaultCenter] postNotificationName:USER_NEEDS_AUTH_NOTIFICATION object:nil];
        return YES;
    } else {
        return NO;
    }
}

@end
