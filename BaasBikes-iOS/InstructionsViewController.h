//
//  LockBikeViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RentalViewController.h"

@interface InstructionsViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *caption;
@property (strong, nonatomic) UIView *loadingView;

@property RentalViewController *rentalVC;

- (void) setCaption:(NSString *)captionText
         buttonText:(NSString *)buttonText
    backgroundImage:(UIImage *)backgroundImage
          backBlock:(void (^)(void))backBlock
        actionBlock:(void (^)(void))actionBlock;

- (IBAction)didPressBack:(id)sender;

- (IBAction)didPressButton:(id)sender;

- (void)startLoading;

- (void)stopLoading;

@end
