//
//  MainViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import "RentalFlowNavigationController.h"

@protocol MenuDelegate <NSObject>
- (void)setOffset:(float)offset;
@end

@interface MainViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *priceButton;

@property BOOL rentalViewIsShowing;
@property BOOL reportIsActive;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapContainerBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *mapContainerView;
@property (strong, nonatomic) RentalFlowNavigationController *rentalVC;

@property (weak, nonatomic) id<MenuDelegate> menuDelegate;

- (void)showRentalFlowNavigationVC;
- (void)hideRentalFlowNavigationVC;

@end
