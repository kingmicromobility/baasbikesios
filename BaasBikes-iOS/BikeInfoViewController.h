//
//  BikeInfoViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/2/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BikeInfoViewController : UIViewController

- (IBAction)didPressBack:(id)sender;
- (IBAction)didPressMorePurchaseDetails:(id)sender;

@end
