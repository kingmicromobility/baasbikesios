//
//  Environment.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/4/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Environment : NSObject

+ (instancetype) sharedInstance;

- (NSDictionary *) configuration;

@end
