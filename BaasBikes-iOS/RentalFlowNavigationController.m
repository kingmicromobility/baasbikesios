//
//  RentalFlowNavigationController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RentalFlowNavigationController.h"
#import "Constants.h"

@interface RentalFlowNavigationController ()

@end

@implementation RentalFlowNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addRootViewController];
    [self setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_REVIEW_BIKE_VC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addRootViewController {
    UIViewController *reserveBikeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReserveBikeVC"];
    [self setViewControllers:@[reserveBikeVC]];
}

- (void)startLoading {
    [self startLoadingWithMessage:@"Loading"];
}

- (void)startLoadingWithMessage:(NSString *)message {
    self.activityIndicator = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityIndicatorVC"];
    [self.activityIndicator setMessage:message];
    [self.activityIndicator addToView:self.view];
}

- (void)updateLoadingMessage:(NSString *)message {
    [self.activityIndicator setMessage:message];
}

- (void)stopLoading{
    [self.activityIndicator removeFromView];
}

- (void)pushRentalFlowVCForState:(RENTAL_FLOW_STATE)state {
    
    UIViewController *rentalVCForState;
    
    switch (state) {
        case kRENTAL_FLOW_STATE_RESERVE_BIKE_VC:
            rentalVCForState = [self.storyboard instantiateViewControllerWithIdentifier:@"ReserveBikeVC"];
            break;
        case kRENTAL_FLOW_STATE_BIKE_RESERVED_VC:
            rentalVCForState = [self.storyboard instantiateViewControllerWithIdentifier:@"BikeReservedVC"];
            break;
        case kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC:
            rentalVCForState = [self.storyboard instantiateViewControllerWithIdentifier:@"TripInProgressVC"];
            break;
        case kRENTAL_FLOW_STATE_HOLD_VC:
            rentalVCForState = [self.storyboard instantiateViewControllerWithIdentifier:@"HoldVC"];
            break;
        case kRENTAL_FLOW_STATE_REVIEW_BIKE_VC:
            rentalVCForState = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewBikeVC"];
            break;
            
        default:
            break;
    }
    [self pushViewController:rentalVCForState animated:false];
}

@end
