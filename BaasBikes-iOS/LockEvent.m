//
//  LockEvent.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Bike.h"
#import "Trip.h"
#import "LockEvent.h"
#import "RestApiClient.h"
#import "LocationManager.h"
#import "NSString+LocationHelper.h"

#define URI @"lock_events/"

@implementation LockEvent

+ (LockEvent *)getByUUID:(NSUUID *)uuid {
    return nil;
}

+ (void)unlockBike:(Bike *)bike
       withSuccess:(void (^)(LockEvent *))successBlock
        andFailure:(void (^)(NSError *))failureBlock {
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    LockEvent *unlockEvent = [[LockEvent alloc] initWithType:@"unlocked"
                                                        bike:bike
                                                 andlocation:location];
    [unlockEvent saveWithSuccess:successBlock
                      andFailure:failureBlock];
}

+ (void)lockBike:(Bike *)bike
     withSuccess:(void (^)(LockEvent *))successBlock
      andFailure:(void (^)(NSError *))failureBlock {
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    LockEvent *lockEvent = [[LockEvent alloc] initWithType:@"locked"
                                                      bike:bike
                                               andlocation:location];
    [lockEvent saveWithSuccess:successBlock
                    andFailure:failureBlock];
}

- (instancetype)initWithType:(NSString *)type
                        bike:(Bike *)bike
                 andlocation:(CLLocation *)location {
    self.type = type;
    self.bike = bike;
    self.location = location;
    return self;
}

- (void)saveWithSuccess:(void(^)(LockEvent *))successBlock
             andFailure:(void(^)(NSError *))failureBlock {
    NSDictionary *params = @{
                             @"location":[NSString makeStringFromCoordinate:self.location.coordinate],
                             @"bike":self.bike.guid.UUIDString.lowercaseString,
                             @"event_type":self.type
                             };

    [[RestApiClient sharedInstance] create:URI
                                    params:params
                                   success:[self onSaveSuccess:successBlock]
                                     error:failureBlock];
}

- (void (^)(NSDictionary *))onSaveSuccess:(void(^)(LockEvent *))block {
    return ^void(NSDictionary *lockEventDict) {
        if ([lockEventDict objectForKey:@"guid"] != [NSNull null]) {
            self.guid = lockEventDict[@"guid"];
        }
        self.bike.lastLocation = self.location;
        self.trip = [[Trip alloc] initWithDictionary:lockEventDict[@"trip"]];
        block(self);
    };
}

@end
