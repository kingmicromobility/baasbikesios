//
//  NSString+PennyHelper.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "NSString+PennyHelper.h"

@implementation NSString (PennyHelper)
+ (NSString*)formatAmount:(int) amount {
    int cents = amount % 100;
    int dollars = (amount - cents) / 100;
    
    NSString *camount;
    if (cents <=9)
    {
        camount = [NSString stringWithFormat:@"0%d",cents];
    }
    else
    {
        camount = [NSString stringWithFormat:@"%d",cents];
    }
    NSString *t = [NSString stringWithFormat:@"$%d.%@",dollars,camount];
    
    return t;
}
@end
