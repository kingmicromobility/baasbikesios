//
//  BikeInfoViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/2/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "BikeInfoViewController.h"
#import "ApplicationState.h"
#import "Mixpanel.h"
#import "Bike.h"

@implementation BikeInfoViewController

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden: YES animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.details.viewed" properties:[bike propertiesAsDictionary]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didPressBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.details.back" properties:[bike propertiesAsDictionary]];
}

- (IBAction)didPressMorePurchaseDetails:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmBikePurchaseViewController"];
    [self.navigationController pushViewController:vc animated:true];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.details.morePurchaseDetails" properties:[bike propertiesAsDictionary]];
}
@end
