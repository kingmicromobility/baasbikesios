//
//  RideHistoryTableViewCell.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideHistoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeDifferenceLabel;

@end
