//
//  RouteMapPolyline.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 8/3/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "RouteMapPolyline.h"
#import "ImageCache.h"
#import "Bike.h"
#import "Constants.h"


@interface RouteMapPolyline()
@property (nonatomic, strong) GMSMapView *assignedMap;
@end

@implementation RouteMapPolyline

- (id)initWithTrip:(Trip *)trip {
    self = [super init];
    self.trip = trip;
    self.strokeColor = [[UIColor alloc] initWithRed:0.0f green:0.5f blue:1.0f alpha:0.75f];
    self.strokeWidth = 12;
    [self refresh];
    return self;
}

- (void)assignToMap:(GMSMapView *)mapView {
    self.assignedMap = mapView;
    [self refresh];
}

- (void)refresh {
    if ([self.trip isInProgress]) [self display];
    else                          [self hide];
}

- (void)display {
    GMSMutablePath *path = [GMSMutablePath path];
    for (CLLocation *location in self.trip.route) {
        [path addCoordinate:location.coordinate];
    }
    self.path = path;
    [self show];
}

- (void) hide {
    self.map = nil;
}

- (void) show {
    self.map = self.assignedMap;
}

@end
