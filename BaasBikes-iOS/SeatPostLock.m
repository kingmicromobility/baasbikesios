//
//  SeatPostLock.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 2/22/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "SeatPostLock.h"
#import "AnswerCode.h"

@interface SeatPostLock()
@property (strong, nonatomic) NSString *baasLockId;
@property (strong, nonatomic) BluetoothCentralManager *bluetoothCentralManager;
@property (strong, nonatomic) CBPeripheral *lockPeripheral;
@property (strong, nonatomic) NSNumber *rawChallengeCode;
@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL compatible;
@property (nonatomic) BOOL commandPending;
@end

@implementation SeatPostLock

#pragma mark - Lifecycle

- (instancetype) initWithBaasLockId:(NSString *)lockId
                             andUrn:(NSString *)urn
                       withDelegate:(id<LockDelegate>)delegate {
    self.unlockMessageTitle = SEAT_POST_UNLOCK_MESSAGE_TITLE;
    self.unlockMessageBody = SEAT_POST_UNLOCK_MESSAGE_BODY;
    self.baasLockId = lockId;
    self.urn = urn;
    self.delegate = delegate;
    self.type = @"seat-post";
    self.bluetoothCentralManager = [[BluetoothCentralManager alloc] initWithDelegate:self];
    return self;
}


#pragma mark - Public

- (void) connect {
    NSLog(@"[SeatPostLock] Searching for lock %@", self.baasLockId);
    [self.bluetoothCentralManager beginSearchingForLockPeripheral:self.baasLockId
                                                      withService:BAAS_SEAT_LOCK_SERVICE_UUID];
}

- (BOOL) isConnected {
    return self.connected;
}

- (void) disconnect {
    NSLog(@"[SeatPostLock] Disconnecting from bluetooth peripheral");
    if (self.lockPeripheral) {
        [self.bluetoothCentralManager disconnectLockPeripheral:self.lockPeripheral];
    } else {
        NSLog(@"[SeatPostLock] No peripheral to disconnect from");
    }
}

- (void) confirmCompatibility {
    NSLog(@"[SeatPostLock] Confirming compatibility");
    if (self.lockPeripheral) {
        [self.lockPeripheral discoverServices:nil];
    } else {
        self.compatible = NO;
    }
}

- (BOOL) isCompatible {
    return self.compatible;
}

- (void) unlock {
    self.desiredState = @"unlocked";
    [self withPeripheralConnection:^{
        [self withAnswerCode:^(NSNumber *answerCode){
            NSLog(@"[SeatPostLock] Issuing unlock command.");
            short answerCodeShort = [answerCode shortValue];
            NSLog(@"Answer Code is: %i", answerCodeShort);
            NSData *answerCodeData = [NSData dataWithBytes:&answerCodeShort length:sizeof(answerCodeShort)];
            [self.lockPeripheral writeValue:answerCodeData
                          forCharacteristic:self.unlockCommand
                                       type:CBCharacteristicWriteWithResponse];
        }];
    }];
}

- (void) lock {
    self.desiredState = @"locked";
    [self withPeripheralConnection:^{
        [self withAnswerCode:^(NSNumber *answerCode){
            NSLog(@"[SeatPostLock] Issuing lock command.");
            short answerCodeShort = [answerCode shortValue];
            NSLog(@"Answer Code is: %i", answerCodeShort);
            NSData *answerCodeData = [NSData dataWithBytes:&answerCodeShort length:sizeof(answerCodeShort)];
            [self.lockPeripheral writeValue:answerCodeData
                          forCharacteristic:self.lockCommand
                                       type:CBCharacteristicWriteWithResponse];
        }];
    }];
}

- (BOOL) isLocked {
    return [self.state isEqualToString:@"locked"];
}

#pragma mark - BluetoothCentralManagerDelegate

- (void) didFindLockPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"[SeatPostLock] Found Lock peripheral. Connecting...");
    self.lockPeripheral = peripheral;
    self.lockPeripheral.delegate = self;
    [self.bluetoothCentralManager connectLockPeripheral:self.lockPeripheral];
}

- (void) didConnectLockPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"[SeatPostLock] Connected to lock peripheral!");
    self.lockPeripheral = peripheral;
    self.lockPeripheral.delegate = self;
    self.connected = YES;
    if ([self.delegate respondsToSelector:@selector(didConnect:)]) {
        [self.delegate didConnect:self];
    }
    [self confirmCompatibility];
}

- (void) didDisconnectLockPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"[SeatPostLock] Disconnected from lock peripheral.");
    self.lockPeripheral = nil;
    self.connected = NO;
    if ([self.delegate respondsToSelector:@selector(didDisconnect:)]) {
        [self.delegate didDisconnect:self];
    }
}


#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverServices:(nullable NSError *)error {
    for (CBService *service in peripheral.services) {
        NSLog(@"[SeatPostLock] Discovered service %@", service);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(nonnull CBService *)service
             error:(nullable NSError *)error {
    NSArray *characteristics = [service characteristics];
    BOOL unlockDiscovered = NO;
    BOOL lockDiscovered = NO;
    BOOL challengeDiscovered = NO;
    BOOL stateDiscovered = NO;

    if (peripheral != self.lockPeripheral) {
        NSLog(@"[SeatPostLock] Wrong Peripheral.\n");
        return;
    }
    if (error != nil) {
        NSLog(@"[SeatPostLock] Error %@\n", error);
        return;
    }
    for (CBCharacteristic *characteristic in characteristics) {
        NSLog(@"[SeatPostLock] Found characteristic: %@", [characteristic UUID]);
        if ([[characteristic UUID] isEqual:BAAS_UNLOCK_COMMAND_CHAR_UUID]) {
            self.unlockCommand = characteristic;
            unlockDiscovered = YES;
        }
        if ([[characteristic UUID] isEqual:BAAS_LOCK_COMMAND_CHAR_UUID]) {
            self.lockCommand = characteristic;
            lockDiscovered = YES;
        }
        if ([[characteristic UUID] isEqual:BAAS_CHALLENGE_CODE_CHAR_UUID]) {
            self.challengeCode = characteristic;
            challengeDiscovered = YES;
        }
        if ([[characteristic UUID] isEqual:BAAS_LOCK_STATE_CHAR_UUID]) {
            self.lockState = characteristic;
            stateDiscovered = YES;
            [self.lockPeripheral setNotifyValue:YES
                              forCharacteristic:self.lockState];
        }
    }
    if (unlockDiscovered && lockDiscovered &&
        challengeDiscovered && stateDiscovered) {
        self.compatible = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didConfirmCompatibility"
                                                            object:nil
                                                          userInfo:nil];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic
             error:(nullable NSError *)error {
    NSLog(@"[SeatPostLock] Updated Characteristic Value: %@", characteristic.value);
    if (characteristic == self.lockState) {
        [self setStateFromCharacteristicValue:characteristic.value];
        [self invokeDelegateAfterAttemptedStateChange];
    } else if (characteristic == self.challengeCode) {
        [self setRawChallengeCodeFromCharacteristicValue:characteristic.value];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didUpdateChallengeCode"
                                                            object:nil
                                                          userInfo:nil];
    }
}


#pragma mark - Private

- (void)setStateFromCharacteristicValue:(NSData *)value {
    self.state = [[NSString alloc] initWithData:value
                                       encoding:NSUTF8StringEncoding];
}

- (void)setRawChallengeCodeFromCharacteristicValue:(NSData *)value {
    short decodedInteger;
    [value getBytes:&decodedInteger length:2];
    self.rawChallengeCode = @(decodedInteger);
    NSLog(@"Challenge Code data is: %@", self.rawChallengeCode);
}

- (void)invokeDelegateAfterAttemptedStateChange {
    NSLog(@"Lock says '%@'. We want '%@'.", self.state, self.desiredState);
    if ([self.state isEqualToString:self.desiredState]) {
        if ([self isLocked]) {
            if ([self.delegate respondsToSelector:@selector(didLock:)]) {
                [self.delegate didLock:self];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(didUnlock:)]) {
                [self.delegate didUnlock:self];
            }
        }
    } else {
        if ([self.desiredState isEqualToString:@"locked"]) {
            if ([self.delegate respondsToSelector:@selector(failedToLock:)]) {
                [self.delegate failedToLock:self];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(failedToUnlock:)]) {
                [self.delegate failedToUnlock:self];
            }
        }
    }
    [self disconnect];
}

- (void)withPeripheralConnection:(void (^)(void))completionBlock {
    __block NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    if (![self isConnected]) {
        __block __weak id successObserver;
        __block __weak id failureObserver;
        NSTimer *connectionTimer = [NSTimer scheduledTimerWithTimeInterval:15
                                                                    target:self
                                                                  selector:@selector(connectionTimedOut)
                                                                  userInfo:nil
                                                                   repeats:NO];
        successObserver = [notiCenter addObserverForName:@"didConfirmCompatibility"
                                                  object:nil
                                                   queue:[NSOperationQueue mainQueue]
                                              usingBlock:^(NSNotification *notification){
                                                  completionBlock();
                                                  [connectionTimer invalidate];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
                                              }];
        failureObserver = [notiCenter addObserverForName:@"didFailToConnect"
                                                  object:nil
                                                   queue:[NSOperationQueue mainQueue]
                                              usingBlock:^(NSNotification *notification){
                                                  [connectionTimer invalidate];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
                                              }];
        [self connect];
        if ([self.delegate respondsToSelector:@selector(didBeginConnecting:)]) {
            [self.delegate didBeginConnecting:self];
        }
    } else {
        completionBlock();
    }
}

- (void)withAnswerCode:(void (^)(NSNumber* answerCode))answerCodeBlock {
    __block NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    __block __weak id successObserver;
    successObserver = [notiCenter addObserverForName:@"didUpdateChallengeCode"
                                              object:nil
                                               queue:[NSOperationQueue mainQueue]
                                          usingBlock:^(NSNotification *notification){
                                              AnswerCode *answer = [[AnswerCode alloc] initWithChallengeCode:self.rawChallengeCode
                                                                                                     andLock:self];
                                              [answer getAnswerWithSuccess:answerCodeBlock andFailure:nil];
                                              [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
                                          }];
    [self.lockPeripheral readValueForCharacteristic:self.challengeCode];
}

- (void)connectionTimedOut {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFailToConnect" object:nil];
    if ([self.delegate respondsToSelector:@selector(failedToConnect:)]) {
        [self.delegate failedToConnect:self];
    }
}

@end
