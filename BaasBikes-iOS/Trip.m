//
//  Trip.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Bike.h"
#import "Trip.h"
#import "RestApiClient.h"
#import "LocationManager.h"
#import "ApplicationState.h"

#define URI @"trips/"

@implementation Trip

- (instancetype)initWithDictionary:(NSDictionary *)tripDictionary {
    if ([tripDictionary objectForKey:@"guid"] != [NSNull null]) {
        self.guid = [[NSUUID alloc] initWithUUIDString:tripDictionary[@"guid"]];
    }
    if ([tripDictionary objectForKey:@"session"] != [NSNull null]) {
        self.sessionGuid = [[NSUUID alloc] initWithUUIDString:tripDictionary[@"session"]];
    }
    if ([tripDictionary objectForKey:@"started_event"] != [NSNull null]) {
        self.startedEvent = tripDictionary[@"started_event"];
    }
    if ([tripDictionary objectForKey:@"ended_event"] != [NSNull null]) {
        self.endedEvent = tripDictionary[@"ended_event"];
    }
    if ([tripDictionary objectForKey:@"route"] != [NSNull null]) {
        self.route = [[NSMutableArray alloc] initWithArray:[self deserializeRoute:tripDictionary[@"route"]]];
    }
    self.routeMapPolyline = [[RouteMapPolyline alloc] initWithTrip:self];
    if ([self isInProgress]) {
        [self startTracking];
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)tripDictionary
                        andSession:(Session *)session {
    self = [self initWithDictionary:tripDictionary];
    self.session = session;
    return self;
}

- (instancetype)initWithSession:(Session *)session {
    self.session = session;
    return self;
}

- (BOOL)isInProgress {
    return !self.endedEvent;
}

- (void)startTracking {
    [self addToMapView:[ApplicationState sharedInstance].mainMap];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdated) name:@"baas.person.location.updated" object:nil];
}

- (void)stopTracking {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"baas.person.location.updated" object:nil];
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    [self.route addObject:location];
    self.session.bike.lastLocation = location;
    [self.session.bike.marker refreshPosition];
    [self saveWithSuccess:^(Trip *trip) {
                    [self.routeMapPolyline refresh];
                    NSLog(@"Successfully saved trip.");
                }
               andFailure:^(NSError *error) {
                    NSLog(@"Failed to save trip.");
                }];
}

- (void)addToMapView:(GMSMapView *)mapView {
    [self.routeMapPolyline assignToMap:mapView];
}

#pragma mark - private methods

- (void)locationUpdated {
    if (!self.route) { self.route = [[NSMutableArray alloc] init]; }
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitSecond) fromDate:location.timestamp];
    NSInteger second = [components second];

    if (second % 5 == 0) { // the number of seconds is a multiple of 5
        NSLog(@"Trip: %@, Current Location: %f %f %f %f %@", self, location.course, location.coordinate.latitude, location.coordinate.longitude, location.altitude, location.timestamp);
        [self.route addObject:location];
        [self.routeMapPolyline refresh];
    }

    if (second == 0) { // the top of every minute
        NSLog(@"Saving Trip.");
        [self saveWithSuccess:^(Trip *trip) { NSLog(@"Successfully saved trip."); }
                   andFailure:^(NSError *error) { NSLog(@"Failed to save trip."); }];
    }

    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        self.session.bike.lastLocation = location;
        [self.session.bike.marker refreshPosition];
    }
}

- (NSString *)serializeRoute {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
    NSString *serializedRoute = @"";
    for (CLLocation *location in self.route) {
        NSString *routeEntryString = [NSString stringWithFormat:@"%f,%f,%f,%f,%@\n",
                                      location.course,
                                      location.coordinate.latitude,
                                      location.coordinate.longitude,
                                      location.altitude,
                                      [dateFormatter stringFromDate:location.timestamp]];
        serializedRoute = [serializedRoute stringByAppendingString:routeEntryString];
    }
    return serializedRoute;
}

- (NSArray *)deserializeRoute:(NSString *)serializedRoute {
    NSMutableArray *routeArray = [[NSMutableArray alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
    NSString *routeEntryPattern = @"([\\d\\.\\-]+),([\\d\\.\\-]+),([\\d\\.\\-]+),([\\d\\.\\-]+),([\\dT\\:\\.\\-]+)\\\n";
    NSRange rangeToSearch = NSMakeRange(0, [serializedRoute length]);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:routeEntryPattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSArray *routeEntries = [regex matchesInString:serializedRoute options:0 range:rangeToSearch];
    for (NSTextCheckingResult *entry in routeEntries) {
        NSString *course    = [serializedRoute substringWithRange:[entry rangeAtIndex:1]];
        NSString *latitude  = [serializedRoute substringWithRange:[entry rangeAtIndex:2]];
        NSString *longitude = [serializedRoute substringWithRange:[entry rangeAtIndex:3]];
        NSString *altitude  = [serializedRoute substringWithRange:[entry rangeAtIndex:4]];
        NSString *timestamp = [serializedRoute substringWithRange:[entry rangeAtIndex:5]];

        CLLocation *locationEntry = [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake([latitude floatValue], [longitude floatValue])
                                                                  altitude:[altitude floatValue]
                                                        horizontalAccuracy:0.0f
                                                          verticalAccuracy:0.0f
                                                                    course:[course floatValue]
                                                                     speed:0.0f
                                                                 timestamp:[dateFormatter dateFromString:timestamp]];
        [routeArray addObject:locationEntry];
    }
    return routeArray;
}

- (void)saveWithSuccess:(void(^)(Trip *))successBlock
             andFailure:(void(^)(NSError *))failureBlock {
    NSDictionary *params = @{@"route":[self serializeRoute]};
    [[RestApiClient sharedInstance] update:URI
                                      uuid:self.guid
                                    params:params
                                   success:[self onSaveSuccess:successBlock]
                                     error:failureBlock];
}

- (void (^)(NSDictionary *))onSaveSuccess:(void(^)(Trip *))block {
    return ^void(NSDictionary *tripDict) { block(self); };
}


@end
