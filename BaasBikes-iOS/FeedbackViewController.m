//
//  FeedbackViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "FeedbackViewController.h"
#import "LocationManager.h"
#import "PromptSetHelper.h"
#import "Constants.h"
#import "NSString+LocationHelper.h"
#import "Mixpanel.h"
#import "ApplicationState.h"
#import "NetworkingController.h"


@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.feedback" properties:[bike propertiesAsDictionary]];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressedYesOnAlertView {
    NSDictionary *detailSet = [_ftv getDetails];
    [self sendFeedbackWithRating:_rating details:@[detailSet] rideableFlag:YES];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"bike.feedback.isRideable.answered" properties:@{@"feedback_is_rideable": @"yes"}];
}

- (void)pressedNoOnAlertView {
    NSDictionary *detailSet = [_ftv getDetails];
    [self sendFeedbackWithRating:_rating details:@[detailSet] rideableFlag:NO];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"bike.feedback.isRideable.answered" properties:@{@"feedback_is_rideable": @"no"}];
}

- (IBAction)didPressBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didPressSubmit:(id)sender {
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    
    if (_ftv.selectedRow < 0) {
        [self sendFeedbackWithRating:_rating details:nil rideableFlag:YES];
    }else{
        [CustomAlertViewController showWithTitle:@"Help Us Out"
                                         message:@"Is this bike still rideable for the next person?"
                                   yesButtonText:@"Yes"
                                yesButtonHandler:^{[self pressedYesOnAlertView];}
                                    noButtonText:@"No"
                                 noButtonHandler:^{[self pressedNoOnAlertView];}
                                  dismissHandler:nil
                                     contextView:self];
        [[Mixpanel sharedInstance] track:@"bike.feedback.isRideable.viewed" properties:[bike propertiesAsDictionary]];
    }
}

- (void)sendFeedbackWithRating:(int)star_rating details:(NSArray *)detailSet rideableFlag:(BOOL)rideable {
    [self startLoadingWithMessage:@"Submitting feedback"];
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    NSArray *blockSafeDetailSet = detailSet;
    if (!blockSafeDetailSet) {
        blockSafeDetailSet = @[];
    }
    ApplicationState *appState = [ApplicationState sharedInstance];
    NSString *promptSetGuid = [PromptSetHelper getPromptsForSetNamed:SESSION_END_PROMPTS_KEY][@"guid"];
    NSMutableDictionary *feedback_params = [@{} mutableCopy];
    
    [feedback_params setObject:[NSString makeStringFromCoordinate:location.coordinate] forKey:@"location"];
    [feedback_params setObject:appState.currentBike.guid.UUIDString.lowercaseString forKey:@"bike"];
    [feedback_params setObject:promptSetGuid forKey:@"prompts"];
    [feedback_params setObject:@(star_rating) forKey:@"star_rating"];
    [feedback_params setObject:blockSafeDetailSet forKey:@"detail_set"];
    [feedback_params setObject:@(rideable) forKey:@"bike_rideable"];

    [[NetworkingController sharedInstance] postFeedback:feedback_params withSuccess:^{
        NSLog(@"Got it!");
        [self dismissViewControllerAnimated:YES completion:nil];
        [self stopLoading];
        NSMutableDictionary *props = [[appState.currentBike propertiesAsDictionary] mutableCopy];
        [props setObject:@(star_rating) forKey:@"feedback_rating"];
        [props setObject:@(rideable) forKey:@"feedback_is_rideable"];
        [props setObject:promptSetGuid forKey:@"feedback_prompt_set"];
        for (NSDictionary *detail in blockSafeDetailSet) {
            [props setObject:detail[@"value"] forKey:[NSString stringWithFormat:@"feedback_prompt_%@",detail[@"prompt"]]];
        }

        [[Mixpanel sharedInstance] track:@"bike.feedback.submitted"
                              properties:props];
        if (star_rating > 0) {
            [appState.currentBike deselect];
            [appState clearRentalState];
        }
    } failure:^{
        NSLog(@"Failed");
        [self dismissViewControllerAnimated:YES completion:nil];
        [self stopLoading];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"feedback_tv_embed"]) {
        _ftv = (FeedbackTableViewController *)[segue destinationViewController];
        if ([self.restorationIdentifier isEqualToString:@"FeedbackVC"]) {
            _ftv.promptArray = [PromptSetHelper getPromptsForSetNamed:SESSION_END_PROMPTS_KEY][@"prompts"];
        }else{
            _ftv.promptArray = [PromptSetHelper getPromptsForSetNamed:OUT_OF_SESSION_PROMPTS_KEY][@"prompts"];
        }
    }
}

@end
