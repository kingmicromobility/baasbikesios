//
//  AppRestoreViewController.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 8/12/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "AppRestoreViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MenuBaseViewController.h"
#import "ApplicationState.h"
#import "LocationManager.h"
#import "Constants.h"
#import "Mixpanel.h"



@interface AppRestoreViewController ()
@end

@implementation AppRestoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureAppAndLoadInitialData];
}

- (void)configureAppAndLoadInitialData {
    
    [GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onFirstLocationUpdate)
                                                 name:@"baas.person.location.updated"
                                               object:nil];
    
    [LocationManager sharedInstance]; //just touch the location manager so it initializes.
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] == nil) {
        [userDefaults setObject:@(YES) forKey:PERSON_SHOW_INSTRUCTIONS_KEY];
    }
}

- (void)onFirstLocationUpdate {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"baas.person.location.updated"
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onFirstAppStateRestore)
                                                 name:DID_RESTORE_APP_STATE_NOTIFICATION
                                               object:nil];
    [[ApplicationState sharedInstance] restore];
}

- (void)onFirstAppStateRestore {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:DID_RESTORE_APP_STATE_NOTIFICATION
                                                  object:nil];
    MenuBaseViewController *menuBaseVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuBaseVC"];
    [self presentViewController:menuBaseVC animated:NO completion:^{
        [[Mixpanel sharedInstance] track:@"app.launch.finished"];
    }];
}

@end
