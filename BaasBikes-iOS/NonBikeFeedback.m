//
//  NonBikeFeedbackViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/27/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "NonBikeFeedback.h"
#import "Constants.h"
#import <ZendeskSDK/ZendeskSDK.h>
#import "Mixpanel.h"

@interface NonBikeFeedback ()

@end

@implementation NonBikeFeedback

- (void)showNonBikeFeedbackWithNavigationController:(UINavigationController *)navigationController andMarketName:(NSString *)marketName andMarketGUID:(NSString *)marketGUID {
    [[ZDKConfig instance] initializeWithAppId:@"2b6dfaf35876fa3caf4c5979d1f24b42043bd234a0481585" zendeskUrl:@"https://baasbikes.zendesk.com" ClientId:@"mobile_sdk_client_7204bda2847ef58f7363" onSuccess:^() {
        
        [ZDKLogger enable:YES];
        [self configureAppearance];
        [self setUserIdentity];
        
        [ZDKRequests configure:^(ZDKAccount *account, ZDKRequestCreationConfig *requestCreationConfig) {
            [self configureCustomFieldsWithMarketName:marketName andMarketGUID:marketGUID];
            NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
            if(appName) {
                requestCreationConfig.additionalRequestInfo =
                [NSString stringWithFormat:@"\n%@-------------\nSent from %@.", requestCreationConfig.contentSeperator, appName];
            }
        }];
        
        [ZDKRequests showRequestCreationWithNavController:navigationController];
        
    } onError:^(NSError *error) {
        NSLog(@"Error connecting to zendesk: %@", error);
    }];
}

- (void)configureCustomFieldsWithMarketName:(NSString *)marketName andMarketGUID:(NSString *)marketGUID {
//     configgure additional info
    NSString *appVersionString = [NSString stringWithFormat:@"version_%@",
                                  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    // Setting the custom form id to use the IOS Support form
    [ZDKConfig instance].ticketFormId = @62609;
    
    // adding Application Version information
    ZDKCustomField *customFieldApplicationVersion = [[ZDKCustomField alloc] initWithFieldId:@28981538 andValue:appVersionString];
    //OS version
    ZDKCustomField *customFieldOSVersion = [[ZDKCustomField alloc] initWithFieldId:@28988197
                                                                          andValue:[UIDevice currentDevice].systemVersion];
    //            NSString *osVersion = [UIDevice currentDevice].systemVersion;
    
    //Device model
    ZDKCustomField *customFieldDeviceModel = [[ZDKCustomField alloc] initWithFieldId:@28981548
                                                                            andValue:[ZDKDeviceInfo deviceType]];
    //            NSString *deviceType = [ZDKDeviceInfo deviceType];
    //Device memory
    NSString *deviceMemory = [NSString stringWithFormat:@"%f MB", [ZDKDeviceInfo totalDeviceMemory]];
    ZDKCustomField *customFieldDeviceMemory = [[ZDKCustomField alloc] initWithFieldId:@28981558
                                                                             andValue:deviceMemory];
    //Device free space
    NSString *deviceFreeSpace = [NSString stringWithFormat:@"%f GB", [ZDKDeviceInfo freeDiskspace]];
    ZDKCustomField *customFieldDeviceFreeSpace = [[ZDKCustomField alloc] initWithFieldId:@28981568 andValue:deviceFreeSpace];
    
    //Device battery level
    NSString *deviceBatteryLevel = [NSString stringWithFormat:@"%f", [ZDKDeviceInfo batteryLevel]];
    ZDKCustomField *customFieldDeviceBatteryLevel = [[ZDKCustomField alloc] initWithFieldId:@28988277 andValue:deviceBatteryLevel];
    
    if (marketName == nil) {
        marketName = @"None";
    }
    if (marketGUID == nil) {
        marketGUID = @"None";
    }
    
    ZDKCustomField *customFieldMarketGUIG = [[ZDKCustomField alloc] initWithFieldId:@28992127 andValue:marketGUID];
    ZDKCustomField *customFieldMarketName = [[ZDKCustomField alloc] initWithFieldId:@28992047 andValue:marketName];
    
    
    [ZDKConfig instance].customTicketFields = @[customFieldApplicationVersion,
                                                customFieldOSVersion,
                                                customFieldDeviceModel,
                                                customFieldDeviceMemory,
                                                customFieldDeviceFreeSpace,
                                                customFieldDeviceBatteryLevel,
                                                customFieldMarketGUIG,
                                                customFieldMarketName];
    
    
}

- (void)configureAppearance {
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"baas_ic_repair_drk"];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@"My label text"];
    [myString appendAttributedString:attachmentString];
    
    [[ZDKCreateRequestView appearance] setPlaceholderTextColor:[UIColor lightGrayColor]];
    [[ZDKCreateRequestView appearance] setTextEntryColor:[UIColor blackColor]];
    [[ZDKCreateRequestView appearance] setTextEntryBackgroundColor:[UIColor whiteColor]];
    [[ZDKCreateRequestView appearance] setViewBackgroundColor:[UIColor whiteColor]];
    [[ZDKCreateRequestView appearance] setTextEntryFont:[UIFont systemFontOfSize:15.0f]];
    
    UIImage *attachmentImage = [[ZDKBundleUtils imageNamed:@"icoAttach" ofType:@"png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [[ZDKCreateRequestView appearance] setAttachmentButtonImage:attachmentImage];
    [[[ZDKCreateRequestView appearance] attachImageButton] setTintColor:ORANGE_TEXT_COLOR];
    
    [[ZDKCreateRequestView appearance] setAttachmentButtonBackground:[UIColor whiteColor]];
    [[ZDKCreateRequestView appearance] setAttachmentButtonBorderColor:ORANGE_TEXT_COLOR];
    [[ZDKCreateRequestView appearance] setAttachmentButtonBorderWidth:@2];
    [[ZDKCreateRequestView appearance] setAttachmentButtonCornerRadius:@10];
    
    [[ZDKCreateRequestView appearance] setAutomaticallyHideNavBarOnLandscape:@1];
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    [spinner setTintColor:ORANGE_TEXT_COLOR];
    [[ZDKCreateRequestView appearance] setSpinner:(id<ZDKSpinnerDelegate>)spinner];
    
    [[[ZDKCreateRequestView appearance] textView] setPlaceholderText:@"THIS IS IT"];
}

- (void)setUserIdentity {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *email = [defaults valueForKey:PERSON_EMAIL_KEY];
    NSString *firstName = [defaults valueForKey:PERSON_FIRST_NAME_KEY];
    NSString *lastName = [defaults valueForKey:PERSON_LAST_NAME_KEY];
    NSString *guid = [defaults valueForKey:PERSON_GUID_KEY];
    
    ZDKAnonymousIdentity *identity = [ZDKAnonymousIdentity new];
    identity.name = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    identity.email = email;
    identity.externalId = guid;
    [ZDKConfig instance].userIdentity = identity;
}

@end
