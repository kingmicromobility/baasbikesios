//
//  NSString+LocationHelper.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/14/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "NSString+LocationHelper.h"

@implementation NSString (LocationHelper)

+ (NSString *)makeStringFromCoordinate:(CLLocationCoordinate2D)coord {
    NSString *formatString = @"SRID=4326;POINT (%.9f %.9f)";
    return [NSString stringWithFormat:formatString, coord.longitude, coord.latitude];
}

@end
