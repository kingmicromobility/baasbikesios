//
//  Market.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RestApiClient.h"
#import "Environment.h"
#import "Imagecache.h"
#import "WKTParser.h"
#import "Constants.h"
#import "Market.h"
#import "Bike.h"

#define URI @"markets/"

@interface Market()
@property (nonatomic, strong) PTPusher *pusherClient;
@property (nonatomic, strong) PTPusherChannel *pusherChannel;
@end

@implementation Market

+ (void)nearest:(CLLocation *)location
        success:(void (^)(Market *))successBlock
        failure:(void (^)(NSError *))failureBlock {
    NSDictionary *params = @{
                            @"lat": @(location.coordinate.latitude),
                            @"lon": @(location.coordinate.longitude)
                            };
    [[RestApiClient sharedInstance] list:URI
                                  params:params
                                 success:[self onSuccess:successBlock]
                                   error:failureBlock];
}

+ (void (^)(NSDictionary *))onSuccess:(void(^)(Market *))block {
    return ^void(NSDictionary *marketsDict) {
        Market *closestMarket = [[Market alloc] initWithDictionary:marketsDict[@"results"][0]];
        block(closestMarket);
    };
}

+ (void)getByUUID:(NSUUID *)uuid
      withSuccess:(void (^)(Market *))successBlock
       andFailure:(void (^)(NSError *))failureBlock {
    [[RestApiClient sharedInstance] show:URI
                                    uuid:uuid
                                  params:nil
                                 success:[self onGetByUUIDSuccess:successBlock]
                                   error:failureBlock];
}

+ (void (^)(NSDictionary *))onGetByUUIDSuccess:(void(^)(Market *))block {
    return ^void(NSDictionary *marketDict) {
        Market *market = [[Market alloc] initWithDictionary:marketDict];
        block(market);
    };
}


- (instancetype)initWithDictionary:(NSDictionary *)marketDict {
    self.guid = [[NSUUID alloc] initWithUUIDString:marketDict[@"guid"]];
    self.name = marketDict[@"name"];
    self.geoshape = [self parseMarketGeoshapeString:marketDict[@"geoshape"]];
    self.attributionChannels = marketDict[@"attribution_channels"];
    self.closestBikeDistance = marketDict[@"closest_bike_distance"];
    if ([marketDict objectForKey:@"bike_set"] != [NSNull null]) {
        self.bikes = [Bike dictionaryFromArray:marketDict[@"bike_set"] forMarket:self];
    }
    if ([marketDict objectForKey:@"closest_bike"] != [NSNull null]) {
        self.closestBike = self.bikes[[[NSUUID alloc] initWithUUIDString:marketDict[@"closest_bike"][@"guid"]]];
    }
    [self cacheImages:marketDict[@"bike_images"]];
    [self calculateCorners];
    [self receiveWebsocketUpdates];
    return self;
}

- (void)location:(CLLocation *)location
        isInside:(void (^)(Market *))isInsideBlock
       isOutside:(void (^)(Market *))isOutsideBlock
       orFailure:(void (^)(NSError *))failureBlock{
    NSDictionary *params = @{
                             @"lat": @(location.coordinate.latitude),
                             @"lon": @(location.coordinate.longitude)
                             };
    [[RestApiClient sharedInstance] list:[NSString stringWithFormat:@"%@%@/contains/", URI, self.guid.UUIDString.lowercaseString]
                                  params:params
                                 success:^(NSDictionary *successDict) {
                                     if ([successDict[@"within_market"] isEqualToValue:@YES]) {
                                         isInsideBlock(self);
                                     } else {
                                         isOutsideBlock(self);
                                     }
                                 } error:failureBlock];
}

- (void)addToMapView:(GMSMapView *)mapView {
    for (NSArray *point_set in self.geoshape) {
        [self addOverlayForRectOfPoints:point_set toMap:mapView];
    }
    for (Bike *bike in [self.bikes allValues]) {
        [bike.marker assignToMap:mapView];
    }
}

- (BoundingRect *)getBoundingRect {
    if (!CLLocationCoordinate2DIsValid(self.nwCorner)
      || CLLocationCoordinate2DIsValid(self.neCorner)
      || CLLocationCoordinate2DIsValid(self.swCorner)
      || CLLocationCoordinate2DIsValid(self.seCorner)) {
        [self calculateCorners];
    }

    BoundingRect *rect = [[BoundingRect alloc] init];
    rect.center = self.center;
    rect.topLeft = self.nwCorner;
    rect.topRight = self.neCorner;
    rect.bottomLeft = self.swCorner;
    rect.bottomRight = self.seCorner;
    return rect;
}

- (void)focusMap:(GMSMapView *)mapView {
    BoundingRect *marketBoundingRect = [self getBoundingRect];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marketBoundingRect.center coordinate:marketBoundingRect.center];
    GMSMarker *topLeft = [GMSMarker markerWithPosition:marketBoundingRect.topLeft];
    GMSMarker *topRight = [GMSMarker markerWithPosition:marketBoundingRect.topRight];
    GMSMarker *bottomLeft = [GMSMarker markerWithPosition:marketBoundingRect.bottomLeft];
    GMSMarker *bottomRight = [GMSMarker markerWithPosition:marketBoundingRect.bottomRight];
    NSArray *markers = @[topLeft, topRight, bottomLeft, bottomRight];
    for (GMSMarker *marker in markers)
        bounds = [bounds includingCoordinate:marker.position];
    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:15.0f]];
}

- (NSDictionary *)propertiesAsDictionary {
    NSMutableDictionary *props = [NSMutableDictionary dictionaryWithDictionary:@{
     @"market_name"          : self.name,
     @"market_bike_count"    : [NSNumber numberWithLong:self.bikes.count],
     @"market_center_lat"    : [NSNumber numberWithDouble:self.center.latitude],
     @"market_center_lng"    : [NSNumber numberWithDouble:self.center.longitude],
     @"market_nw_corner_lat" : [NSNumber numberWithDouble:self.nwCorner.latitude],
     @"market_nw_corner_lng" : [NSNumber numberWithDouble:self.nwCorner.longitude],
     @"market_ne_corner_lat" : [NSNumber numberWithDouble:self.neCorner.latitude],
     @"market_ne_corner_lng" : [NSNumber numberWithDouble:self.neCorner.longitude],
     @"market_sw_corner_lat" : [NSNumber numberWithDouble:self.swCorner.latitude],
     @"market_sw_corner_lng" : [NSNumber numberWithDouble:self.swCorner.longitude],
     @"market_se_corner_lat" : [NSNumber numberWithDouble:self.seCorner.latitude],
     @"market_se_corner_lng" : [NSNumber numberWithDouble:self.seCorner.longitude],
     @"market_closest_available_bike_distance" : self.closestBikeDistance
     }];

    if (self.closestBike) {
        [props addEntriesFromDictionary:@{
          @"market_closest_available_bike_lat" : [NSNumber numberWithDouble:self.closestBike.lastLocation.coordinate.latitude],
          @"market_closest_available_bike_lng" : [NSNumber numberWithDouble:self.closestBike.lastLocation.coordinate.longitude]
          }];
    }
    return props;
}

# pragma mark - Private Methods

- (void)cacheImages:(NSDictionary *)imageSetsDict {
    ImageCache *imageCache = [ImageCache sharedInstance];
    [imageCache cacheImageSetsDict:imageSetsDict];
}

- (NSArray *)parseMarketGeoshapeString:(NSString *)wktPolygonString {
    if ([wktPolygonString containsString:@";"]) {
        wktPolygonString = [wktPolygonString componentsSeparatedByString:@";"][1];
    }
    return [WKTParser coordinatesForExteriorsFromWkt:wktPolygonString];
}

- (void)calculateCorners {
    double maxY = 0.0;
    double minY = 0.0;
    double maxX = 0.0;
    double minX = 0.0;
    for (NSArray *point_set in self.geoshape) {
        for (CLLocation *point in point_set) {
            CLLocationCoordinate2D currentPoint = CLLocationCoordinate2DMake(point.coordinate.latitude, point.coordinate.longitude);
            if (maxY == 0.0) maxY = currentPoint.latitude;
            if (minY == 0.0) minY = currentPoint.latitude;
            if (maxX == 0.0) maxX = currentPoint.longitude;
            if (minX == 0.0) minX = currentPoint.longitude;
            if (fabs(currentPoint.latitude)  > fabs(maxY)) maxY = currentPoint.latitude;
            if (fabs(currentPoint.latitude)  < fabs(minY)) minY = currentPoint.latitude;
            if (fabs(currentPoint.longitude) > fabs(maxX)) maxX = currentPoint.longitude;
            if (fabs(currentPoint.longitude) < fabs(minX)) minX = currentPoint.longitude;
        }
    }
    double cX = minX + ((maxX - minX)/2);
    double cY = minY + ((maxY - minY)/2);
    self.center = CLLocationCoordinate2DMake(cY, cX);
    self.nwCorner = CLLocationCoordinate2DMake(maxY, minX);
    self.neCorner = CLLocationCoordinate2DMake(maxY, maxX);
    self.swCorner = CLLocationCoordinate2DMake(minY, minX);
    self.seCorner = CLLocationCoordinate2DMake(minY, maxX);
}

- (void)addOverlayForRectOfPoints:(NSArray *)points toMap:(GMSMapView *)map {
    GMSMutablePath *rect = [GMSMutablePath path];
    for (CLLocation *point in points) {
        [rect addCoordinate:point.coordinate];
    };
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor   = OVERLAY_FILL_COLOR;
    polygon.strokeColor = OVERLAY_STROKE_COLOR;
    polygon.strokeWidth = 1;
    polygon.map = map;
    polygon.accessibilityLabel = @"Market Boundaries";
}


- (void)receiveWebsocketUpdates {
    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *appKey = config[@"PUSHER_APP_KEY"];
    NSString *marketGuid = self.guid.UUIDString.lowercaseString;
    NSString *channelName = [NSString stringWithFormat:@"market-%@", marketGuid];
    self.pusherClient = [PTPusher pusherWithKey:appKey delegate:self];
    self.pusherChannel = [self.pusherClient subscribeToChannelNamed:channelName];
    [self.pusherChannel bindToEventNamed:@"bike" handleWithBlock:[self onWebSocketEventReceived]];
    [self.pusherClient connect];
}

- (void (^)(PTPusherEvent *))onWebSocketEventReceived {
    return ^void(PTPusherEvent *bikeEvent) {
        NSUUID *bikeUUID = [[NSUUID alloc] initWithUUIDString:bikeEvent.data[@"guid"]];
        Bike *bike = [self.bikes objectForKey:bikeUUID];
        if (bike) {
            [bike updateWithDictionary:bikeEvent.data];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"baas.market.bike.updated"
                                                                object:bike
                                                              userInfo:nil];
        } else {
            bike = [[Bike alloc] initWithDictionary:bikeEvent.data
                                          andMarket:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"baas.market.bike.added"
                                                                object:bike
                                                              userInfo:nil];
        }
    };
}


@end
