//
//  LeftMenuViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/29/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "MenuBaseViewController.h"
#import "Constants.h"
#import "Help.h"
#import "NonBikeFeedback.h"
#import "Mixpanel.h"
#import "ApplicationState.h"

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showScrollerWithImage:[UIImage imageNamed:@"baas_logo_horizontal"]];
    [self embedAppropriateMenuInContainer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn) name:USER_LOGGED_IN_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOut) name:USER_LOGGED_OUT_NOTIFICATION object:nil];
}

- (IBAction)pressedSignIn {
    [self showSignInOrSignUpScreen];
}

- (void)userLoggedIn {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showAuthenticatedMenu];
    });
}

- (void)userLoggedOut {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showUnauthenticatedMenu];
    });
}

- (void)embedAppropriateMenuInContainer {
    if ([self userIsPossiblyAuthenticated]) {
        [self showAuthenticatedMenu];
    }
    else {
        [self showUnauthenticatedMenu];
    }
}

- (void)showAuthenticatedMenu {
    [self showMenuWithStoryboardID:@"AuthenticatedMenu"];
    _signInButton.userInteractionEnabled = NO;
    [_signInButton setTitle:@"" forState:UIControlStateNormal];
}

- (void)showUnauthenticatedMenu {
    [self showMenuWithStoryboardID:@"UnauthenticatedMenu"];
    _signInButton.userInteractionEnabled = YES;
    [_signInButton setTitle:@"SIGN UP / LOG IN" forState:UIControlStateNormal];
}

- (void)showMenuWithStoryboardID:(NSString *)storyboardID {
    if (_embeddedTableView) {
        [_embeddedTableView removeFromParentViewController];
        [_embeddedTableView.view removeFromSuperview];
        _embeddedTableView = nil;
        [[_containerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    _embeddedTableView = [self.storyboard instantiateViewControllerWithIdentifier:storyboardID];
    [self addChildViewController:_embeddedTableView];
    [_containerView addSubview:_embeddedTableView.view];
    [_embeddedTableView didMoveToParentViewController:self];
    _embeddedTableView.delegate = self;
    //TODO: consider adding constraints here?
}

#pragma mark MenuSelectionDelegate methods
- (void)selectedMenuItemWithName:(NSString *)menuItemName {
    
    MenuBaseViewController *menuBase = ((MenuBaseViewController *)self.parentViewController);
    UINavigationController *contentVC = (UINavigationController *)menuBase.contentViewController;
    
    if ([menuItemName isEqualToString: @"CONTACT US"]) {
        [self hideMenuIfPossible];
        NonBikeFeedback *nonBikeFeedback = [[NonBikeFeedback alloc] init];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [nonBikeFeedback showNonBikeFeedbackWithNavigationController:contentVC andMarketName:appState.currentMarket.name andMarketGUID:appState.currentMarket.guid.UUIDString.lowercaseString];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"drawer.left.problem" properties:nil];
    }else if ([menuItemName isEqualToString: @"RIDE A BIKE"]) {
        [self hideMenuIfPossible];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"drawer.left.ride" properties:nil];
    }else if ([menuItemName isEqualToString: @"WHAT ARE BAAS BIKES?"]) {
        UIViewController *signInVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:signInVC];
        [navController setNavigationBarHidden:YES];
        [self presentViewController:navController animated:YES completion:nil];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"drawer.left.whatAreBaasBikes" properties:nil];
    }else if ([menuItemName isEqualToString: @"HELP"]){
        [self hideMenuIfPossible];
        Help *help = [[Help alloc] init];
        [help showHelpCenterWithNavigationController:contentVC];
    }else{
        // TODO: Give everything that unnecessarily has its own navigation controller to the contentVC
//        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:menuItemName];
//        [contentVC pushViewController:vc animated:YES];
        [self showViewControllerWithStoryboardID:menuItemName];
    }
    [self hideMenuIfPossible];
}

@end
