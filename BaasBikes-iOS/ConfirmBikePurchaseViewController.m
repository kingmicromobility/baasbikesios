//
//  ConfirmBikePurchaseViewController.m
//  BaasBikes-iOS
//
//  Created by Daniel Christopher on 3/19/17.
//  Copyright © 2017 Baas, Inc. All rights reserved.
//

#import "ConfirmBikePurchaseViewController.h"
#import "Mixpanel.h"
#import "ApplicationState.h"

@interface ConfirmBikePurchaseViewController ()

@end

@implementation ConfirmBikePurchaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden: YES animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)backPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)confirmPressed:(UIButton *)sender {
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.details.purchasecomplete" properties:[bike propertiesAsDictionary]];
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageUsInstructionsViewController"];
    [self.navigationController pushViewController:vc animated:true];
}
@end
