//
//  RideHistoryViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface RideHistoryViewController : BaseViewController

- (IBAction)didPushBackButton:(id)sender;

@end
