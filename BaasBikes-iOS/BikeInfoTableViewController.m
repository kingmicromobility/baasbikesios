//
//  BikeInfoTableViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/30/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "BikeInfoTableViewController.h"
#import "ApplicationState.h"
#import "ImageCache.h"

@interface BikeInfoTableViewController ()

@end

@implementation BikeInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ApplicationState *appState = [ApplicationState sharedInstance];
    ImageCache *imageCache = [ImageCache sharedInstance];
    
    [_brandLabel setText:appState.currentBike.brand];
    [_colorLabel setText:appState.currentBike.details[@"color"]];
    [_modelLabel setText:appState.currentBike.details[@"model"]];
    [_gearsLabel setText:appState.currentBike.details[@"gears"]];
    [_sizeLabel setText:appState.currentBike.size];
    NSString *priceString = appState.currentBike.details[@"price_with_lock"];
    NSInteger price = priceString.intValue/100;
    [_priceLabel setText:[NSString stringWithFormat:@"$%ld", (long)price]];
    [_bikeImageView setImage:[imageCache getImageWithKey:@"thumbnail" forSet:appState.currentBike.imageSet]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
