//
//  MenuBaseViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/29/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RESideMenu.h"
#import "MainViewController.h"

@interface MenuBaseViewController : RESideMenu <RESideMenuDelegate, MenuDelegate>

@end
