//
//  SignUpOrLoginViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/1/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "SignUpOrLoginViewController.h"
#import "WalkthroughContentViewController.h"
#import "Constants.h"
#import "Mixpanel.h"

@interface SignUpOrLoginViewController ()

@end

@implementation SignUpOrLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePageControl:) name:PAGE_INDEX_CHANGED_NOTIFICATION object:nil];
    
    _pageImages = @[@"tutorial_01_reserve", @"tutorial_02_unlock", @"tutorial_03_adjust_seat", @"tutorial_04_hold_bike", @"tutorial_05_geofence"];
    _pageHeadlines = @[@"Find a bike nearby", @"Push button on smart lock to wake it up", @"Unlock bike with bluetooth to test ride", @"If you like it, purchase bike from the app", @"...or relock the bike to the original lock"];
    
    _headline.layer.cornerRadius = 8;
    _headline.layer.masksToBounds = YES;
    
    _pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WALKTHROUGH CONTROLLER"];
    _pageViewController.dataSource = self;
    
    // Create walkthrough controller and set the initial screen
    WalkthroughContentViewController *initialView = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[initialView];
    [_pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    _pageControl.pageIndicatorTintColor = WHITE_INDICATOR_COLOR_ALPHA;
    _pageControl.currentPageIndicatorTintColor = WHITE_INDICATOR_COLOR;
    _pageControl.backgroundColor = [UIColor clearColor];
    
    [self.view insertSubview:_pageViewController.view atIndex:0];
    
    // Remove extra white space from, below the pageViewController
    NSArray *subviews = _pageViewController.view.subviews;
    UIPageControl *thisControl = nil;
    for (int i=0; i<[subviews count]; i++) {
        if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
            thisControl = (UIPageControl *)[subviews objectAtIndex:i];
        }
    }
    
    thisControl.hidden = true;
    _pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);

    [self showNextButton];
    [self showScrollerWithText:@"SIGN UP / SIGN IN"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PAGE_INDEX_CHANGED_NOTIFICATION object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pressedClose {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"tutorial.back" properties:nil];
}

- (IBAction)pressedNext:(id)sender {
    _pageControl.currentPage++;
    [_pageViewController setViewControllers:@[[self viewControllerAtIndex:_pageControl.currentPage]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES
                                 completion:nil];
    [self updateHeadline:_pageControl.currentPage];
    if (_pageControl.currentPage == 4) {
        [self showSignUpButton];
    } else {
        [self showNextButton];
    }
}

-(void)updatePageControl:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSUInteger pageIndex = [[userInfo objectForKey:@"page"] unsignedIntegerValue];
    _pageControl.currentPage = pageIndex;
    [self updateHeadline:_pageControl.currentPage];
    if (_pageControl.currentPage == 4) {
        [self showSignUpButton];
    } else {
        [self showNextButton];
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((WalkthroughContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = ((WalkthroughContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageImages count]) {
        return nil; 
    }
    return [self viewControllerAtIndex:index];
    
}

- (WalkthroughContentViewController *)viewControllerAtIndex:(NSUInteger)index {
    if (([self.pageImages count] == 0) || (index >= [self.pageImages count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    WalkthroughContentViewController *walkthroughContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WALKTHROUGH CONTENT"];
    walkthroughContentViewController.imageFile = _pageImages[index];
    walkthroughContentViewController.pageIndex = index;
    
    return walkthroughContentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageImages count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void) updateHeadline:(NSInteger)index {
    _headlineContainerTopConstraint.constant = -70.0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _headline.text = _pageHeadlines[_pageControl.currentPage];
        _headlineContainerTopConstraint.constant = 20.0;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];
}

- (void) showSignUpButton {
    _nextButtonBottomDistance.constant = -120.0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _signUpButtonBottomDistance.constant = 38.0;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];
}

- (void) showNextButton {
    _signUpButtonBottomDistance.constant = -120.0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        _nextButtonBottomDistance.constant = 38.0;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];
}

@end
