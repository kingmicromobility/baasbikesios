//
//  CreateAccountTableViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "CreateAccountTableViewController.h"

@implementation CreateAccountTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0,0,0,0)];
    [self.tableView setContentOffset:CGPointMake(0, 0)];
    
    _firstNameTextField.delegate = _textDelegate;
    _lastNameTextField.delegate = _textDelegate;
    _emailTextField.delegate = _textDelegate;
    _passwordTextField.delegate = _textDelegate;
    _phoneNumberTextField.delegate = _textDelegate;
    _attributionSelector.delegate = _pickerDelegate;

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_firstNameTextField becomeFirstResponder];
}


@end
