//
//  ImageCache.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 7/23/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "ImageCache.h"

@interface ImageCache()
@property (nonatomic, strong) NSMutableDictionary *cache;
@end

@implementation ImageCache

+ (instancetype)sharedInstance {
    static ImageCache *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[ImageCache alloc] init];
    });

    return this;
}

- (instancetype) init {
    self = [super init];
    self.cache = [[NSMutableDictionary alloc] init];
    return self;
}

- (void) cacheImageSetsDict:(NSDictionary *)imageSetsDict {
    for (NSString *set in [imageSetsDict allKeys]) {
        NSDictionary *imageSet = [imageSetsDict objectForKey:set];
        for (NSString *key in [imageSet allKeys]) {
            NSLog(@"value of 'key' is: %@", key);
            NSString *imageUri = imageSetsDict[set][key];
            [self cacheImageUri:imageUri withKey:key forSet:[[NSUUID alloc] initWithUUIDString:set]];
        }
    }
}

- (void) cacheImageUri:(NSString *)imageUri
               withKey:(NSString *)key
                forSet:(NSUUID *)uuid {
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUri]];
    NSMutableDictionary *set = [self.cache objectForKey:uuid.UUIDString.lowercaseString];
    if (!set) {
        set = [[NSMutableDictionary alloc] init];
    }
    [set setObject:imageData forKey:key];
    [self.cache setValue:set forKey:uuid.UUIDString.lowercaseString];
}

- (UIImage *) getImageWithKey:(NSString *)key
                       forSet:(NSUUID *)uuid {
    NSData *imageData = self.cache[uuid.UUIDString.lowercaseString][key];
    return [UIImage imageWithData:imageData];
}

@end
