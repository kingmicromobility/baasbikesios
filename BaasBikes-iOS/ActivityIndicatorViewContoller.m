//
//  ActivityIndicatorViewController.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/7/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "ActivityIndicatorViewController.h"

@interface ActivityIndicatorViewController()
@property (nonatomic, strong) NSString *messageText;
@end

@implementation ActivityIndicatorViewController

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.activityIndicatorLabel.text = self.messageText;
    [self.view layoutIfNeeded];
}

- (void) addToView:(UIView *)parentView {
    NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter addObserver:self
                   selector:@selector(setMessageFromNotification:)
                       name:@"LoadingIndicatorMessageUpdate"
                     object:nil];
    [self.view setFrame:parentView.bounds];
    [parentView addSubview:self.view];
    [parentView bringSubviewToFront:self.view];
}

- (void) removeFromView {
    [self.view removeFromSuperview];
    NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter removeObserver:self
                          name:@"LoadingIndicatorMessageUpdate"
                        object:nil];
}

- (void) setMessage:(NSString *)message {
    self.messageText = message;
    self.activityIndicatorLabel.text = message;
    [self.view layoutIfNeeded];
}

- (void) setMessageFromNotification:(NSNotification *)notification {
    [self setMessage:notification.object];
}

@end
