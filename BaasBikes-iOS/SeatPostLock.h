//
//  SeatPostLock.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 2/22/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BluetoothCentralManager.h"
#import "Lock.h"

#define BAAS_BATTERY_SERVICE_UUID       [CBUUID UUIDWithString:@"180F"]
#define BAAS_SEAT_LOCK_SERVICE_UUID     [CBUUID UUIDWithString:@"855487cd-4ed4-454a-9e5b-1ed4412a5a75"]
#define BAAS_LOCK_STATE_CHAR_UUID       [CBUUID UUIDWithString:@"a91693f8-2206-4472-b85d-ea3f85107f93"]
#define BAAS_CHALLENGE_CODE_CHAR_UUID   [CBUUID UUIDWithString:@"4c3bc30a-821d-47b2-9df3-5e03e99b8401"]
#define BAAS_UNLOCK_COMMAND_CHAR_UUID   [CBUUID UUIDWithString:@"18da67a3-d04c-43e3-90bb-8902735425cf"]
#define BAAS_LOCK_COMMAND_CHAR_UUID     [CBUUID UUIDWithString:@"0d408213-a740-c49f-bae7-ee959d465506"]

#define SEAT_POST_UNLOCK_MESSAGE_TITLE @"To Unlock this Bike..."
#define SEAT_POST_UNLOCK_MESSAGE_BODY  @"When you arrive at this bike, press the button on the side of the seat post to turn the lock on. Once the button turns blue, press the 'unlock' button (in the app) below."

@interface SeatPostLock : Lock <BluetoothCentralManagerDelegate, CBPeripheralDelegate>

@property CBCharacteristic *batteryLevel;
@property CBCharacteristic *lockState;
@property CBCharacteristic *challengeCode;
@property CBCharacteristic *lockCommand;
@property CBCharacteristic *unlockCommand;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *desiredState;

@end
