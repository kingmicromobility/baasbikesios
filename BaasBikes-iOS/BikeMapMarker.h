//
//  BikeMapMarker.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/16/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@class Bike;

@interface BikeMapMarker : GMSMarker

@property (nonatomic, strong) Bike *bike;

- (id)initWithBike:(Bike *)bike;
- (void)assignToMap:(GMSMapView *)mapView;
- (void)refresh;
- (void)refreshPosition;

@end
