//
//  LocationManager.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 7/28/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "LocationManager.h"
#import "Mixpanel.h"

@interface LocationManager()
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLLocationManager *clLocationManager;
@property (nonatomic, strong) NSNotificationCenter *notiCenter;
@end

@implementation LocationManager

+ (instancetype) sharedInstance {
    static dispatch_once_t predicate;
    static LocationManager *instance = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_once(&predicate, ^{instance = [[self alloc] init];});
    });
    
    return instance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _notiCenter = [NSNotificationCenter defaultCenter];
        _clLocationManager = [[CLLocationManager alloc] init];
        _clLocationManager.delegate = self;
        _clLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        _clLocationManager.activityType = CLActivityTypeFitness;
        _clLocationManager.pausesLocationUpdatesAutomatically = NO;
        _clLocationManager.allowsBackgroundLocationUpdates = YES;
        [_clLocationManager requestAlwaysAuthorization];
        
        if (![self isAuthorized]) {
            [self postNotificationWithNewLocation:nil];
        }
    }
    
    return self;
}

- (BOOL) isReady {
    return [self isAuthorized] && self.currentLocation;
}

- (BOOL) isAuthorized {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    return status == kCLAuthorizationStatusAuthorizedAlways;
}

- (void) startLocationUpdates {
    [self.clLocationManager startUpdatingLocation];
}

- (void) stopLocationUpdates {
    [self.clLocationManager stopUpdatingLocation];
}

- (CLLocation *) getCurrentLocation {
    return self.currentLocation;
}

# pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    if ([self isAuthorized]) {
        [mixpanel track:@"permissions.location.allowed" properties:nil];
        [self startLocationUpdates];
    } else if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // Display a popup asking user to change authorization in phone settings.
        //        [self showAlertWithTitle:@"Location Access Required"
        //                  andDescription:@"In order for Baas Bikes to function, the app needs access to your current location. Please go to Settings -> Baas Bikes, and allow location access."];
    } else if (status == kCLAuthorizationStatusDenied) {
        [mixpanel track:@"permissions.location.denied" properties:nil];
        //        [self showAlertWithTitle:@"Location Access Required"
        //                  andDescription:@"In order for Baas Bikes to function, the app needs access to your current location. Please go to Settings -> Baas Bikes, and allow location access."];
    }
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = newLocation;
    [self postNotificationWithNewLocation:newLocation];
}

- (void)postNotificationWithNewLocation:(CLLocation *)location {
    [self.notiCenter postNotificationName:@"baas.person.location.updated"
                                   object:location
                                 userInfo:nil];
}


@end
