//
//  Reservation.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class Bike;
@class Session;
@class Market;

@interface Reservation : NSObject
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) Bike *bike;
@property (nonatomic, strong) Market *market;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NSNumber *canceled;
@property (nonatomic, strong) NSDate *expiresAt;
@property (nonatomic, strong) NSDate *sessionStartedAt;
@property NSTimeInterval remainingTime;

+ (Reservation *)getByUUID:(NSUUID *)uuid;

- (instancetype)initWithBike:(Bike *)bike;

- (void)initWithDictionary:(NSDictionary *)reservationDictionary
                andSuccess:(void (^)(Reservation *))successBlock
                andFailure:(void (^)(NSError *))failureBlock;

- (void)beginWithSuccess:(void(^)(Reservation *))successBlock
              andFailure:(void(^)(NSError *))failureBlock;

- (void)beginSessionWithSuccess:(void(^)(Session *))successBlock
                     andFailure:(void(^)(NSError *))failureBlock;

- (void)cancelWithSuccess:(void(^)(Reservation *))successBlock
               andFailure:(void(^)(NSError *))failureBlock;

- (void)saveWithSuccess:(void(^)(Reservation *))successBlock
             andFailure:(void(^)(NSError *))failureBlock;

- (NSDictionary *)propertiesAsDictionary;

@end
