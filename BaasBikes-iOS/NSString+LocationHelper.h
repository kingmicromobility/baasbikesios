//
//  NSString+LocationHelper.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/14/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface NSString (LocationHelper)

+ (NSString *)makeStringFromCoordinate:(CLLocationCoordinate2D)coord;

@end
