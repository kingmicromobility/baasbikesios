//
//  SessionInProgressViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "SessionInProgressViewController.h"
#import "Session.h"
#import "ApplicationState.h"

@interface SessionInProgressViewController ()

@end

@implementation SessionInProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Session *sess = [ApplicationState sharedInstance].currentSession;

    int hours = 0 - ((int)sess.elapsedTime / 3600.0);
    int minutes = 0 - ((int)(sess.elapsedTime / 60) % 60);
    
    if (minutes < 10) {
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour 0%d min", hours, minutes]];
    }else{
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour %d min", hours, minutes]];
    }
    
    [_priceLabel setText:[NSString stringWithFormat:@"Price: $%ld.00",(long)sess.totalPrice]];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateLabels:)
                                                 name:@"baas.session.update.elapsedTime"
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"baas.session.update.elapsedTime"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)updateLabels:(NSNotification *)notification {
    Session *sess = (Session *)notification.object;
    
    int hours = 0 - (sess.elapsedTime / 3600.0);
    int minutes = 0 - ((int)(sess.elapsedTime / 60) % 60);
    
    if (minutes < 10) {
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour 0%d min", hours, minutes]];
    }else{
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour %d min", hours, minutes]];
    }
    
    [_priceLabel setText:[NSString stringWithFormat:@"Price: $%ld.00",(long)sess.totalPrice]];
}

@end
