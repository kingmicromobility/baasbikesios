//
//  BaasPickerLabel.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/14/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaasPickerLabel : UILabel

@end
