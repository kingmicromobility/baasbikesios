//
//  ApplicationState.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/17/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationManager.h"
#import "Reservation.h"
#import "Session.h"
#import "Market.h"
#import "Bike.h"
#import "Trip.h"

@interface ApplicationState : NSObject

@property (strong, nonatomic) Reservation *currentReservation;
@property (strong, nonatomic) Session *currentSession;
@property (strong, nonatomic) Market *currentMarket;
@property (strong, nonatomic) Bike *currentBike;
@property (strong, nonatomic) GMSMapView *mainMap;

+ (instancetype)sharedInstance;

- (void)restoreForPerson:(Person *)person
             withSuccess:(void (^)(ApplicationState *, CLLocation *))successBlock
              andFailure:(void (^)(NSError *))failureBlock;

- (void)restore;

- (void)updateCurrentBike:(Bike *)newCurrentBike;

- (void)clearRentalState;

@end
