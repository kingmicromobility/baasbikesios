//
//  RideHistoryViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RideHistoryViewController.h"
#import "RideHistoryTableViewController.h"
#import "Mixpanel.h"

@interface RideHistoryViewController ()

@end

@implementation RideHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"drawer.left.history" properties:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"EmbedRideHistorySegue"]) {
        RideHistoryTableViewController *rideHistoryTableVC = (RideHistoryTableViewController *)segue.destinationViewController;
        rideHistoryTableVC.rideHistoryVC = self;
    }
}

- (IBAction)didPushBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
