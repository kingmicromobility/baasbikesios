//
//  RouteMapPolyline.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 8/3/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@class Trip;

@interface RouteMapPolyline : GMSPolyline

@property (nonatomic, strong) Trip *trip;

- (id)initWithTrip:(Trip *)trip;
- (void)assignToMap:(GMSMapView *)mapView;
- (void)refresh;

@end
