//
//  LockManager.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/4/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lock.h"

#define BAAS_FAKE_LOCK @"fake"
#define BAAS_DUTCH_RETROFIT_LOCK @"dutch-retrofit"
#define BAAS_SEAT_POST_LOCK @"seat-post"

@interface LockManager : NSObject

+ (instancetype) sharedInstance;

- (Lock *) getLockOfType:(NSString *)type
              withLockId:(NSString *)lockId
                  andUrn:(NSString *)urn
            withDelegate:(id<LockDelegate>)delegate;

- (Lock *) getLockByURN:(NSString *)urn
           withDelegate:(id<LockDelegate>)delegate;


@end
