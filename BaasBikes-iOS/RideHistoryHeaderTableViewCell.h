//
//  RideHistoryHeaderTableViewCell.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideHistoryHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
