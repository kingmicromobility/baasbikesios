//
//  ReserveBikeViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RentalViewController.h"

@interface ReserveBikeViewController : RentalViewController


- (IBAction)didPressCancel:(id)sender;
- (IBAction)didPressReserve:(id)sender;

@end
