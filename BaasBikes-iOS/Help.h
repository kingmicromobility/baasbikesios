//
//  HelpViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/28/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Help : NSObject

- (void)showHelpCenterWithNavigationController:(UINavigationController *)navigationController;

@end
