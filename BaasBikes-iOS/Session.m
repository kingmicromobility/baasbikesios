//
//  Session.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RestApiClient.h"
#import "Market.h"
#import "Session.h"
#import "Bike.h"

#define URI @"sessions/"

@interface Session()
@property NSTimer *timer;
@end

@implementation Session

+ (Session *)getByUUID:(NSUUID *)uuid {
    return nil;
}

- (void)initWithDictionary:(NSDictionary *)sessionDictionary
                andSuccess:(void (^)(Session *))successBlock
                andFailure:(void (^)(NSError *))failureBlock {
    NSUUID *marketGuid = [[NSUUID alloc] initWithUUIDString:sessionDictionary[@"market"][@"guid"]];
    [Market getByUUID:marketGuid
          withSuccess:^(Market *market) {
              [[Reservation alloc] initWithDictionary:sessionDictionary[@"reservation"]
                    andSuccess:^(Reservation *reservation) {
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
                        self.guid = [[NSUUID alloc] initWithUUIDString:sessionDictionary[@"guid"]];
                        self.market = market;
                        self.reservation = reservation;
                        self.bike = self.market.bikes[[[NSUUID alloc] initWithUUIDString:sessionDictionary[@"bike"][@"guid"]]];
                        self.startedAt = [dateFormatter dateFromString:sessionDictionary[@"started_at"]];
                        self.totalPrice = 0;
                        self.allowOutsideMarket = sessionDictionary[@"end_outside_market"];
                        if ([sessionDictionary objectForKey:@"ended_at"] != [NSNull null]) { self.ended = @YES; }
                        else { self.ended = @NO; }
                        [self startTimer];

                        self.trips = [[NSMutableArray alloc] init];
                        for (NSDictionary *tripDict in sessionDictionary[@"trip_set"]){
                            Trip *trip = [[Trip alloc] initWithDictionary:tripDict andSession:self];
                            [self.trips addObject:trip];
                        }

                        successBlock(self);
                    }
                    andFailure:failureBlock];
         } andFailure:failureBlock];
}

- (instancetype)initWithBike:(Bike *)bike {
    self.bike = bike;
    self.market = bike.market;
    self.totalPrice = 0;
    self.allowOutsideMarket = @NO;
    self.startedAt = [[NSDate alloc] init];
    self.trips = [[NSMutableArray alloc] init];
    self.ended = @NO;
    return self;
}

- (void)beginWithSuccess:(void (^)(Session *))successBlock
              andFailure:(void (^)(NSError *))failureBlock {
    [self.bike unlockWithSuccess:^(LockEvent *lockEvent){
        [self startTimer];
        self.guid = lockEvent.trip.sessionGuid;
        lockEvent.trip.session = self;
        [self.trips addObject:lockEvent.trip];
        successBlock(self);
    } andFailure:failureBlock];
}

- (void)holdWithinMarket:(void (^)(Session *))successBlock
        andOutsideMarket:(void (^)(Session *))outsideMarketBlock
              andFailure:(void (^)(NSError *))failureBlock {
    [self.bike lockWithSuccess:^(LockEvent *lockEvent) {
        [self.bike isWithinMarketBounds:^(Bike *bike) {
            Trip *trip = [self getCurrentTrip];
            trip.endedEvent = lockEvent.guid;
            [trip stopTracking];
            successBlock(self);
        } orOutsideMarketBounds:^(Bike *bike) {
            Trip *trip = [self getCurrentTrip];
            trip.endedEvent = lockEvent.guid;
            [trip stopTracking];
            outsideMarketBlock(self);
        } andFailure:failureBlock];
    } andFailure:failureBlock];
}

- (void)endWithSuccess:(void (^)(Session *))successBlock
      andOutsideMarket:(void (^)(Session *))outsideMarketBlock
            andFailure:(void (^)(NSError *))failureBlock {
    self.ended = @YES;
    [self.bike isWithinMarketBounds:^(Bike *bike) {
        [self saveWithSuccess:[self onEndSuccess:successBlock] andFailure:failureBlock];
    } orOutsideMarketBounds:^(Bike *bike){
        if ([self.allowOutsideMarket isEqual:@YES]) {
            [self saveWithSuccess:[self onEndSuccess:successBlock] andFailure:failureBlock];
        } else {
            outsideMarketBlock(self);
        }
    } andFailure:failureBlock];
}

- (void (^)(Session *))onEndSuccess:(void(^)(Session *))block {
    return ^void(Session *session) {
        [self.timer invalidate];
        block(self);
    };
}

- (void)resumeWithSuccess:(void (^)(Session *))successBlock
               andFailure:(void (^)(NSError *))failureBlock {
    [self.bike unlockWithSuccess:^(LockEvent *lockEvent){
        lockEvent.trip.session = self;
        [self.trips addObject:lockEvent.trip];
        successBlock(self);
    } andFailure:failureBlock];
}

- (void)saveWithSuccess:(void(^)(Session *))successBlock
             andFailure:(void(^)(NSError *))failureBlock {
    NSDictionary *params = @{@"ended":self.ended,
                             @"end_outside_market": self.allowOutsideMarket};
    [[RestApiClient sharedInstance] update:URI
                                      uuid:self.guid
                                    params:params
                                   success:[self onSaveSuccess:successBlock]
                                     error:failureBlock];
}

- (void (^)(NSDictionary *))onSaveSuccess:(void(^)(Session *))block {
    return ^void(NSDictionary *sessionDict) { block(self); };
}

- (NSDictionary *)propertiesAsDictionary {
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:
        @{
          @"session_started_at"           : self.startedAt,
          @"session_ended"                : self.ended,
          @"session_allow_outside_market" : self.allowOutsideMarket,
          @"session_elapsed_time"         : [NSNumber numberWithFloat:self.elapsedTime],
          @"session_total_price"          : [NSNumber numberWithInteger:self.totalPrice]
          }];
    [properties addEntriesFromDictionary:[self.market propertiesAsDictionary]];
    [properties addEntriesFromDictionary:[self.bike propertiesAsDictionary]];
    [properties addEntriesFromDictionary:[self.reservation propertiesAsDictionary]];
    return properties;
}

- (Trip *)getCurrentTrip {
    Trip *trip = (Trip *)[self.trips lastObject];
    if ([trip isInProgress]) {
        return trip;
    } else {
        return nil;
    }
}

- (void)startTimer {
    [self updateTimeElapsed];
    if (self.ended != 0) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(updateTimeElapsed)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)updateTimeElapsed {
    self.elapsedTime = [self.startedAt timeIntervalSinceNow];
    self.totalPrice = [self calculateTotalPrice];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"baas.session.update.elapsedTime"
                                                        object:self
                                                      userInfo:nil];
}

- (NSInteger)calculateTotalPrice {
    NSInteger hours = self.elapsedTime/3600;
    return -1 * hours + 1;
}

@end
