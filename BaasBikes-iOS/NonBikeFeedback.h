//
//  NonBikeFeedbackViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/27/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol NonBikeFeedbackDelegate <NSObject>
//- (void)finishedWithFeedbackDetails:(NSDictionary *)feedbackDetails andRideableFlag:(BOOL)bikeRideable;
//- (void)postFeedbackData:(NSDictionary *)feedbackDict;
//- (void)hideReportVC;
//@property BOOL reportIsActive;
//@end

@interface NonBikeFeedback : NSObject

- (void)showNonBikeFeedbackWithNavigationController:(UINavigationController *)navigationController andMarketName:(NSString *)marketName andMarketGUID:(NSString *)marketGUID;

@end
