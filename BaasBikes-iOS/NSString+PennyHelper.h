//
//  NSString+PennyHelper.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PennyHelper)
+ (NSString*)formatAmount:(int) amount;
@end
