//
//  NSString+EmailValidation.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/13/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EmailValidation)
-(BOOL)isValidEmail;
@end
