//
//  EditInfoViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "EditInfoViewController.h"
#import "EditInfoTableViewController.h"
#import "Constants.h"
#import "NetworkingController.h"
#import "Mixpanel.h"

@interface EditInfoViewController ()

#define FIRST_NAME_FIELD_TAG            600
#define LAST_NAME_FIELD_TAG             601
#define EMAIL_FIELD_TAG                 602
#define PHONE_NUMBER_FIELD_TAG          603
#define CURRENT_PASSWORD_FIELD_TAG      604
#define NEW_PASSWORD_FIELD_TAG          605
#define CONFIRM_NEW_PASSWORD_FIELD_TAG  606

@end

@implementation EditInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.edit" properties:nil];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"left_arrow"] style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    
    [backButton setTintColor:ORANGE_TEXT_COLOR];
    
    [self.navigationItem setLeftBarButtonItem:backButton];
    
    [self.view setBackgroundColor:ORANGE_BACKGROUND_COLOR];
    [_myAccountLabel setTextColor:ORANGE_TEXT_COLOR];
    
    [self showScrollerWithText:@"MY ACCOUNT"];
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)hideKeyboard {
    
    [_subTableView.firstNameField resignFirstResponder];
    [_subTableView.lastNameField resignFirstResponder];
    [_subTableView.emailField resignFirstResponder];
    [_subTableView.phoneNumberField resignFirstResponder];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"embed_editInfoTableView"]) {
        EditInfoTableViewController * childViewController = (EditInfoTableViewController *) [segue destinationViewController];
        EditInfoTableViewController *tableView = childViewController;
        
        _subTableView = tableView;
        _subTableView.textDelegate = self;
        // do something with the AlertView's subviews here...
    }
}

- (void)keyboardWillHide:(NSNotification *)n {
    _submitButtonBottomConstraint.constant = 0;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillShow:(NSNotification *)n {
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _submitButtonBottomConstraint.constant = keyboardSize.height + 8;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)didPressSubmit:(id)sender {
    [self validateFields];
}

- (void)submitChanges {
    NetworkingController *nc = [NetworkingController new];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    NSMutableDictionary  *mixpanelProps = [[NSMutableDictionary alloc] init];
    
    NSString *email = [_subTableView.emailField text];
    NSString *firstName = [_subTableView.firstNameField text];
    NSString *lastName = [_subTableView.lastNameField text];
    NSString *phoneNumber = [_subTableView.phoneNumberField text];
    NSString *currentPassword = [_subTableView.currentPasswordField text];
    NSString *password = [_subTableView.passwordField text];
    NSString *confirmNewPassword = [_subTableView.confirmNewPasswordField text];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    
    if ([email length] > 0 && ![email isEqualToString:_subTableView.emailField.placeholder]) {
        [mixpanelProps setObject:@"yes" forKey:@"person_email_changed"];
        [params setObject:@{
                            @"email": email,
                            } forKey:@"user"];
    }
    if ([firstName length] > 0 && ![firstName isEqualToString:_subTableView.firstNameField.placeholder]){
        [mixpanelProps setObject:@"yes" forKey:@"person_first_name_changed"];
        [params setObject:firstName forKey:@"first_name"];
    }
    if ([lastName length] > 0 && ![lastName isEqualToString:_subTableView.lastNameField.placeholder]){
        [mixpanelProps setObject:@"yes" forKey:@"person_last_name_changed"];
        [params setObject:lastName forKey:@"last_name"];
    }
    if ([phoneNumber length] > 0 && ![phoneNumber isEqualToString:_subTableView.phoneNumberField.placeholder]) {
        [mixpanelProps setObject:@"yes" forKey:@"person_phone_number_changed"];
        [params setObject:phoneNumber forKey:@"phone_number"];
    }
    if ([currentPassword length] > 0 && ![currentPassword isEqualToString:_subTableView.currentPasswordField.placeholder]) {
        
        if ([password isEqualToString:confirmNewPassword]) {
            [mixpanelProps setObject:@"yes" forKey:@"person_password_changed"];
            
            [params setObject:@{
                                @"old_password": currentPassword,
                                @"new_password": password,
                                @"confirm_password": confirmNewPassword
                                } forKey:@"user"];
            
            [nc updateAccountWithParams:params success:^(void){
                
                if ([email length] > 0 && ![email isEqualToString:_subTableView.emailField.placeholder]) {
                    [[NSUserDefaults standardUserDefaults] setObject:email forKey:PERSON_EMAIL_KEY];
                    [mixpanel createAlias:email forDistinctID:mixpanel.distinctId];
                }
                if ([firstName length] > 0 && ![firstName isEqualToString:_subTableView.firstNameField.placeholder]){
                    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:PERSON_FIRST_NAME_KEY];
                }
                if ([lastName length] > 0 && ![lastName isEqualToString:_subTableView.lastNameField.placeholder]){
                    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:PERSON_LAST_NAME_KEY];
                }
                if ([phoneNumber length] > 0 && ![phoneNumber isEqualToString:_subTableView.phoneNumberField.placeholder]) {
                    [[NSUserDefaults standardUserDefaults] setObject:phoneNumber forKey:PERSON_PHONE_NUMBER_KEY];
                }
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"User info has been updated");
                [self dismissViewControllerAnimated:YES completion:nil];
                [mixpanel track:@"account.edit.submitted" properties:mixpanelProps];
            }failure:^(NSString *errorString) {
                NSLog(@"Error updating user info: %@", errorString);
            }];
        }else{
            [self showAlertWithTitle:@"Invalid Password" andDescription:@"Make sure the two fields for your new password are identical."];
        }
    }else{
        
        [nc updateAccountWithParams:params success:^(void){
            
            if ([email length] > 0 && ![email isEqualToString:_subTableView.emailField.placeholder]) {
                [[NSUserDefaults standardUserDefaults] setObject:email forKey:PERSON_EMAIL_KEY];
                [mixpanel createAlias:email forDistinctID:mixpanel.distinctId];
            }
            if ([firstName length] > 0 && ![firstName isEqualToString:_subTableView.firstNameField.placeholder]){
                [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:PERSON_FIRST_NAME_KEY];
            }
            if ([lastName length] > 0 && ![lastName isEqualToString:_subTableView.lastNameField.placeholder]){
                [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:PERSON_LAST_NAME_KEY];
            }
            if ([phoneNumber length] > 0 && ![phoneNumber isEqualToString:_subTableView.phoneNumberField.placeholder]) {
                [[NSUserDefaults standardUserDefaults] setObject:phoneNumber forKey:PERSON_PHONE_NUMBER_KEY];
            }
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSLog(@"User info has been updated");
            [self dismissViewControllerAnimated:YES completion:nil];
            [mixpanel track:@"account.edit.submitted" properties:mixpanelProps];
            [mixpanel.people set:@{
                                   @"$first_name" : firstName,
                                   @"$last_name"  : lastName,
                                   @"$email"      : email,
                                   @"$phone"      : phoneNumber
                                   }];
        }failure:^(NSString *errorString) {
            NSLog(@"Error updating user info: %@", errorString);
            [self showAlertWithTitle:@"Failed To Update Info" andDescription:@"Please check to make sure all of the information provided is correct and try again."];
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"THIS TEXT FIELD");
    
    UITableViewCell *cell = (UITableViewCell*) textField.superview.superview;
    NSIndexPath *indexPath = [_subTableView.tableView indexPathForCell:cell];
    
    [_subTableView.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    switch (textField.tag) {
        case FIRST_NAME_FIELD_TAG:
            [_subTableView.lastNameField becomeFirstResponder];
            break;
        case LAST_NAME_FIELD_TAG:
            [_subTableView.emailField becomeFirstResponder];
            break;
        case EMAIL_FIELD_TAG:
            [_subTableView.phoneNumberField becomeFirstResponder];
            break;
        case PHONE_NUMBER_FIELD_TAG:
            [_subTableView.currentPasswordField becomeFirstResponder];
            break;
        case CURRENT_PASSWORD_FIELD_TAG:
            [_subTableView.passwordField becomeFirstResponder];
            break;
        case NEW_PASSWORD_FIELD_TAG:
            [_subTableView.confirmNewPasswordField becomeFirstResponder];
            break;
        case CONFIRM_NEW_PASSWORD_FIELD_TAG:
            [_subTableView.confirmNewPasswordField resignFirstResponder];
            break;
        default:
            break;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [textField setTextColor:[UIColor blackColor]];
    
    if (textField.tag == PHONE_NUMBER_FIELD_TAG) {
        
        NSInteger length = [self getLength:textField.text];
        
        if ([textField.text hasPrefix:@"1"]) {
            if(length == 11)
            {
                if(range.length == 0)
                    return NO;
            }
            if(length == 4)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"%@ (%@) ",[num substringToIndex:1],[num substringFromIndex:1]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:4]];
            }
            else if(length == 7)
            {
                NSString *num = [self formatNumber:textField.text];
                NSRange numRange = NSMakeRange(1, 3);
                textField.text = [NSString stringWithFormat:@"%@ (%@) %@-",[num substringToIndex:1] ,[num substringWithRange:numRange],[num substringFromIndex:4]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
            
        } else {
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:textField.text];
                
                textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        return YES;
    }else{
        return YES;
    }
}

-(NSString*)formatNumber:(NSString*)mobileNumber {
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    
    
    return mobileNumber;
}

-(NSInteger)getLength:(NSString*)mobileNumber {
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    
    return length;
}

- (void)validateFields {
    
    int invalidNumber = 0;
    
//    if (_subTableView.firstNameField.text.length > 200) {
//        invalidNumber++;
//        [_subTableView.firstNameField setTextColor:[UIColor redColor]];
//    }
//    
//    if (_subTableView.lastNameField.text.length > 200) {
//        invalidNumber++;
//        [_subTableView.lastNameField setTextColor:[UIColor redColor]];
//    }
    
    if (_subTableView.emailField.text.length > 0 && ![self validateEmail:_subTableView.emailField.text]) {
        invalidNumber++;
        [_subTableView.emailField setTextColor:[UIColor redColor]];
    }
    
    if (_subTableView.phoneNumberField.text.length > 0 && _subTableView.phoneNumberField.text.length < 14) {
        invalidNumber++;
        [_subTableView.phoneNumberField setTextColor:[UIColor redColor]];
    }
    
    if (_subTableView.passwordField.text.length > 0 && _subTableView.passwordField.text.length < 6) {
        invalidNumber++;
        [_subTableView.passwordField setTextColor:[UIColor redColor]];
    }
    
    if (invalidNumber == 0) {
        
        [self submitChanges];
        
    }else if (invalidNumber == 1) {
//        if (_subTableView.firstNameField.text.length < 1) {
//            [self showAlertWithTitle:@"Invalid Name" andDescription:@"Please provide your first name."];
//        }
//        
//        if (_subTableView.lastNameField.text.length < 1) {
//            [self showAlertWithTitle:@"Invalid Name" andDescription:@"Please provide your last name."];
//        }
        
        if (_subTableView.emailField.text.length > 0 && ![self validateEmail:_subTableView.emailField.text]) {
            [self showAlertWithTitle:@"Invalid Email" andDescription:@"Please provide a valid email address."];
        }
        
        if (_subTableView.phoneNumberField.text.length > 0 && _subTableView.phoneNumberField.text.length < 14) {
            [self showAlertWithTitle:@"Invalid Phone Number" andDescription:@"Please provide a valid phone number."];
        }
        
        if (_subTableView.passwordField.text.length > 0 && _subTableView.passwordField.text.length < 6) {
            [self showAlertWithTitle:@"Invalid Password" andDescription:@"Please provide a new password with a minimum of 6 characters."];
        }
    }else{
        [self showAlertWithTitle:@"Invalid Entries" andDescription:@"You have provided two or more invalid entries for account creation."];
    }
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
    
@end







