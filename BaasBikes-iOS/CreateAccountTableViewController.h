//
//  CreateAccountTableViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccountTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *attributionSelector;

@property (weak, nonatomic) id<UITextFieldDelegate> textDelegate;
@property (weak, nonatomic) id<UIPickerViewDelegate> pickerDelegate;

@end
