//
//  Environment.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/4/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "Environment.h"

#ifdef DEBUG
static BOOL debug = YES;
#else
static BOOL debug = NO;
#endif

@implementation Environment

+ (instancetype)sharedInstance {
    static Environment *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[Environment alloc] init];
    });

    return this;
}

- (NSDictionary *)configuration {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appStoreReceipt = [[bundle appStoreReceiptURL] lastPathComponent];
    NSString *path;

    if (debug) {
        path = [bundle pathForResource:@"development" ofType:@"plist"];
    } else if ([appStoreReceipt isEqualToString:@"sandboxReceipt"]) {
        path = [bundle pathForResource:@"testflight" ofType:@"plist"];
    } else {
        path = [bundle pathForResource:@"production" ofType:@"plist"];
    }
    return [NSDictionary dictionaryWithContentsOfFile:path];
}

@end
