//
//  ReviewBikeViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RentalViewController.h"

@interface ReviewBikeViewController : RentalViewController

@property (weak, nonatomic) IBOutlet UILabel *rentalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIButton *starOne;
@property (weak, nonatomic) IBOutlet UIButton *starTwo;
@property (weak, nonatomic) IBOutlet UIButton *starThree;
@property (weak, nonatomic) IBOutlet UIButton *starFour;
@property (weak, nonatomic) IBOutlet UIButton *starFive;
@property NSArray *starArray;
@property int rating;

@end
