//
//  Person.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reservation.h"
#import "Session.h"
#import "Market.h"
#import "Trip.h"

@interface Person : NSObject
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) NSString *authToken;
@property (nonatomic, strong) Market *currentMarket;

+ (Person *)getByUUID:(NSUUID *)uuid;

- (instancetype) initWithDictionary:(NSDictionary *)personDictionary;

- (void)activeReservationWithSuccess:(void (^)(Reservation *))successBlock
                          andFailure:(void (^)(NSError *))failureBlock;

- (void)activeSessionWithSuccess:(void (^)(Session *))successBlock
                      andFailure:(void (^)(NSError *))failureBlock;

- (void)activeTripWithSuccess:(void (^)(Trip *))successBlock
                   andFailure:(void (^)(NSError *))failureBlock;

@end
