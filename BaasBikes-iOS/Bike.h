//
//  Bike.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Market.h"
#import "Lock.h"
#import "LockEvent.h"
#import "BikeMapMarker.h"

@class Reservation;

@interface Bike : NSObject <LockDelegate>
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) NSUUID *ambassador;
@property (nonatomic, strong) Market *market;
@property (nonatomic, strong) NSString *withinMarketBounds;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSUUID *imageSet;
@property (nonatomic, strong) NSString *brand;
@property (nonatomic, strong) NSString *style;
@property (nonatomic, strong) NSString *size;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *renterCount;
@property (nonatomic, strong) NSDictionary *details;
@property (nonatomic, strong) CLLocation *lastLocation;
@property (nonatomic, strong) Lock *bikeLock;

@property (nonatomic, strong) BikeMapMarker *marker;

+ (NSMutableDictionary *)dictionaryFromArray:(NSArray *)bikeDicts;
+ (NSMutableDictionary *)dictionaryFromArray:(NSArray *)bikeDicts forMarket:(Market *)market;
+ (Bike *)getByUUID:(NSUUID *)uuid;

- (instancetype)initWithDictionary:(NSDictionary *)bikeDict
                         andMarket:(Market *)market;

- (void)updateWithDictionary:(NSDictionary *)bikeDict;

- (void)isWithinMarketBounds:(void (^)(Bike *))withinMarketBlock
       orOutsideMarketBounds:(void (^)(Bike *))outsideMarketBlock
                  andFailure:(void (^)(NSError *))failureBlock;

- (void)reserveWithSuccess:(void (^)(Reservation *))successBlock
                andFailure:(void (^)(NSError *))failureBlock;

- (void)lockWithSuccess:(void (^)(LockEvent *))successBlock
             andFailure:(void (^)(NSError *))failureBlock;

- (void)unlockWithSuccess:(void (^)(LockEvent *))successBlock
               andFailure:(void (^)(NSError *))failureBlock;

- (void)releaseChainWithSuccess:(void (^)(LockEvent *))successBlock
                     andFailure:(void (^)(NSError *))failureBlock;

- (void)select;

- (void)deselect;

- (BOOL)isSelected;

- (BOOL)isAvailable;

- (NSDictionary *)propertiesAsDictionary;

@end