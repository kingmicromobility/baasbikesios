//
//  LoginViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/11/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginTableViewController.h"

@interface LoginViewController : BaseViewController<UITextFieldDelegate>

- (IBAction)pressedXButton;
- (IBAction)pressedBackButton;
- (IBAction)pressedLoginButton;
- (IBAction)pressedForgotPasswordButton;

@property (weak, nonatomic) LoginTableViewController *ltv;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsToBottomSpacing;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end
