//
//  RentalViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RentalFlowNavigationController.h"

@interface RentalViewController : UIViewController

- (void)pushRentalFlowVCForState:(RENTAL_FLOW_STATE)state;

- (IBAction)didPressRepair:(id)sender;
- (IBAction)didPressInfo:(id)sender;
- (void)dismissSelfAndClearRentalState;

- (void)startLoading;
- (void)startLoadingWithMessage:(NSString *)message;
- (void)updateLoadingMessage:(NSString *)message;
- (void)stopLoading;

@end
