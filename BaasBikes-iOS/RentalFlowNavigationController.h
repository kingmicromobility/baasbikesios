//
//  RentalFlowNavigationController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityIndicatorViewController.h"

typedef enum {
    kRENTAL_FLOW_STATE_RESERVE_BIKE_VC,
    kRENTAL_FLOW_STATE_BIKE_RESERVED_VC,
    kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC,
    kRENTAL_FLOW_STATE_HOLD_VC,
    kRENTAL_FLOW_STATE_REVIEW_BIKE_VC
} RENTAL_FLOW_STATE;

@interface RentalFlowNavigationController : UINavigationController

@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) ActivityIndicatorViewController *activityIndicator;

- (void)startLoading;
- (void)startLoadingWithMessage:(NSString *)message;
- (void)updateLoadingMessage:(NSString *)message;
- (void)stopLoading;

- (void)pushRentalFlowVCForState:(RENTAL_FLOW_STATE)state;

@end
