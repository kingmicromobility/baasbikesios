//
//  BaasPickerLabel.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/14/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "BaasPickerLabel.h"

@implementation BaasPickerLabel

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {15, 15, 15, 15};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

@end
