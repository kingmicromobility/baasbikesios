//
//  LoginTableViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/12/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) id<UITextFieldDelegate> textDelegate;

@end
