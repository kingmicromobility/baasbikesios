//
//  WalkthroughContentViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "WalkthroughContentViewController.h"
#import "Constants.h"
#import "Mixpanel.h"

@interface WalkthroughContentViewController ()

@end

@implementation WalkthroughContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _walkthroughImageView.image = [UIImage imageNamed:_imageFile];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PAGE_INDEX_CHANGED_NOTIFICATION object:self userInfo:@{@"page": @(_pageIndex)}];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];

    switch (_pageIndex) {
        case 0:
            [mixpanel track:@"tutorial.page.1.reserve.viewed" properties:nil];
            break;
        case 1:
            [mixpanel track:@"tutorial.page.2.unlock.viewed" properties:nil];
            break;
        case 2:
            [mixpanel track:@"tutorial.page.3.adjust-seat.viewed" properties:nil];
            break;
        case 3:
            [mixpanel track:@"tutorial.page.4.hold.viewed" properties:nil];
            break;
        case 4:
            [mixpanel track:@"tutorial.page.5.geofence.viewed" properties:nil];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
