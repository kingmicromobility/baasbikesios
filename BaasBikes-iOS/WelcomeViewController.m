//
//  WelcomeViewController.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 8/14/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "WelcomeViewController.h"
#import "CustomAlertViewController.h"
#import "NetworkingController.h"
#import "BraintreeCard.h"
#import "Constants.h"
#import "Mixpanel.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_FIRST_NAME_KEY];
    [self.firstNameLabel setText:[NSString stringWithFormat:@"%@!", firstName]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)addPaymentButtonPressed:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.collectPostalCode = YES;
    [self presentViewController:scanViewController animated:YES completion:^{
        [[Mixpanel sharedInstance] track:@"account.payment.add"];
    }];
}

- (IBAction)skipPaymentButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [[Mixpanel sharedInstance] track:@"person.welcome.skipped-payment"];
    }];
}


#pragma mark - CardIOPaymentViewControllerDelegate


- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    NSLog(@"User canceled payment info");
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info
             inPaymentViewController:(CardIOPaymentViewController *)scanViewController {

    void (^successBlock)() = ^{
        [scanViewController dismissViewControllerAnimated:YES completion:^{
            [[Mixpanel sharedInstance] track:@"account.payment.added"];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    };

    void (^failureBlock)(NSString *) = ^(NSString *error){
        [CustomAlertViewController showWithTitle:@"Could not validate card"
                                         message:@"We couldn't validate the card information that you provided. Please try again."
                                   yesButtonText:nil
                                yesButtonHandler:nil
                                    noButtonText:nil
                                 noButtonHandler:nil
                                  dismissHandler:nil
                                     contextView:scanViewController];
        [[Mixpanel sharedInstance] track:@"account.payment.add.error"];
    };

    NetworkingController *nc = [NetworkingController sharedInstance];
    [nc getBTClientTokenWithSuccess:^(NSString *token) {
        [self createBTCard:info
           withClientToken:token
               withSuccess:successBlock
                andFailure:failureBlock];
    } failure:^{
        failureBlock(@"Something went Wrong");
    }];
}


#pragma mark - private methods


- (void)createBTCard:(CardIOCreditCardInfo *)info
         withClientToken:(NSString *)token
             withSuccess:(void (^)())successBlock
              andFailure:(void (^)(NSString *))failureBlock {

    NetworkingController *nc = [NetworkingController sharedInstance];
    BTAPIClient *braintree = [[BTAPIClient alloc] initWithAuthorization:token];
    BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:braintree];
    BTCard *card = [[BTCard alloc] initWithNumber:info.cardNumber
                                  expirationMonth:[@(info.expiryMonth) stringValue]
                                   expirationYear:[@(info.expiryYear) stringValue]
                                              cvv:info.cvv];
    card.postalCode = info.postalCode;
    [cardClient tokenizeCard:card completion:^(BTCardNonce * _Nullable tokenizedCard, NSError * _Nullable error) {
        [nc makeNewPaymentMethodForCurrentUserWithNonce:tokenizedCard.nonce
                                                success:successBlock
                                                failure:failureBlock];
    }];
}

@end
