//
//  TOSViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol TermsDelegate <NSObject>
- (void)acceptedTerms;
@end

@interface TOSViewController : BaseViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)didPressAccept:(id)sender;
- (IBAction)didPressCancel:(id)sender;
- (IBAction)didPressBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *acceptButton;

@property BOOL webViewIsLoaded;

@property (weak, nonatomic) IBOutlet UIView *activityFrameView;

@property (weak, nonatomic) id<TermsDelegate> delegate;

@end
