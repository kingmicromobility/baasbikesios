//
//  HelpViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/28/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Help.h"
#import <ZendeskSDK/ZendeskSDK.h>
#import "Mixpanel.h"

@interface Help()

@end

@implementation Help

- (void)showHelpCenterWithNavigationController:(UINavigationController *)navigationController {
    [[ZDKConfig instance] initializeWithAppId:@"2b6dfaf35876fa3caf4c5979d1f24b42043bd234a0481585" zendeskUrl:@"https://baasbikes.zendesk.com" ClientId:@"mobile_sdk_client_7204bda2847ef58f7363" onSuccess:^() {
        
        [ZDKLogger enable:YES];
        [self setUserIdentity];
        [self configureAppearance];
        
        [ZDKHelpCenter showHelpCenterWithNavController:navigationController filterBySectionId:@"202745648" sectionName:@"Help" layoutGuide:ZDKLayoutRespectTop];
        
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"drawer.left.help" properties:nil];
        
    } onError:^(NSError *error) {
        NSLog(@"Error connecting to zendesk: %@", error);
    }];
}

- (void)setUserIdentity {
    ZDKAnonymousIdentity *identity = [ZDKAnonymousIdentity new];
    [ZDKConfig instance].userIdentity = identity;
}

- (void)configureAppearance {

    [ZDKHelpCenter setNavBarConversationsUIType:ZDKNavBarConversationsUITypeNone];
}

@end
