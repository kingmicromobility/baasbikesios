//
//  Lock.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/4/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Lock;

@protocol LockDelegate <NSObject>
@optional
- (void) didBeginConnecting:(Lock *)lock;
- (void) didConnect:(Lock *)lock;
- (void) didBeginConfirmingCompatibility:(Lock *)lock;
- (void) didConfirmCompatibility:(Lock *)lock;
- (void) didBeginAuthorizing:(Lock *)lock;
- (void) didAuthorize:(Lock *)lock;
- (void) didDisconnect:(Lock *)lock;
- (void) didLock:(Lock *)lock;
- (void) didUnlock:(Lock *)lock;
- (void) failedToConnect:(Lock *)lock;
- (void) failedToLock:(Lock *)lock;
- (void) failedToUnlock:(Lock *)lock;
@end

@interface Lock : NSObject
    @property (nonatomic, strong) id <LockDelegate> delegate;
    @property (nonatomic, strong) NSString *type;
    @property (nonatomic, strong) NSString *urn;
    @property (nonatomic, strong) NSString *unlockMessageTitle;
    @property (nonatomic, strong) NSString *unlockMessageBody;

    - (instancetype) initWithBaasLockId:(NSString *)lockId
                                 andUrn:(NSString *)urn
                           withDelegate:(id<LockDelegate>)delegate;
    - (void) connect;
    - (void) disconnect;
    - (BOOL) isConnected;
    - (void) confirmCompatibility;
    - (BOOL) isCompatible;
    - (void) lock;
    - (void) unlock;
    - (BOOL) isLocked;
@end