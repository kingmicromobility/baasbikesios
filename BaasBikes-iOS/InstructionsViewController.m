//
//  LockBikeViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "InstructionsViewController.h"
#import "Constants.h"
#import "Mixpanel.h"
#import "ApplicationState.h"
#import "RentalViewController.h"
#import "MainViewController.h"

@interface InstructionsViewController()
@property (strong, nonatomic) NSString *captionText;
@property (strong, nonatomic) NSString *buttonText;
@property (strong, nonatomic) UIImage *backgroundImage;
@property (copy) void (^backBlock)(void);
@property (copy) void (^actionBlock)(void);
@end

@implementation InstructionsViewController

- (void) setCaption:(NSString *)captionText
         buttonText:(NSString *)buttonText
    backgroundImage:(UIImage *)backgroundImage
          backBlock:(void (^)(void))backBlock
        actionBlock:(void (^)(void))actionBlock {
    self.captionText = captionText;
    self.buttonText = buttonText;
    self.backgroundImage = backgroundImage;
    self.backBlock = backBlock;
    self.actionBlock = actionBlock;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.backBlock) { [self.backButton setHidden:YES]; }
    [self.actionButton setTitle:self.buttonText forState:UIControlStateNormal];
    [self.caption setText:self.captionText];
    [self.caption setNumberOfLines:0];
    [self.caption setLineBreakMode:NSLineBreakByWordWrapping];
    [self.backgroundImageView setImage:self.backgroundImage];
    Session *currentSession = [ApplicationState sharedInstance].currentSession;
    [[Mixpanel sharedInstance] track:@"session.lock.instructions.viewed" properties:[currentSession propertiesAsDictionary]];
}

- (IBAction)didPressBack:(id)sender {
    if (self.backBlock) { self.backBlock(); }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didPressButton:(id)sender {
    if (self.actionBlock) { self.actionBlock(); }
}

- (void)startLoading {
    [self startLoadingWithMessage:@"Loading"];
}

- (void)startLoadingWithMessage:(NSString *)message {
    self.activityIndicator = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityIndicatorVC"];
    [self.activityIndicator setMessage:message];
    [self.activityIndicator addToView:self.view];
}

@end
