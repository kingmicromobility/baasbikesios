//
//  ConfirmBikePurchaseViewController.h
//  BaasBikes-iOS
//
//  Created by Daniel Christopher on 3/19/17.
//  Copyright © 2017 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmBikePurchaseViewController : UIViewController
- (IBAction)backPressed:(UIButton *)sender;

- (IBAction)confirmPressed:(UIButton *)sender;

@end
