//
//  WelcomeViewController.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 8/14/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardIO.h"

@interface WelcomeViewController : UIViewController<CardIOPaymentViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UILabel *firstNameLabel;

- (IBAction)addPaymentButtonPressed:(id)sender;

- (IBAction)skipPaymentButtonPressed:(id)sender;

@end
