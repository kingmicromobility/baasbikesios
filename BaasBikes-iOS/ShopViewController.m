//
//  ShopViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ShopViewController.h"

@implementation ShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *urlString = @"http://baas-bikes.myshopify.com/products/test-bike";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:urlRequest];
    
    [_segmentNavigationControl setSelectedSegmentIndex:-1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)didPressBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didPressSegmentNavigation:(id)sender {
    if (_segmentNavigationControl.selectedSegmentIndex == 0) {
        [_webView goBack];
    }else{
        [_webView goForward];
    }
}

@end
