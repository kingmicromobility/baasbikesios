//
//  AnswerCode.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 2/24/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import "AnswerCode.h"
#import "RestApiClient.h"

#define URI @"answer_codes/"

@interface AnswerCode()
@property (nonatomic, strong) NSNumber *challengeCode;
@property (nonatomic, strong) Lock *lock;
@end

@implementation AnswerCode

#pragma mark - Lifecycle

- (instancetype) initWithChallengeCode:(NSNumber *)challengeCode
                               andLock:(Lock *)lock {
    self.challengeCode = challengeCode;
    self.lock = lock;
    return self;
}

- (void) getAnswerWithSuccess:(void (^)(NSNumber *))successBlock
                   andFailure:(void (^)(NSError *))failureBlock {
    NSDictionary *params = @{@"challenge_code": self.challengeCode,
                             @"lock": self.lock.urn};
    [[RestApiClient sharedInstance] create:URI
                                    params:params
                                   success:[self onSuccess:successBlock]
                                     error:failureBlock];
}

- (void (^)(NSDictionary *))onSuccess:(void(^)(NSNumber *))block {
    return ^void(NSDictionary *resultsDict) {
        NSNumber *answerCode = resultsDict[@"answer_code"];
        block(answerCode);
    };
}

@end
