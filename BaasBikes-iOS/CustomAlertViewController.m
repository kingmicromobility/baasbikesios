//
//  CustomAlertViewController.m
//  PoolCloud
//
//  Created by John Gazzini on 3/10/15.
//  Copyright (c) 2015 Gazzini Inc. All rights reserved.
//

#import "CustomAlertViewController.h"
#import "KeyboardListener.h"

@interface CustomAlertViewController ()
@property (nonatomic, strong) void (^yesButtonHandler)(void);
@property (nonatomic, strong) void (^noButtonHandler)(void);
@property (nonatomic, strong) void (^dismissHandler)(void);
@property (nonatomic, strong) NSString *titleText;
@property (nonatomic, strong) NSString *messageText;
@property (nonatomic, strong) NSString *yesButtonText;
@property (nonatomic, strong) NSString *noButtonText;
@property (nonatomic, strong) UIView *darkBg;
@property (nonatomic, strong) UIViewController *contextView;
@property (nonatomic, strong) NSLayoutConstraint *hCenter;
@property (nonatomic, strong) NSLayoutConstraint *vCenter;
@property (nonatomic, strong) NSLayoutConstraint *width;
@end

@implementation CustomAlertViewController

+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
        yesButtonText:(NSString *)yesButtonText
     yesButtonHandler:(void (^)(void))yesButtonHandler
         noButtonText:(NSString *)noButtonText
      noButtonHandler:(void (^)(void))noButtonHandler
       dismissHandler:(void (^)(void))dismissHandler
          contextView:(UIViewController *)contextView {
    CustomAlertViewController *alert =
    [[CustomAlertViewController alloc] initWithTitle:title
                                             message:message
                                       yesButtonText:yesButtonText
                                    yesButtonHandler:yesButtonHandler
                                        noButtonText:noButtonText
                                     noButtonHandler:noButtonHandler
                                      dismissHandler:dismissHandler
                                         contextView:contextView];
}

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                yesButtonText:(NSString *)yesButtonText
             yesButtonHandler:(void (^)(void))yesButtonHandler
                 noButtonText:(NSString *)noButtonText
              noButtonHandler:(void (^)(void))noButtonHandler
               dismissHandler:(void (^)(void))dismissHandler
                  contextView:(UIViewController *)contextView {
    return [self initWithNibName:nil
                           title:title
                         message:message
                   yesButtonText:yesButtonText
                yesButtonHandler:yesButtonHandler
                    noButtonText:noButtonText
                 noButtonHandler:noButtonHandler
                  dismissHandler:dismissHandler
                     contextView:contextView];
}

- (instancetype)initWithNibName:(NSString *)nibName
                          title:(NSString *)title
                        message:(NSString *)message
                  yesButtonText:(NSString *)yesButtonText
               yesButtonHandler:(void (^)(void))yesButtonHandler
                   noButtonText:(NSString *)noButtonText
                noButtonHandler:(void (^)(void))noButtonHandler
                 dismissHandler:(void (^)(void))dismissHandler
                    contextView:(UIViewController *)contextView {
    if (nibName) {
        self = [self initWithNibName:nibName bundle:nil];
    } else if (yesButtonText || noButtonText) {
        self = [self initWithNibName:@"CustomAlertViewController" bundle:nil];
    } else {
        self = [self initWithNibName:@"CustomAlertViewControllerNoButtons" bundle:nil];
    }
    self.yesButtonHandler     = yesButtonHandler;
    self.yesButtonText        = yesButtonText;
    self.noButtonHandler      = noButtonHandler;
    self.noButtonText         = noButtonText;
    self.dismissHandler       = dismissHandler;
    self.contextView          = contextView;
    self.messageText          = message;
    self.titleText            = title;

    //Set up the dark background
    self.darkBg = [[UIView alloc] initWithFrame:self.contextView.view.bounds];
    self.darkBg.backgroundColor = [UIColor blackColor];
    self.darkBg.alpha = 0;
    [self.contextView.view addSubview:self.darkBg];

    //Set up the foreground
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contextView.view addSubview:self.view];
    [self.contextView addChildViewController:self];

    //Set up constraints
    self.hCenter = [NSLayoutConstraint constraintWithItem:self.view
                                                attribute:NSLayoutAttributeCenterX
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:self.contextView.view
                                                attribute:NSLayoutAttributeCenterX
                                               multiplier:1.0
                                                 constant:0];
    self.vCenter = [NSLayoutConstraint constraintWithItem:self.view
                                                attribute:NSLayoutAttributeCenterY
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:self.darkBg
                                                attribute:NSLayoutAttributeCenterY
                                               multiplier:1.0
                                                 constant:self.contextView.view.frame.size.height];
    self.width = [NSLayoutConstraint constraintWithItem:self.view
                                              attribute:NSLayoutAttributeWidth
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.contextView.view
                                              attribute:NSLayoutAttributeWidth
                                             multiplier:1.0
                                               constant:-24];
    [self.contextView.view addConstraints:@[self.hCenter, self.vCenter, self.width]];
    [self.contextView.view layoutIfNeeded];

    KeyboardListener *kbListener = [KeyboardListener sharedInstance];
    if (kbListener.keyboardVisible) self.vCenter.constant = -kbListener.keyboardHeight/2;
    else                            self.vCenter.constant = 0;

    [self.contextView.view bringSubviewToFront:self.view];
    [UIView animateWithDuration:0.4 animations:^{
        [self.contextView.view layoutIfNeeded];
        self.darkBg.alpha = 0.8;
    }];

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.xButton   addTarget:self action:@selector(pressedX)   forControlEvents:UIControlEventTouchUpInside];
    [self.yesButton addTarget:self action:@selector(pressedYes) forControlEvents:UIControlEventTouchUpInside];
    [self.noButton  addTarget:self action:@selector(pressedNo)  forControlEvents:UIControlEventTouchUpInside];
    self.view.layer.cornerRadius = 4;
    self.view.layer.masksToBounds = YES;

    [self.titleTextView    setText:self.titleText];
    [self.messageTextView setText:self.messageText];
    [self.yesButton       setTitle:self.yesButtonText forState:UIControlStateNormal];
    [self.noButton        setTitle:self.noButtonText  forState:UIControlStateNormal];

    [self.titleTextView sizeToFit];
    [self.titleTextView layoutIfNeeded];
    self.titleTextView.accessibilityElementsHidden = @NO;
    self.messageTextView.accessibilityElementsHidden = @NO;
    if (self.messageTextView.text == nil) {
        [self.messageTextView setFrame:CGRectMake(0, 0, 0, 0)];
    }else{
        [self.messageTextView sizeToFit];
        [self.messageTextView layoutIfNeeded];
    }
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)pressedX {
    [self removeWithCompletion:self.dismissHandler];
}

- (void)pressedYes {
    [self removeWithCompletion:self.yesButtonHandler];
}

- (void)pressedNo {
    [self removeWithCompletion:self.noButtonHandler];
}

-(CGSize) getContentSize:(UITextView*) textView{
    return [textView sizeThatFits:CGSizeMake(textView.frame.size.width, FLT_MAX)];
}

- (void)removeWithCompletion:(void (^)(void))completion {
    self.vCenter.constant = self.contextView.view.frame.size.height;
    [UIView animateWithDuration:0.4 animations:^{
        self.darkBg.alpha = 0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
        [self.darkBg removeFromSuperview];
    }];
    if (completion) completion();
}


@end
