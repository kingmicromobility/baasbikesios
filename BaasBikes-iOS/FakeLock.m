//
//  FakeLock.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/6/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "FakeLock.h"

@interface FakeLock()
@property (nonatomic, strong) NSString* baasLockId;
@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL compatible;
@property (nonatomic) BOOL locked;
@end

@implementation FakeLock

#pragma mark - Lifecycle

- (instancetype) initWithBaasLockId:(NSString *)lockId
                             andUrn:(NSString *)urn
                       withDelegate:(id<LockDelegate>)delegate {
    self.unlockMessageTitle = FAKE_LOCK_UNLOCK_MESSAGE_TITLE;
    self.unlockMessageBody = FAKE_LOCK_UNLOCK_MESSAGE_BODY;
    self.baasLockId = lockId;
    self.urn = urn;
    self.delegate = delegate;
    self.type = @"fake";
    return self;
}

#pragma mark - Public

- (void) connect {
    NSLog(@"[Fake Lock] This is a fake lock, so we don't actually connect.");
    if ([self.delegate respondsToSelector:@selector(didBeginConnecting:)]) {
        [self.delegate didBeginConnecting:self];
    }
    [self progressNotification:@"Connecting to bike"];
    [NSThread sleepForTimeInterval:1.0]; //pretend to connect to lock
    self.connected = YES;
    [self confirmCompatibility];
    if ([self.delegate respondsToSelector:@selector(didConnect:)]) {
        [self.delegate didConnect:self];
    }
}

- (BOOL) isConnected {
    return self.connected;
}

- (void) disconnect {
    NSLog(@"[Fake Lock] This is a fake lock, so we're logically disconnecting");
    self.connected = NO;
    if ([self.delegate respondsToSelector:@selector(didDisconnect:)]) {
        [self.delegate didDisconnect:self];
    }
    [self progressNotification:@"Disconnecting from bike"];
}

- (void) confirmCompatibility {
    NSLog(@"[Fake Lock] No hardware here, compatibility is guaranteed");
    if ([self.delegate respondsToSelector:@selector(didBeginConfirmingCompatibility:)]) {
        [self.delegate didBeginConfirmingCompatibility:self];
    }
    [NSThread sleepForTimeInterval:0.5]; //pretent to confirm compatibilty
    self.compatible = YES;
    if ([self.delegate respondsToSelector:@selector(didConfirmCompatibility:)]) {
        [self.delegate didConfirmCompatibility:self];
    }
}

- (BOOL) isCompatible {
    return self.compatible;
}

- (void) unlock {
    NSLog(@"[Fake Lock] Unlocking...");
    [self authorize];
    [self progressNotification:@"Unlocking"];
    self.locked = NO;
    if ([self.delegate respondsToSelector:@selector(didUnlock:)]) {
        [self.delegate didUnlock:self];
    }
}

- (void) lock {
    NSLog(@"[Fake Lock] Locking...");
    [self authorize];
    [self progressNotification:@"Locking"];
    self.locked = YES;
    if ([self.delegate respondsToSelector:@selector(didLock:)]) {
        [self.delegate didLock:self];
    }
}

- (BOOL) isLocked {
    return self.locked;
}

- (void) authorize {
    NSLog(@"[Fake Lock] Authorizing...");
    if ([self.delegate respondsToSelector:@selector(didBeginAuthorizing:)]) {
        [self.delegate didLock:self];
    }
    [self progressNotification:@"Authorizing"];
    [NSThread sleepForTimeInterval:1.0]; //pretend to do challenge code handshake
    if ([self.delegate respondsToSelector:@selector(didAuthorize:)]) {
        [self.delegate didAuthorize:self];
    }
}

- (void) progressNotification:(NSString *)message {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadingIndicatorMessageUpdate"
                                                        object:message];
}

@end
