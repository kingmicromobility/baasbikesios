//
//  RideHistoryTableViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RideHistoryTableViewController.h"
#import "Constants.h"
#import "NetworkingController.h"
#import "NSString+PennyHelper.h"
#import "RideHistoryTableViewCell.h"
#import "RideHistoryHeaderTableViewCell.h"
#import "Mixpanel.h"

@interface RideHistoryTableViewController ()

@end

@implementation RideHistoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.view setBackgroundColor:ORANGE_BACKGROUND_COLOR];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self loadRideHistory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of days
    return [_datesWithRides count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rides
    if (!_datesWithRides) {
        return 0;
    }else{
        NSString *dateString = _datesWithRides[section];
        return [_ridesAtDates[dateString] count];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    RideHistoryHeaderTableViewCell *header = [tableView dequeueReusableCellWithIdentifier:@"rideHeader"];
    
    NSLog(@"%@", _headerDates[section]);
    
    NSString *headerTitle = _headerDates[section];
    [header.dateLabel setText:headerTitle];
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RideHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rideCell" forIndexPath:indexPath];
    NSString *dateString = _datesWithRides[indexPath.section];
    
    NSArray *allRidesOnThisDate = _ridesAtDates[dateString];
    NSDictionary *thisRide = allRidesOnThisDate[indexPath.row];
    
    cell.timeDifferenceLabel.text = thisRide[@"timeDifference"];
    cell.timeRangeLabel.text = thisRide[@"timeRange"];
    cell.totalCostLabel.text = thisRide[@"totalCost"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  50.0;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%ldhr %ldm", (long)hours, (long)minutes];
    }else{
        return [NSString stringWithFormat:@"%ldm", (long)minutes];
    }
}

- (NSString *)formatDateToString:(NSDate *)date withDateFormatter:(NSDateFormatter *)dateFormatter {
    [dateFormatter setDateFormat:@"h:mma"];
    NSString *timeString = [[dateFormatter stringFromDate:date] lowercaseString];
    return [timeString substringToIndex:[timeString length] - 1];
}

- (void)gatherCellData:(NSDictionary *)dict withDateFormatter:(NSDateFormatter *)dateFormatter {
    
    NSString *endTimeString = dict[@"ended_at"];
    NSString *startTimeString = dict[@"started_at"];
    
    if ((!endTimeString) || (endTimeString == (NSString *)[NSNull null])) {
        NSLog(@"Session still active.");
    }else{
        
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
        NSDate *endDate = [dateFormatter dateFromString:endTimeString];
        NSDate *startDate = [dateFormatter dateFromString:startTimeString];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:endDate];
        
        NSString *endTime = [self formatDateToString:endDate withDateFormatter:dateFormatter];
        NSString *startTime = [self formatDateToString:startDate withDateFormatter:dateFormatter];
        
        NSString *timeRange = [NSString stringWithFormat:@"%@ - %@", startTime, endTime];
        
        NSTimeInterval timeInterval = [endDate timeIntervalSinceDate:startDate];
        NSString *timeDifference = [self stringFromTimeInterval:timeInterval];
        NSString *totalCost = [NSString stringWithFormat:@"Total Cost %@", [NSString formatAmount:[dict[@"total_price"] intValue]]];
        
        NSDictionary *thisRideDict = @{@"timeRange":timeRange, @"timeDifference":timeDifference, @"totalCost":totalCost};
        
        if (![_ridesAtDates objectForKey:dateString]) {
            NSMutableArray *ridesAtDate = [[NSMutableArray alloc] initWithCapacity:1];
            [ridesAtDate addObject:thisRideDict];
            [_ridesAtDates setObject:ridesAtDate forKey:dateString];
            [_datesWithRides addObject:dateString];
        }
        else {
            NSMutableArray *ridesAtDate = _ridesAtDates[dateString];
            [ridesAtDate addObject:thisRideDict];
        }
    }
}

- (void)getHeaderDatesWithDateFormatter:(NSDateFormatter *)dateFormatter {
    
    _headerDates = [[NSMutableArray alloc] init];
    
    for (NSString *dateString in _datesWithRides) {
        
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString:dateString];
        
        NSString *todayString = [dateFormatter stringFromDate:[NSDate date]];
        NSDate *today = [dateFormatter dateFromString:todayString];
        NSDate *yesterday = [today dateByAddingTimeInterval: -86400.0];
        NSString *yesterdayString = [dateFormatter stringFromDate:yesterday];
        
        if ([todayString isEqualToString:dateString]) {
            [_headerDates addObject:@"Today"];
        }else if ([yesterdayString isEqualToString:dateString]){
            [_headerDates addObject:@"Yesterday"];
        }else{
            NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay | NSCalendarUnitMonth fromDate:date];
            
            NSString *monthString = [[NSString alloc] init];
            
            switch (components.month) {
                case 1:
                    monthString = @"Jan";
                    break;
                case 2:
                    monthString = @"Feb";
                    break;
                case 3:
                    monthString = @"Mar";
                    break;
                case 4:
                    monthString = @"Apr";
                    break;
                case 5:
                    monthString = @"May";
                    break;
                case 6:
                    monthString = @"Jun";
                    break;
                case 7:
                    monthString = @"Jul";
                    break;
                case 8:
                    monthString = @"Aug";
                    break;
                case 9:
                    monthString = @"Sep";
                    break;
                case 10:
                    monthString = @"Oct";
                    break;
                case 11:
                    monthString = @"Nov";
                    break;
                case 12:
                    monthString = @"Dec";
                    break;
                default:
                    break;
            }
            NSString *dateToAdd = [NSString stringWithFormat:@"%@. %ld", monthString, (long)components.day];
            [_headerDates addObject:dateToAdd];
            
        }
    }
}

- (void)loadRideHistory {
    [_rideHistoryVC startLoadingWithMessage:@"Loading ride history"];
    NSLog(@"did start loading");
    NetworkingController *nc = [NetworkingController new];
    [nc getPersonSessionHistoryWithPages:1 success:^(NSArray *sessionArray) {
        _ridesAtDates = [[NSMutableDictionary alloc] initWithCapacity:1];
        _datesWithRides = [[NSMutableArray alloc] initWithCapacity:[sessionArray count]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        for(NSDictionary *rideDict in sessionArray) {
            [self gatherCellData:rideDict withDateFormatter: dateFormatter];
        }
        [_datesWithRides sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        [self getHeaderDatesWithDateFormatter:dateFormatter];
        [self.tableView reloadData];
        
        [_rideHistoryVC stopLoading];
        
        } failure:^(NSString *errorString) {
        // Account for the case where the rides call fails
        NSLog(@"ride array failure");
        [_rideHistoryVC stopLoading];
    }];
}

- (void)getMoreRides{
    
    int sections = (int)[self.tableView numberOfSections];
    int rows = 0;
    for(int i=0; i < sections; i++)
    {
        rows += [self.tableView numberOfRowsInSection:i];
    }
    
    if (rows >= 10) {
        _pagesToGet++;
        NetworkingController *nc = [NetworkingController new];
        [nc getPersonSessionHistoryWithPages:_pagesToGet success:^(NSArray *sessionArray) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            for(NSDictionary *rideDict in sessionArray) {
                [self gatherCellData:rideDict withDateFormatter: dateFormatter];
            }
            [_datesWithRides sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            [self getHeaderDatesWithDateFormatter:dateFormatter];
            [self.tableView reloadData];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"history.nextPage" properties:nil];
            
        } failure:^(NSString *errorString) {
            // Account for the case where the rides call fails
            NSLog(@"ride array failure");
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 80.0) {
        NSLog(@"scrolled past it");
        [self getMoreRides];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
