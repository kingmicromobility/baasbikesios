//
//  DutchRetrofitLock.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/4/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BluetoothCentralManager.h"
#import "Lock.h"

#define BAAS_LOCK_SERVICE_UUID  [CBUUID UUIDWithString:@"195AE58A-437A-489B-B0CD-B7C9C394BAE4"]
#define BAAS_TRANSMIT_CHAR_UUID [CBUUID UUIDWithString:@"5FC569A0-74A9-4FA4-B8B7-8354C86E45A4"]
#define BAAS_RECEIVE_CHAR_UUID  [CBUUID UUIDWithString:@"21819AB0-C937-4188-B0DB-B9621E1696CD"]

@interface DutchRetrofitLock : Lock <BluetoothCentralManagerDelegate, CBPeripheralDelegate>
@property CBCharacteristic *transmit;
@property CBCharacteristic *receive;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *desiredState;
- (void)invokeDelegateAfterAttemptedStateChange;
@end
