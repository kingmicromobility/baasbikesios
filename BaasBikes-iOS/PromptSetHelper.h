//
//  PromptSetHelper.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromptSetHelper : NSObject

+(NSDictionary *)getPromptsForSetNamed:(NSString *)promptSetName;

@end
