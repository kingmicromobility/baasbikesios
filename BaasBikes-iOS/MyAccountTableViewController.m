//
//  MyAccountTableViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "MyAccountTableViewController.h"
#import "CustomAlertViewController.h"
#import "NetworkingController.h"
#import "BraintreeCard.h"
#import "Constants.h"
#import "Mixpanel.h"

typedef enum {
    INFO_CELL=0,
    EDIT_INFO_CELL,
    PAYMENT_CELL,
    PUSH_NOTIFICATIONS_CELL
}MY_ACCOUNT_CELLS;

@interface MyAccountTableViewController ()

@end

@implementation MyAccountTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //TODO: consider doing all of these things in the Storyboard file.
    [_userNameLabel setTextColor:ORANGE_TEXT_COLOR];
    [_userEmailLabel setTextColor: [UIColor grayColor]];
    [_instructionsSwitch setOnTintColor:ORANGE_TEXT_COLOR];
    [_instructionsSwitch addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];
    
    [self populateUIWithAuthenticatedUsersInformation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
}

- (void)populateUIWithAuthenticatedUsersInformation {
    //TODO: consider moving this logic into the parent view controller.
    NSString *email = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_EMAIL_KEY];
    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_FIRST_NAME_KEY];
    NSString *lastName = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_LAST_NAME_KEY];
    
    _nameLabel.text = [NSString stringWithFormat:@"Hi,\n%@ %@", firstName, lastName];
    _emailLabel.text = email;
    
    NSString *cardType = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_CARD_TYPE_KEY];
    NSString *lastFour = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_CARD_LAST_FOUR_KEY];
    
    if (cardType.length == 0 && lastFour.length == 0) {
        [self.creditCardLabel setText:@"No Valid Payment\nOn File"];
    }else{
        NSString *creditCardString = [NSString stringWithFormat:@"%@ Ending In\n%@", cardType, lastFour];
        [self.creditCardLabel setText:creditCardString];
    }

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [_instructionsSwitch setOn:[[userDefaults objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == INFO_CELL) {
        return NO;
    }else{
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case INFO_CELL:
            //do nothing
            break;
        case EDIT_INFO_CELL:
            [self.parentViewController performSegueWithIdentifier:@"editInfo" sender:self];
            break;
        case PAYMENT_CELL:
            [self moveToEditPay];
            break;
        default:
            break;
    }
}

- (void)moveToEditPay {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.collectPostalCode = YES;
    [self presentViewController:scanViewController animated:YES completion:^{
        [[Mixpanel sharedInstance] track:@"account.payment.add"];
    }];
}

- (void)setState:(id)sender{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@([sender isOn]) forKey:PERSON_SHOW_INSTRUCTIONS_KEY];
    if ([sender isOn]) {
        [mixpanel track:@"account.instructions.on" properties:nil];
    }else{
        [mixpanel track:@"account.instructions.off" properties:nil];
    }
}


#pragma mark - CardIOPaymentViewControllerDelegate


- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    NSLog(@"User canceled payment info");
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info
             inPaymentViewController:(CardIOPaymentViewController *)scanViewController {

    void (^successBlock)() = ^{
        [scanViewController dismissViewControllerAnimated:YES completion:^{
            [[Mixpanel sharedInstance] track:@"account.payment.added"];
        }];
    };

    void (^failureBlock)(NSString *) = ^(NSString *error){
        [CustomAlertViewController showWithTitle:@"Could not validate card"
                                         message:@"We couldn't validate the card information that you provided. Please try again."
                                   yesButtonText:nil
                                yesButtonHandler:nil
                                    noButtonText:nil
                                 noButtonHandler:nil
                                  dismissHandler:nil
                                     contextView:scanViewController];
        [[Mixpanel sharedInstance] track:@"account.payment.add.error"];
    };

    NetworkingController *nc = [NetworkingController sharedInstance];
    [nc getBTClientTokenWithSuccess:^(NSString *token) {
        [self createBTCard:info
           withClientToken:token
               withSuccess:successBlock
                andFailure:failureBlock];
    } failure:^{
        failureBlock(@"Something went Wrong");
    }];
}


#pragma mark - private methods


- (void)createBTCard:(CardIOCreditCardInfo *)info
     withClientToken:(NSString *)token
         withSuccess:(void (^)())successBlock
          andFailure:(void (^)(NSString *))failureBlock {

    NetworkingController *nc = [NetworkingController sharedInstance];
    BTAPIClient *braintree = [[BTAPIClient alloc] initWithAuthorization:token];
    BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:braintree];
    BTCard *card = [[BTCard alloc] initWithNumber:info.cardNumber
                                  expirationMonth:[@(info.expiryMonth) stringValue]
                                   expirationYear:[@(info.expiryYear) stringValue]
                                              cvv:info.cvv];
    card.postalCode = info.postalCode;
    [cardClient tokenizeCard:card completion:^(BTCardNonce * _Nullable tokenizedCard, NSError * _Nullable error) {
        [nc makeNewPaymentMethodForCurrentUserWithNonce:tokenizedCard.nonce
                                                success:successBlock
                                                failure:failureBlock];
    }];
}


@end
