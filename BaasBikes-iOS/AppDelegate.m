//
//  AppDelegate.m
//  BaasBikes-iOS
//  Jimmy Butler so ugly bruh look like face you put on totem pole bruh
//  Lookin like a tiki mask bruh
//  Created by Rahul Sundararaman on 7/23/15.
//  Copyright (c) 2015 Baas, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "Environment.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "NetworkingController.h"
#import "Constants.h"
#import "ApplicationState.h"
#import "LocationManager.h"
#import "Mixpanel.h"
#import "Branch.h"

@import GoogleMaps;

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {     
    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *mixpanelToken = config[@"MIXPANEL_TOKEN"];
    [Mixpanel sharedInstanceWithToken:mixpanelToken];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"app.launch.started" properties:launchOptions];
    [mixpanel timeEvent:@"app.launch.finished"];
    return YES;
}


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    __block NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    __block Mixpanel *mixpanel = [Mixpanel sharedInstance];
    Branch *branch = [Branch getInstance];
    [branch initSessionWithLaunchOptions:launchOptions
            andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {

        [mixpanel track:@"app.launch.withLink" properties:params];
        NSDictionary *existingBranchParams = [userDefaults objectForKey:BRANCH_LINK_DATA];
        if (existingBranchParams) {
            NSMutableDictionary *mutableBranchParams = [existingBranchParams mutableCopy];
            [mutableBranchParams addEntriesFromDictionary:params];
            NSLog(@"accumulated deep link data: %@", [mutableBranchParams description]);
            [userDefaults setObject:mutableBranchParams forKey:BRANCH_LINK_DATA];
        } else {
            NSLog(@"deep link data: %@", [params description]);
            [userDefaults setObject:params forKey:BRANCH_LINK_DATA];
        }
    }];
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    [[Branch getInstance] handleDeepLink:url];
    return YES;
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray *))restorationHandler {
    [[Branch getInstance] continueUserActivity:userActivity];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    [mixpanel track:@"app.opened" properties:@{
                                               @"lat":@(location.coordinate.latitude),
                                               @"lng":@(location.coordinate.longitude)
                                              }];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

@end
