//
//  TripInProgressViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "TripInProgressViewController.h"
#import "InstructionsViewController.h"
#import "RentalFlowNavigationController.h"
#import "ApplicationState.h"
#import "Mixpanel.h"
#import "Constants.h"

@interface TripInProgressViewController ()

@end

@implementation TripInProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didPressLock:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]) {
        [self showFirstInstruction];
    } else {
        ApplicationState *appState = [ApplicationState sharedInstance];
        [[Mixpanel sharedInstance] track:@"session.lock.instructions.lock" properties:[appState.currentSession propertiesAsDictionary]];
        [self startLoadingWithMessage:@"Connecting to bike"];
        [appState.currentSession holdWithinMarket:^(Session *session) {
            NSLog(@"INSIDE MARKET");
            appState.currentSession = session;
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.success" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.success" properties:[appState.currentSession propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.hold.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"hold count": @1}];
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_HOLD_VC];
            [self stopLoading];
        } andOutsideMarket:^(Session *session) {
            NSLog(@"OUT OF MARKET");
            appState.currentSession = session;
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.success" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.success" properties:[appState.currentSession propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.hold.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"trip count": @1}];
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_HOLD_VC];
            [self stopLoading];
            [self showOutOfBoundsMessage];
        } andFailure:^(NSError *error) {
            NSLog(@"Error locking bike %@", error);
            // TODO: ALERT THE USER THAT THE LOCK HAS FAILED
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.fail" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.fail" properties:[appState.currentSession propertiesAsDictionary]];
        }];
    }
}

- (void)showFirstInstruction {
    Session *currentSession = [ApplicationState sharedInstance].currentSession;
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"If buying the bike, relock the chain without the bike inside.\nIf returning the bike, make sure the chain is wrapped around the frame."
                    buttonText:@"CONTINUE"
               backgroundImage:[UIImage imageNamed:@"instruction_screen_4"]
                     backBlock:[self firstInstructionBackBlock]
                   actionBlock:[self firstInstructionActionBlock:instructionsVC]];
    ApplicationState *appState = [ApplicationState sharedInstance];
    [[Mixpanel sharedInstance] track:@"session.lock.started"
                          properties:[appState.currentSession propertiesAsDictionary]];
    [[Mixpanel sharedInstance] timeEvent:@"session.lock.success"];
    [[Mixpanel sharedInstance] timeEvent:@"session.lock.fail"];
    [self.navigationController.parentViewController presentViewController:instructionsVC
                                                                 animated:YES
                                                               completion:^{
                                                                   [[Mixpanel sharedInstance] track:@"session.lock.before.instructions.step-one.viewed"
                                                                                         properties:[currentSession propertiesAsDictionary]];
                                                               }];
}

- (void (^)())firstInstructionBackBlock {
    return ^{
        Session *currentSession = [ApplicationState sharedInstance].currentSession;
        [[Mixpanel sharedInstance] track:@"session.lock.before.instructions.step-one.back"
                              properties:[currentSession propertiesAsDictionary]];
    };
}

- (void (^)())firstInstructionActionBlock:(InstructionsViewController *)currentVC {
    return ^{
        Session *currentSession = [ApplicationState sharedInstance].currentSession;
        InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
        [instructionsVC setCaption:@"Insert chain and press button to wake lock"
                        buttonText:@"LOCK BIKE"
                   backgroundImage:[UIImage imageNamed:@"instructions_before_lock_seat_post"]
                         backBlock:[self secondInstructionBackBlock:instructionsVC]
                       actionBlock:[self secondInstructionActionBlock:instructionsVC]];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [[Mixpanel sharedInstance] track:@"session.lock.started"
                              properties:[appState.currentSession propertiesAsDictionary]];
        [[Mixpanel sharedInstance] timeEvent:@"session.lock.success"];
        [[Mixpanel sharedInstance] timeEvent:@"session.lock.fail"];
        [currentVC presentViewController:instructionsVC
                                 animated:YES
                              completion:^{
                                  [[Mixpanel sharedInstance] track:@"session.lock.before.instructions.step-two.viewed"
                                                        properties:[currentSession propertiesAsDictionary]];
                              }];
    };
}

- (void (^)())secondInstructionBackBlock:(InstructionsViewController *)currentVC {
    return ^{
        [currentVC stopLoading];
        Session *currentSession = [ApplicationState sharedInstance].currentSession;
        [[Mixpanel sharedInstance] track:@"session.lock.before.instructions.step-two.back" properties:[currentSession propertiesAsDictionary]];
    };
}

- (void (^)())secondInstructionActionBlock:(InstructionsViewController *)currentVC {
    return ^{
        ApplicationState *appState = [ApplicationState sharedInstance];
        [[Mixpanel sharedInstance] track:@"session.lock.instructions.lock" properties:[appState.currentSession propertiesAsDictionary]];
        [currentVC startLoadingWithMessage:@"Connecting to bike"];
        [appState.currentSession holdWithinMarket:^(Session *session) {
            NSLog(@"INSIDE MARKET");
            appState.currentSession = session;
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.success" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.success" properties:[appState.currentSession propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.hold.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"hold count": @1}];
            [self dismissViewControllerAnimated:YES completion:^{
                [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_HOLD_VC];
                [self stopLoading];
                [self askUserIfAppShouldShowInstructions];
            }];
        } andOutsideMarket:^(Session *session) {
            NSLog(@"OUT OF MARKET");
            appState.currentSession = session;
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.success" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.success" properties:[appState.currentSession propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.hold.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"trip count": @1}];
            [self dismissViewControllerAnimated:YES completion:^{
                [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_HOLD_VC];
                [self stopLoading];
                [self showOutOfBoundsMessage];
            }];
        } andFailure:^(NSError *error) {
            NSLog(@"Error locking bike %@", error);
            // TODO: ALERT THE USER THAT THE LOCK HAS FAILED
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.lock.fail" properties:[appState.currentBike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.lock.fail" properties:[appState.currentSession propertiesAsDictionary]];
        }];
    };
}

- (void)askUserIfAppShouldShowInstructions {
    [CustomAlertViewController showWithTitle:@"Keep showing instructions?"
                                     message:@"Looks like you've got the hang of this! Would you like to keep seeing unlock/lock instructions? Just remember to wake the lock up before pressing \"unlock\" or \"hold\"."
                               yesButtonText:@"No, I've got it!"
                            yesButtonHandler:^{
                                [[NSUserDefaults standardUserDefaults] setObject:@(NO) forKey:PERSON_SHOW_INSTRUCTIONS_KEY];
                                [[Mixpanel sharedInstance] track:@"person.instructions.continue-showing.no"];
                            }
                                noButtonText:@"Yes, coach me."
                             noButtonHandler:^{
                                 [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:PERSON_SHOW_INSTRUCTIONS_KEY];
                                 [[Mixpanel sharedInstance] track:@"person.instructions.continue-showing.yes"];
                             }
                              dismissHandler:^{
                                  [[Mixpanel sharedInstance] track:@"person.instructions.continue-showing.dismissed"];
                              }
                                 contextView:self.parentViewController.navigationController.parentViewController];
    [[Mixpanel sharedInstance] track:@"person.instructions.continue-showing.viewed"];
}

- (void)showOutOfBoundsMessage {
    [CustomAlertViewController showWithTitle:@"End Session Within Blue Area On The Map"
                                     message:@"There is a $25 fee for ending your Rental Session outside the blue area.\n\nDuring the rental session, it's OK to lock and hold outside the blue area."
                               yesButtonText:nil
                            yesButtonHandler:nil
                                noButtonText:nil
                             noButtonHandler:nil
                              dismissHandler:^{
                                      if ([[[NSUserDefaults standardUserDefaults] objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]) {
                                          [self askUserIfAppShouldShowInstructions];
                                      }
                                  }
                                 contextView:self.parentViewController.navigationController.parentViewController];
    Session *session = [ApplicationState sharedInstance].currentSession;
    [[Mixpanel sharedInstance] track:@"session.lock.boundsWarning.viewed" properties:[session propertiesAsDictionary]];
}

@end
