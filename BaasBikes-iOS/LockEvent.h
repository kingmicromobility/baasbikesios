//
//  LockEvent.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lock.h"
#import "Person.h"

@class Bike;

@interface LockEvent : NSObject
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) Bike *bike;
@property (nonatomic, strong) Lock *lock;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) Person *person;
@property (nonatomic, strong) NSDate *dateTime;
@property (nonatomic, strong) Trip *trip;
@property (nonatomic, strong) CLLocation *location;
@property BOOL isOutsideMarket;

+ (LockEvent *)getByUUID:(NSUUID *)uuid;

+ (void)unlockBike:(Bike *)bike
       withSuccess:(void (^)(LockEvent *))successBlock
        andFailure:(void (^)(NSError *))failureBlock;

+ (void)lockBike:(Bike *)bike
     withSuccess:(void (^)(LockEvent *))successBlock
      andFailure:(void (^)(NSError *))failureBlock;

- (void)saveWithSuccess:(void (^)(LockEvent *))successBlock
             andFailure:(void (^)(NSError *))failureBlock;

@end
