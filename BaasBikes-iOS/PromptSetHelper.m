//
//  PromptSetHelper.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "PromptSetHelper.h"
#import "Constants.h"
#import "NetworkingController.h"

@implementation PromptSetHelper

+(NSDictionary *)getPromptsForSetNamed:(NSString *)promptSetName {
    NSArray *allPromptSets = [[NSUserDefaults standardUserDefaults] objectForKey:ALL_PROMPTS_KEY];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@", @"name", promptSetName];
    NSArray *filteredPromptSets = [allPromptSets filteredArrayUsingPredicate:pred];
    if ([filteredPromptSets count]) {
        //hopefully, there will only be one of these:
        return filteredPromptSets[0];
    }
    //TODO: try loading the prompts from the web if they don't exist.
    return nil;
}

@end
