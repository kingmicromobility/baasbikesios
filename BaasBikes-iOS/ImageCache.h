//
//  ImageCache.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 7/23/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageCache : NSObject

+ (instancetype)sharedInstance;

- (void) cacheImageSetsDict:(NSDictionary *)imageSetsDict;

- (void) cacheImageUri:(NSString *)imageUri
               withKey:(NSString *)key
                forSet:(NSUUID *)uuid;

- (UIImage *) getImageWithKey:(NSString *)key
                       forSet:(NSUUID *)uuid;

@end
