//
//  WalkthroughContentViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalkthroughContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *walkthroughImageView;

@property NSUInteger pageIndex;
@property NSString *imageFile;

@end
