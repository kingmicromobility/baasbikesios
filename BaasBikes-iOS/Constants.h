//
//  Constants.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/25/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define PRICE_BUTTON_BACKGROUND_COLOR   [UIColor colorWithRed:0.0/255.0 green:117.0/255.0 blue:140.0/255.0 alpha:0.64]

#define OVERLAY_FILL_COLOR   [UIColor colorWithRed:74.0/255.0 green:144.0/255.0 blue:226.0/255.0 alpha:0.2]
#define OVERLAY_STROKE_COLOR   [UIColor colorWithRed:74.0/255.0 green:144.0/255.0 blue:226.0/255.0 alpha:1.0]

#define ORANGE_TEXT_COLOR   [UIColor colorWithRed:249.0/255.0 green:168.0/255.0 blue:37.0/255.0 alpha:1.0]

#define ORANGE_BACKGROUND_COLOR   [UIColor colorWithRed:254.0/255.0 green:253.0/255.0 blue:249.0/255.0 alpha:1.0]
#define BLUE_BACKGROUND_TEXT_COLOR   [UIColor colorWithRed:20.0/255.0 green:140.0/255.0 blue:163.0/255.0 alpha:1.0]
#define WHITE_INDICATOR_COLOR   [UIColor colorWithRed:1 green:1 blue:1 alpha:1.0]
#define WHITE_INDICATOR_COLOR_ALPHA   [UIColor colorWithRed:1 green:1 blue:1 alpha:0.25]

#define GRAY_PLACEHOLDER_COLOR [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.12]

#define AUTHORIZED_INITIAL_ZOOM        15
#define UNAUTHORIZED_INITIAL_ZOOM       3

//NSLog for debug
//#define NSLog                       //

//URI's
//#define CREATE_ACCOUNT_URI              @"create_account/"
#define PERSON_URI                      @"people/"
#define CONFIRM_LOGIN_URI               @"confirm_login/"
#define LOGIN_USER_URI                  @"api-auth-token/"
#define GET_BIKES_URI                   @"bikes/"
#define MARKETS_URI                     @"markets/"
#define RESERVATION_URI                 @"reservations/"
#define LOCK_EVENTS_URI                 @"lock_events/"
#define BRAINTREE_CLIENT_TOKEN_URI      @"payments/client/"
#define SESSIONS_URI                    @"sessions/"
#define FEEDBACK_PROMPTS_URI            @"feedback_prompt_sets/"
#define FEEDBACK_EVENTS_URI             @"feedback_events/"
#define TEMPORARY_PASSWORD_URI          @"temporary_password/"

//URI's for person-specific requests:
#define PERSON_FORMAT_URI               @"people/%@/"

#define PAYMENT_METHODS_URI             @"payment_methods/"
#define TRIPS_URI                       @"trips/"
#define SESSIONS_URI                    @"sessions/"
#define RESERVATIONS_URI                @"reservations/"

//HTTP Headers
#define AUTH_TOKEN_HTTP_HEADER_KEY      @"Authorization"

//NSUserDefault keys
#define AUTH_TOKEN_NSUSERDEFAULTS_KEY   @"auth_token_header_key_for_nsuserdefaults"
#define PERSON_GUID_KEY                 @"guid_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_FIRST_NAME_KEY           @"first_name_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_LAST_NAME_KEY            @"last_name_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_EMAIL_KEY                @"email_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_PHONE_NUMBER_KEY         @"phone_number_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_CARD_TYPE_KEY            @"card_type_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_CARD_LAST_FOUR_KEY       @"card_last_four_for_person_related_to_current_user_key_nsuserdefaults"
#define PERSON_SHOW_INSTRUCTIONS_KEY    @"show_instructions_for_person_related_to_current_user_key_nsuserdefaults"
#define SESSION_GUID_DEFAULTS_KEY       @"guid_for_currently_active_session_key_nsuserdefaults"
#define ALL_PROMPTS_KEY                 @"all_prompts_from_server_key_nsuserdefaults"
#define CURRENT_RESERVATION_KEY         @"current_reservation_key_nsuserdefaults"
#define CURRENT_SESSION_KEY             @"current_session_key_nsuserdefaults"
#define CURRENT_SESSION_START_TIME_KEY  @"current_session_start_time_key_nsuserdefaults"
#define APP_LAUNCHES_KEY                @"app_launches_key_nsuserdefaults"
#define LOCATION_PERMISSION             @"app_location_permission_nsuserdefaults"
#define BRANCH_LINK_DATA                @"branck_link_data_nsuserdefaults"

//different kinds of prompt-sets.
#define SESSION_END_PROMPTS_KEY         @"session_end_feedback"
#define OUT_OF_SESSION_PROMPTS_KEY      @"out_of_session_problem"
#define IN_SESSION_PROMPTS_KEY          @"in_session_problem"
#define NON_BIKE_PROMPTS_KEY            @"non_bike_feedback"

//NSNotification events
#define USER_LOGGED_IN_NOTIFICATION                 @"user_is_now_logged_in_notification"
#define USER_LOGGED_OUT_NOTIFICATION                @"user_is_now_logged_out_notification"
#define USER_NEEDS_AUTH_NOTIFICATION                @"user_needs_auth_notification"
#define CANCELED_ENTERING_PAYMENT_NOTIFICATION      @"user_canceled_pay_screen_notification"
#define PAGE_INDEX_CHANGED_NOTIFICATION             @"page_index_changed_notification"
#define OUT_OF_BOUNDS_HOLD_ALERT_NOTIFICATION       @"out_of_bounds_hold_alert_notification"
#define OUT_OF_BOUNDS_END_ALERT_NOTIFICATION        @"out_of_bounds_end_alert_notification"
#define ALLOW_OUT_OF_BOUNDS_NOTIFICATION            @"allow_out_of_bounds_notification"
#define DID_RESTORE_APP_STATE_NOTIFICATION          @"did_restore_app_state_notification"

//Google Maps
#define GOOGLE_MAPS_API_KEY             @"AIzaSyByqmiVnnC3dzf8leVxcx9kZbQN9SXLBsI"


// “Other” Prompt Guid: fca8f7e6-d3f6-4f25-9c9b-1e89b4c31fab

#endif /* Constants_h */
