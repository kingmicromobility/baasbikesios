//
//  FakeLock.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/6/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lock.h"

#define FAKE_LOCK_UNLOCK_MESSAGE_TITLE @"This is a Fake Bike."
#define FAKE_LOCK_UNLOCK_MESSAGE_BODY  @"This bike doesn't exist in real life. When you press the unlock button below, you will automatically begin renting this fake bike."

@interface FakeLock : Lock
@end