//
//  SessionInProgressViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RentalViewController.h"

@interface SessionInProgressViewController : RentalViewController

@property (weak, nonatomic) IBOutlet UILabel *rentalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
