//
//  BikeMapMarker.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/16/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "BikeMapMarker.h"
#import "ImageCache.h"
#import "Bike.h"
#import "Constants.h"


@interface BikeMapMarker()
@property (nonatomic, strong) GMSMapView *assignedMap;
@end

@implementation BikeMapMarker 

- (id)initWithBike:(Bike *)bike {
    self = [super init];
    self.bike = bike;
    [self refresh];
    return self;
}

- (void)assignToMap:(GMSMapView *)mapView {
    self.assignedMap = mapView;
    [self refresh];
}

- (void)refresh {
    self.title = self.bike.name;
    self.position = self.bike.lastLocation.coordinate;
    if      ([self.bike isSelected])  [self displaySelected];
    else if ([self.bike isAvailable]) [self displayAvailable];
    else                              [self hideMarker];
}

- (void)refreshPosition {
    self.position = self.bike.lastLocation.coordinate;
}

- (void)displayAvailable {

    [self hideMarker];

    NSString *personGUID = [[NSUserDefaults standardUserDefaults] valueForKey:PERSON_GUID_KEY];
    if ([self.bike.ambassador isEqual: personGUID]) {
        self.icon = [UIImage imageNamed:@"ambassador_pin2"];
    }else{
        self.icon = [UIImage imageNamed:@"pin2"];
    }
    self.groundAnchor = CGPointMake(0.5, 0.88461);
    self.accessibilityLabel = self.title;
    self.zIndex = 0;
    
    [self showMarker];
}

- (void)displaySelected {

    [self hideMarker];

    ImageCache *imageCache = [ImageCache sharedInstance];
    UIImage *bikeThumbnail = [imageCache getImageWithKey:@"thumbnail"
                                                  forSet:self.bike.imageSet];

    if (bikeThumbnail != (id)[NSNull null]) {
        UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"big_pin"]];
        UIImageView *bikePic = [[UIImageView alloc] initWithImage:bikeThumbnail];
        [bikePic setContentMode:UIViewContentModeScaleAspectFit];
        [bikePic setFrame:CGRectMake(30, 15, iconView.frame.size.width - 60, iconView.frame.size.width - 85)];
        bikePic.clipsToBounds = YES;
        [iconView addSubview:bikePic];
        
        UILabel *brandLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, iconView.frame.size.width - 70, iconView.frame.size.width - 20, 15)];
        [brandLabel setText:[NSString stringWithFormat:@"%@", self.bike.brand]];
        [brandLabel setFont:[UIFont systemFontOfSize:10]];
        [brandLabel setTextAlignment:NSTextAlignmentCenter];
        [iconView addSubview:brandLabel];
        
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, iconView.frame.size.width - 55, iconView.frame.size.width - 20, 15)];
        [nameLabel setText:[NSString stringWithFormat:@"'%@'", self.bike.name]];
        [nameLabel setFont:[UIFont systemFontOfSize:10]];
        [nameLabel setTextAlignment:NSTextAlignmentCenter];
        [iconView addSubview:nameLabel];
        
        UILabel *sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, iconView.frame.size.width - 40, iconView.frame.size.width - 60, 15)];
        [sizeLabel setText:[NSString stringWithFormat:@"%@", self.bike.size]];
        [sizeLabel setFont:[UIFont systemFontOfSize:10]];
        [sizeLabel setTextAlignment:NSTextAlignmentCenter];
        [iconView addSubview:sizeLabel];
        self.icon = [self imageFromView:iconView];
    }else{
        self.icon = [UIImage imageNamed:@"big_pin"];
    }
    self.groundAnchor = CGPointMake(0.5, 0.957055);
    self.accessibilityLabel = [NSString stringWithFormat:@"Selected Bike: %@", self.title];
    self.zIndex = 10;
    self.appearAnimation = kGMSMarkerAnimationPop;
    CLLocationCoordinate2D focusLocation = CLLocationCoordinate2DMake(self.position.latitude + 0.0006, self.position.longitude);

    [self showMarker];

    [self.assignedMap animateToLocation:focusLocation];
    [self.assignedMap animateToZoom:17];
}

- (void)hideMarker {
    self.map = nil;
}

- (void)showMarker {
    self.map = self.assignedMap;
}

- (UIImage *)imageFromView:(UIView *) view {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
