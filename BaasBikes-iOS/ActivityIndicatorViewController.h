//
//  ActivityIndicatorView.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 3/7/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ActivityIndicatorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *activityIndicatorLabel;

- (void) addToView:(UIView *)parentView;
- (void) removeFromView;
- (void) setMessage:(NSString *)message;

@end
