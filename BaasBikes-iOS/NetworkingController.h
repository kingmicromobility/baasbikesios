//
//  NetworkingController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/9/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Constants.h"
#import <CoreLocation/CoreLocation.h>

@interface NetworkingController : NSObject

+ (instancetype)sharedInstance;

- (void)attemptToCreateAccountWithEmail:(NSString *)email
                               password:(NSString *)password
                              firstName:(NSString *)firstName
                               lastName:(NSString *)lastName
                            phoneNumber:(NSString *)phoneNumber
                selfReportedChannelCode:(NSString *)selfReportedChannelCode
                autoReportedChannelCode:(NSString *)autoReportedChannelCode
                                success:(void(^)())successBlock
                                failure:(void(^)(NSString *errorString))failureBlock;

- (void)updateAccountWithParams:(NSDictionary *)params success:(void(^)())successBlock failure:(void(^)(NSString *errorString))failureBlock;

- (void)resetPasswordWithEmail:(NSString *)email success:(void(^)())successBlock failure:(void(^)(NSString *errorString))failureBlock;

- (void)attemptToLoginWithUsername:(NSString *)email password:(NSString *)password success:(void(^)())successBlock failure:(void(^)())failureBlock;

//Deprecated
- (void)getBikesWithLocation:(CLLocationCoordinate2D)location success:(void(^)(NSArray *bikeDicts))successBlock failure:(void(^)())failureBlock;

- (void)getMarketWithLocation:(CLLocationCoordinate2D)location success:(void(^)(NSDictionary *marketDict))successBlock failure:(void(^)())failureBlock;

- (void)startReservationWithBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)(NSDictionary *reservationDict))successBlock failure:(void(^)(NSInteger statusCode))failureBlock;

- (void)cancelReservationWithGuid:(NSString *)guid success:(void(^)())successBlock failure:(void(^)())failureBlock;

- (void)unlockBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)())successBlock failure:(void(^)())failureBlock;

- (void)lockBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)())successBlock failure:(void(^)())failureBlock;

- (void)getBTClientTokenWithSuccess:(void(^)(NSString *token))successBlock failure:(void(^)())failureBlock;

- (void)makeNewPaymentMethodForCurrentUserWithNonce:(NSString *)nonce success:(void(^)())successBlock failure:(void(^)(NSString *error))failureBlock;

- (void)getPersonPaymentWithSuccess:(void(^)(NSDictionary *responseObject))successBlock failure:(void(^)())failureBlock;

- (void)getPersonSessionHistoryWithPages:(int)pages success:(void(^)(NSArray *sessionArray))successBlock failure:(void(^)(NSString *errorString))failureBlock;

- (void)getCurrentReservationWithSuccess:(void(^)())successBlock failure:(void(^)())failureBlock;

- (void)getCurrentSessionWithSuccess:(void(^)(NSDictionary *responseObject))successBlock failure:(void(^)())failureBlock;

- (void)getSessionStatusWithSuccess:(void(^)(NSArray *tripArray))successBlock failure:(void(^)())failureBlock;

- (void)endSessionWithBikeInBounds:(BOOL)bikeInBounds success:(void(^)(NSDictionary *endSessionDict))successBlock failure:(void(^)())failureBlock;

- (void)getFeedbackPromptsWithSuccess:(void(^)())successBlock failure:(void(^)())failureBlock;

- (void)postFeedback:(NSDictionary *)feedback withSuccess:(void(^)())successBlock failure:(void(^)())failureBlock;


@end
