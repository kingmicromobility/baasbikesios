//
//  MessageUsInstructionsViewController.h
//  BaasBikes-iOS
//
//  Created by Daniel Christopher on 3/19/17.
//  Copyright © 2017 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageUsInstructionsViewController : UIViewController
- (IBAction)continuePressed:(UIButton *)sender;

@end
