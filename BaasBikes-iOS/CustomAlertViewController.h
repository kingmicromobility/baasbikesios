//
//  CustomAlertViewController.h
//  PoolCloud
//
//  Created by John Gazzini on 3/10/15.
//  Copyright (c) 2015 Gazzini Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomAlertDelegate <NSObject>
- (void)pressedXOnAlertView;
- (void)pressedYesOnAlertView;
- (void)pressedNoOnAlertView;
@end

@interface CustomAlertViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleTextView;
@property (weak, nonatomic) IBOutlet UILabel *messageTextView;
@property (weak, nonatomic) IBOutlet UIButton *xButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonLeadingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsHeightConstraint;


@property (weak, nonatomic) id<CustomAlertDelegate>delegate;

+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
        yesButtonText:(NSString *)yesButtonText
     yesButtonHandler:(void (^)(void))yesButtonHandler
         noButtonText:(NSString *)noButtonText
      noButtonHandler:(void (^)(void))noButtonHandler
       dismissHandler:(void (^)(void))dismissHandler
          contextView:(UIViewController *)contextView;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                yesButtonText:(NSString *)yesButtonText
             yesButtonHandler:(void (^)(void))yesButtonHandler
                 noButtonText:(NSString *)noButtonText
              noButtonHandler:(void (^)(void))noButtonHandler
               dismissHandler:(void (^)(void))dismissHandler
                  contextView:(UIViewController *)contextView;

- (instancetype)initWithNibName:(NSString *)nibName
                          title:(NSString *)title
                        message:(NSString *)message
                  yesButtonText:(NSString *)yesButtonText
               yesButtonHandler:(void (^)(void))yesButtonHandler
                   noButtonText:(NSString *)noButtonText
                noButtonHandler:(void (^)(void))noButtonHandler
                 dismissHandler:(void (^)(void))dismissHandler
                    contextView:(UIViewController *)contextView;

@end
