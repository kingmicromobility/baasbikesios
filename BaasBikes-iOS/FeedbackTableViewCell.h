//
//  FeedbackTableViewCell.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTableViewCell : UITableViewCell

// VVVV Only used for cells other than the final cell

@property (weak, nonatomic) IBOutlet UIImageView *selectionButton;

@property (weak, nonatomic) IBOutlet UILabel *promptLabel;

// VVVV Only used for the final cell

@property (weak, nonatomic) IBOutlet UITextView *descriptionField;

@end
