//
//  LeftMenuViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/29/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "OffsetTableViewController.h"

@interface LeftMenuViewController : BaseViewController<MenuSelectionDelegate>

@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) OffsetTableViewController *embeddedTableView;

- (IBAction)pressedSignIn;


@end
