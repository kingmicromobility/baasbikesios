//
//  MainViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "RentalFlowNavigationController.h"
#import "MapViewController.h"
#import "Constants.h"
#import "ApplicationState.h"

@interface MainViewController ()
@property (nonatomic) BOOL acceptBluetoothStatusNotifications;
@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.acceptBluetoothStatusNotifications = YES;
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Baas_Bike_logo"]];
    [self.navigationItem.leftBarButtonItems setAccessibilityLabel:@"Left Menu Drawer"];
    
    _priceButton.layer.cornerRadius = 3.0;
    _priceButton.layer.masksToBounds = YES;
    
    [self addRentalFlowNavigationVC];
    [self restoreRentalState];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restoreRentalState) name:DID_RESTORE_APP_STATE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bluetoothOnPrompt) name:@"bluetooth_unavailable" object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addRentalFlowNavigationVC {
    
    _rentalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RentalFlowNavigationVC"];
    [self.view addSubview:_rentalVC.view];
    [self addChildViewController:_rentalVC];
    
    [_rentalVC.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSLayoutConstraint *rentalLeadingConstraint = [NSLayoutConstraint constraintWithItem:_rentalVC.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *rentalTrailingConstraint = [NSLayoutConstraint constraintWithItem:_rentalVC.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *rentalTopConstraint = [NSLayoutConstraint constraintWithItem:_rentalVC.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_mapContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *rentalBottomConstraint = [NSLayoutConstraint constraintWithItem:_rentalVC.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    [self.view addConstraints:@[rentalLeadingConstraint, rentalTrailingConstraint, rentalTopConstraint, rentalBottomConstraint]];

}

- (void)showRentalFlowNavigationVC {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _mapContainerBottomConstraint.constant = 200;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideRentalFlowNavigationVC {
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _mapContainerBottomConstraint.constant = 0;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [_rentalVC pushRentalFlowVCForState:kRENTAL_FLOW_STATE_RESERVE_BIKE_VC];
    }];
}

- (void)restoreRentalState {
    ApplicationState *appState = [ApplicationState sharedInstance];
    if (appState.currentBike) {
        if ([appState.currentSession getCurrentTrip]) {
            [_rentalVC pushRentalFlowVCForState:kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC];
        }else if (appState.currentSession){
            [_rentalVC pushRentalFlowVCForState:kRENTAL_FLOW_STATE_HOLD_VC];
        }else if (appState.currentReservation) {
            [_rentalVC pushRentalFlowVCForState:kRENTAL_FLOW_STATE_BIKE_RESERVED_VC];
        }
        [self showRentalFlowNavigationVC];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString* segueName = segue.identifier;
    if ([segueName isEqualToString: @"EmbedMapSegue"]) {
        MapViewController *mapVC = (MapViewController *)[segue destinationViewController];
        mapVC.mainVC = self;
    }
}

- (void) bluetoothOnPrompt {
    if (self.acceptBluetoothStatusNotifications == YES) {
        self.acceptBluetoothStatusNotifications = NO;
        [NSTimer timerWithTimeInterval:2.0
                                target:self
                              selector:@selector(allowBluetoothStatusNotifications)
                              userInfo:nil
                               repeats:NO];
        [self showAlertWithTitle:@"It's Bluetooth Time!"
                  andDescription:@"Baas Bikes uses Bluetooth to communicate with bikes, but it appears your phone's Bluetooth is turned off. Please turn Bluetooth on in your phone's settings."];
    }
}

- (void) allowBluetoothStatusNotifications {
    self.acceptBluetoothStatusNotifications = YES;
}



@end
