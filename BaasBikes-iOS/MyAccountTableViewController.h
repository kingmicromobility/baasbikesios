//
//  MyAccountTableViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardIO.h"

@interface MyAccountTableViewController : UITableViewController<CardIOPaymentViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardLabel;
@property (weak, nonatomic) IBOutlet UISwitch *instructionsSwitch;

@end
