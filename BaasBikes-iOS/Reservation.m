//
//  Reservation.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "NSString+LocationHelper.h"
#import "LocationManager.h"
#import "RestApiClient.h"
#import "Reservation.h"
#import "WKTParser.h"
#import "Session.h"
#import "Market.h"
#import "Bike.h"


#define URI @"reservations/"

@interface Reservation()
@property NSTimer *timer;
@end

@implementation Reservation

+ (Reservation *)getByUUID:(NSUUID *)uuid {
    return nil;
}

- (instancetype)initWithBike:(Bike *)bike {
    self.bike = bike;
    self.market = bike.market;
    self.canceled = @NO;
    return self;
}

- (void)initWithDictionary:(NSDictionary *)reservationDictionary
                andSuccess:(void (^)(Reservation *))successBlock
                andFailure:(void (^)(NSError *))failureBlock {
    NSUUID *marketGuid = [[NSUUID alloc] initWithUUIDString:reservationDictionary[@"market"][@"guid"]];
    [Market getByUUID:marketGuid
          withSuccess:^(Market *market) {
              NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
              [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
              [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
              self.guid = [[NSUUID alloc] initWithUUIDString:reservationDictionary[@"guid"]];
              self.market = market;
              self.bike = self.market.bikes[[[NSUUID alloc] initWithUUIDString:reservationDictionary[@"bike"]]];
              self.location = [WKTParser locationForWktPoint:reservationDictionary[@"location"]];
              self.expiresAt = [dateFormatter dateFromString:reservationDictionary[@"expires_at"]];
              if ([reservationDictionary objectForKey:@"session_started_at"] != [NSNull null]) {
                  self.sessionStartedAt = [dateFormatter dateFromString:reservationDictionary[@"session_started_at"]];
              }
              if ([reservationDictionary objectForKey:@"canceled_at"] != [NSNull null]) { self.canceled = @YES; }
              else { self.canceled = @NO; }
              [self startTimer];
              successBlock(self);
          } andFailure:failureBlock];
}

- (void)beginWithSuccess:(void (^)(Reservation *))successBlock
              andFailure:(void (^)(NSError *))failureBlock {
    self.location = [[LocationManager sharedInstance] getCurrentLocation];
    [self saveWithSuccess:^(Reservation *reservation){
        [self startTimer];
        successBlock(reservation);
    } andFailure:failureBlock];
}

- (void)beginSessionWithSuccess:(void (^)(Session *))successBlock
                     andFailure:(void (^)(NSError *))failureBlock {  
    Session *session = [[Session alloc] initWithBike:self.bike];
    [session beginWithSuccess:^(Session *session){
        [self.timer invalidate];
        successBlock(session);
    } andFailure:failureBlock];
}

- (void)cancelWithSuccess:(void (^)(Reservation *))successBlock
               andFailure:(void (^)(NSError *))failureBlock {
    self.canceled = @YES;
    [self.bike deselect];
    [self saveWithSuccess:^(Reservation *reservation){
        [self.timer invalidate];
        successBlock(reservation);
    } andFailure:failureBlock];
}

- (void)expire {
    [self.bike deselect];
    [self.timer invalidate];
}

- (void)saveWithSuccess:(void(^)(Reservation *))successBlock
             andFailure:(void(^)(NSError *))failureBlock {
    NSDictionary *params = @{
                             @"location":[NSString makeStringFromCoordinate:self.location.coordinate],
                             @"bike":self.bike.guid.UUIDString.lowercaseString,
                             @"canceled":self.canceled
                             };
    
    if (self.guid) {
        [[RestApiClient sharedInstance] update:URI
                                          uuid:self.guid
                                        params:params
                                       success:[self onSaveSuccess:successBlock]
                                         error:failureBlock];
    } else {
        [[RestApiClient sharedInstance] create:URI
                                        params:params
                                       success:[self onSaveSuccess:successBlock]
                                         error:failureBlock];
    }
}

- (void (^)(NSDictionary *))onSaveSuccess:(void(^)(Reservation *))block {
    return ^void(NSDictionary *reservationDict) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];

        self.guid = [[NSUUID alloc] initWithUUIDString:reservationDict[@"guid"]];
        self.expiresAt = [dateFormatter dateFromString:reservationDict[@"expires_at"]];
        block(self);
    };
}

- (NSDictionary *)propertiesAsDictionary {
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:
        @{
          @"reservation_lat"            : [NSNumber numberWithFloat:self.location.coordinate.latitude],
          @"reservation_lng"            : [NSNumber numberWithFloat:self.location.coordinate.longitude],
          @"reservation_canceled"       : self.canceled,
          @"reservation_expires_at"     : self.expiresAt,
          @"reservation_remaining_time" : [NSNumber numberWithFloat:self.remainingTime]
        }];
    [properties addEntriesFromDictionary:[self.market propertiesAsDictionary]];
    [properties addEntriesFromDictionary:[self.bike propertiesAsDictionary]];
    return properties;
}

- (void)startTimer {
    [self updateTimeRemaining];
    BOOL canceled = self.canceled == 0;
    BOOL expired = NSOrderedAscending == [self.expiresAt compare:[NSDate new]];
    BOOL hasSession = self.sessionStartedAt != nil;
    if (!canceled && !expired && !hasSession) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(updateTimeRemaining)
                                                    userInfo:nil
                                                     repeats:YES];
    }
}

- (void)updateTimeRemaining {
    self.remainingTime = [self.expiresAt timeIntervalSinceNow];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"baas.reservation.update.remainingTime"
                                                        object:self
                                                      userInfo:nil];
}

@end