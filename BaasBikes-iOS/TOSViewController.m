//
//  TOSViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "TOSViewController.h"
#import "AFNetworking.h"
#import "Mixpanel.h"

@interface TOSViewController ()

@end

@implementation TOSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _webView.delegate = self;
    [_webView.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
    [_webView.layer setBorderWidth:2.0];
    [_webView setClipsToBounds:YES];
    [_webView.layer setCornerRadius:5.0];

    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"terms_of_service"
                                                         ofType:@"html"];
    NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile
                                                     encoding:NSUTF8StringEncoding
                                                        error:nil];
    [_webView loadHTMLString:htmlString baseURL:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    _webViewIsLoaded = YES;
    [_acceptButton setBackgroundImage:[UIImage imageNamed:@"blue_button"] forState:UIControlStateNormal];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.terms.viewed" properties:nil];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self stopLoading];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.terms.failedToLoad" properties:nil];
}

- (IBAction)didPressAccept:(id)sender {
    if (_webViewIsLoaded) {
        [_delegate acceptedTerms];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"account.terms.accepted" properties:nil];
    }
}

- (IBAction)didPressCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.terms.canceled" properties:nil];
}

- (IBAction)didPressBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.terms.back" properties:nil];
}

@end
