//
//  ApplicationState.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/17/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "Constants.h"
#import "Person.h"
#import "Mixpanel.h"
#import "NetworkingController.h"

@implementation ApplicationState

+ (instancetype)sharedInstance {
    static ApplicationState *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[ApplicationState alloc] init];
    });

    return this;
}

- (void)restoreForPerson:(Person *)person
             withSuccess:(void (^)(ApplicationState *, CLLocation *))successBlock
              andFailure:(void (^)(NSError *))failureBlock {
    NSLog(@"restoring application state...");
    person.authToken = [[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    if(person.guid && person.authToken) {
        [person activeSessionWithSuccess:^(Session *session) {
            if (session) {
                [self setStateWithReservation:session.reservation
                                      session:session
                                       market:session.market
                                         bike:session.bike];
                successBlock(self, location);
            } else {
                [person activeReservationWithSuccess:^(Reservation *reservation) {
                    if (reservation) {
                        [self setStateWithReservation:reservation
                                              session:nil
                                               market:reservation.market
                                                 bike:reservation.bike];
                        successBlock(self, location);
                    }else{
                        [Market nearest:location success:^(Market *market) {
                            [self setStateWithReservation:nil
                                                  session:nil
                                                   market:market
                                                     bike:nil];
                            successBlock(self, location);
                        } failure:failureBlock];
                    }
                } andFailure:failureBlock];
            }
        } andFailure:failureBlock];
    } else {
        [Market nearest:location success:^(Market *market) {
            [self setStateWithReservation:nil
                                  session:nil
                                   market:market
                                     bike:nil];
            successBlock(self, location);
        } failure:failureBlock];
    }
}

- (void)restore {
    NSString *personGuidString = [[NSUserDefaults standardUserDefaults] objectForKey:PERSON_GUID_KEY];
    [[NetworkingController sharedInstance] getFeedbackPromptsWithSuccess:^{
        NSLog(@"got the questions.");
    } failure:^{
        NSLog(@"couldn't get questions.");
    }];
    Person *person = [[Person alloc] init];
    if (personGuidString) {
        person.guid = [[NSUUID alloc] initWithUUIDString:personGuidString];
    }
    [self restoreForPerson:person
               withSuccess:^(ApplicationState *appState, CLLocation *location) {
                   NSLog(@"Restored State");
                   NSMutableDictionary *mpProps = [NSMutableDictionary dictionaryWithDictionary:[appState.currentMarket propertiesAsDictionary]];
                   [mpProps addEntriesFromDictionary:@{@"user_lat": [NSNumber numberWithDouble:location.coordinate.latitude],
                                                       @"user_lng": [NSNumber numberWithDouble:location.coordinate.longitude]
                                                       }];
                   [[Mixpanel sharedInstance] track:@"app.state.restored" properties:mpProps];
                   [[NSNotificationCenter defaultCenter] postNotificationName:DID_RESTORE_APP_STATE_NOTIFICATION object:nil];
               } andFailure:^(NSError *error) {
                   [[Mixpanel sharedInstance] track:@"app.state.restore.failed"];
                   NSLog(@"Error restoring application state: %@", error);
               }];
}

- (void)updateCurrentBike:(Bike *)newCurrentBike {
    Bike *previousCurrentBike = self.currentBike;
    self.currentBike = newCurrentBike;
    [previousCurrentBike.marker refresh];
    [self.currentBike.marker refresh];
}


- (void)setStateWithReservation:(Reservation *)reservation
                        session:(Session *)session
                         market:(Market *)market
                           bike:(Bike *)bike {
    self.currentReservation = reservation;
    self.currentSession = session;
    self.currentMarket = market;
    [self updateCurrentBike:bike];
}

- (void)clearRentalState {
    self.currentReservation = nil;
    self.currentSession = nil;
    [self updateCurrentBike:nil];
}

@end
