//
//  MenuBaseViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/29/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "MenuBaseViewController.h"
#import "Mixpanel.h"
#import "Constants.h"


@interface MenuBaseViewController ()

@end

@implementation MenuBaseViewController

- (void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    
    UINavigationController *contentVC = (UINavigationController *)self.contentViewController;
    [contentVC.navigationBar setBackgroundColor:[UIColor whiteColor]];
    [contentVC.navigationBar setTintColor:ORANGE_TEXT_COLOR];
    [contentVC.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: ORANGE_TEXT_COLOR}];
    MainViewController *mainVC = (MainViewController *)contentVC.viewControllers[0];
    mainVC.menuDelegate = self;
    
    //self.rightMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rightMenuViewController"];
    self.backgroundImage = [UIImage imageNamed:@"Stars"];
    self.delegate = self;
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"drawer.left.open" properties:nil];
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"drawer.left.close" properties:nil];
}


#pragma mark MenuDelegate methods
- (void)setOffset:(float)offset {
    self.contentViewInPortraitOffsetCenterX = offset;
}

@end
