//
//  CreateAccountViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import "CreateAccountTableViewController.h"
#import "TOSViewController.h"

@interface CreateAccountViewController : BaseViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, TermsDelegate>

- (IBAction)pressedXButton;
- (IBAction)pressedBackButton;
- (IBAction)pressedCreateAccountButton;

@property (strong, nonatomic) NSArray *attributionOptions;
@property (strong, nonatomic) NSDictionary *deepLinkData;
@property (weak, nonatomic) CreateAccountTableViewController *catv;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *termsToBottomSpacing;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;

@property (strong, nonatomic) NSString *selfReportedAttributionCode;
@property (strong, nonatomic) NSString *autoReportedAttributionCode;

@end
