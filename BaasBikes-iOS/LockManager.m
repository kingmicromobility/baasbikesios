//
//  LockManager.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/4/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Lock.h"
#import "FakeLock.h"
#import "DutchRetrofitLock.h"
#import "SeatPostLock.h"
#import "LockManager.h"
#import "BluetoothCentralManager.h"

@interface LockManager()
@property (strong, nonatomic) BluetoothCentralManager* btCentralManager;
@end

@implementation LockManager

#pragma mark - Lifecycle

+ (instancetype)sharedInstance {
    static LockManager *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[LockManager alloc] init];
    });

    return this;
}

#pragma mark - Public Methods

- (Lock *)getLockByURN:(NSString *)urn
          withDelegate:(id<LockDelegate>)delegate {
    if (!urn) return nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"baas\\.lock\\.([\\w\\d\\-\\_:]+).([\\w\\d\\-\\_:]+)" options:NSRegularExpressionCaseInsensitive error:nil];
    NSTextCheckingResult *urnMatch = [regex firstMatchInString:urn options:0 range:NSMakeRange(0, [urn length])];
    NSRange lockTypeMatch = [urnMatch rangeAtIndex:1];
    NSRange lockIdMatch = [urnMatch rangeAtIndex:2];
    NSString *lockType = [urn substringWithRange:lockTypeMatch];
    NSString *lockId = [urn substringWithRange:lockIdMatch];
    return [self getLockOfType:lockType
                    withLockId:lockId
                        andUrn:urn
                  withDelegate:delegate];
}

- (Lock *)getLockOfType:(NSString *)type
             withLockId:(NSString *)lockId
                 andUrn:(NSString *)urn
           withDelegate:(id<LockDelegate>)delegate {
    NSLog(@"[Lock Manager] type: %@", type);
    NSLog(@"[Lock Manager] lockId: %@", lockId);

    Lock *lock = nil;
    if ([type isEqualToString:BAAS_FAKE_LOCK]) {
        NSLog(@"[Lock Manager] initializing a fake lock.");
        lock = [[FakeLock alloc] initWithBaasLockId:lockId
                                             andUrn:urn
                                       withDelegate:delegate];
    }
    else if ([type isEqualToString:BAAS_DUTCH_RETROFIT_LOCK]) {
        NSLog(@"[Lock Manager] initializing a dutch-retrofit lock.");
        lock = [[DutchRetrofitLock alloc] initWithBaasLockId:lockId
                                                      andUrn:urn
                                                withDelegate:delegate];
    }
    else if ([type isEqualToString:BAAS_SEAT_POST_LOCK]) {
        NSLog(@"[Lock Manager] initializing a seat-post lock.");
        lock = [[SeatPostLock alloc] initWithBaasLockId:lockId
                                                 andUrn:urn
                                           withDelegate:delegate];
    }

    return lock;
}

@end