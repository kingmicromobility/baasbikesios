//
//  KeyboardListener.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 1/4/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyboardListener : NSObject

@property BOOL keyboardVisible;
@property double keyboardHeight;

+ (instancetype)sharedInstance;

@end
