//
//  LoginViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/11/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "LoginViewController.h"
#import "NSString+EmailValidation.h"
#import "NetworkingController.h"
#import "CustomAlertViewController.h"
#import "ApplicationState.h"
#import "Mixpanel.h"


#define EMAIL_FIELD_TAG     600
#define PASSWORD_FIELD_TAG     601

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.viewed" properties:nil];
    
    // Do any additional setup after loading the view.
    [self showScrollerWithText:@"SIGN IN"];
    [self disableSignInButton];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)validateCrednetials {
    if ([self credentialsMightBeValid]) {
        [self enableSignInButton];
    }
    else {
        [self disableSignInButton];
    }
}

- (BOOL)credentialsMightBeValid {
    if (![_ltv.emailTextField.text isValidEmail]) {
        return NO;
    }
    if ([_ltv.passwordTextField.text length] < 1) {
        return NO;
    }
    return YES;
}

- (void)disableSignInButton {
    _loginButton.alpha = 0.54;
    _loginButton.userInteractionEnabled = NO;
}

- (void)enableSignInButton {
    _loginButton.alpha = 1.0;
    _loginButton.userInteractionEnabled = YES;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"login_tv_embed"]) {
        _ltv = (LoginTableViewController *)[segue destinationViewController];
        _ltv.textDelegate = self;
    }
}


- (IBAction)pressedXButton {
    [self dismissViewControllerAnimated:YES completion:nil];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.canceled" properties:nil];
}

- (void)pressedYesOnAlertView {
    NetworkingController *nc = [[NetworkingController alloc] init];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.forgotPassword.submitted" properties:nil];
    [nc resetPasswordWithEmail:self.customAlertVC.textField.text success:^{
        [self showAlertWithTitle:@"Success" andDescription:@"If the email provided is linked to an account, you will recieve a temporary password in your email shortly."];
    } failure:^(NSString *errorString) {
        NSLog(@"Error on reset password: %@", errorString);
    }];
}

- (void)pressedNoOnAlertView {
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.forgotPassword.canceled" properties:nil];
}

- (IBAction)pressedBackButton {
    [self.navigationController popViewControllerAnimated:YES];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.back" properties:nil];
}


#pragma mark UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.tag == 611) {
        NSLog(@"YUP");
        [self pressedYesOnAlertView];
    }else{
        if (textField.tag == EMAIL_FIELD_TAG) {
            [_ltv.passwordTextField becomeFirstResponder];
        }
        else if (textField.tag == PASSWORD_FIELD_TAG) {
            [_ltv.passwordTextField resignFirstResponder];
            [self performLogin];
        }   
    }
    return NO;
}

- (IBAction)pressedLoginButton {
    [self performLogin];
}

- (IBAction)pressedForgotPasswordButton {
    [self resignFirstResponder];
    [self showForgotPasswordAlert:_ltv.emailTextField.text
                 yesButtonHandler:^{[self pressedYesOnAlertView];}
                  noButtonHandler:^{[self pressedNoOnAlertView];}
                   dismissHandler:nil];
    self.customAlertVC.textField.delegate = self;
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signIn.forgotPassword.viewed" properties:nil];
}

- (void)performLogin {
    if ([self credentialsMightBeValid]) {
        NetworkingController *nc = [NetworkingController sharedInstance];
        [nc attemptToLoginWithUsername:_ltv.emailTextField.text password:_ltv.passwordTextField.text success:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGGED_IN_NOTIFICATION object:nil];
            [GMSServices provideAPIKey:@"AIzaSyByqmiVnnC3dzf8leVxcx9kZbQN9SXLBsI"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"account.signIn.success" properties:nil];
            [mixpanel identify:_ltv.emailTextField.text];
            [[ApplicationState sharedInstance] restore];
            
            NetworkingController *nc = [[NetworkingController alloc] init];
            [nc getFeedbackPromptsWithSuccess:^{
                NSLog(@"got the questions.");
            } failure:^{
                NSLog(@"couldn't get questions.");
            }];
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^{
            [self showAlertWithTitle:@"Invalid Login" andDescription:@"Incorrect email or password."];
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"account.signIn.error" properties:nil];
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)n {
    _termsToBottomSpacing.constant = 0;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillShow:(NSNotification *)n {
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _termsToBottomSpacing.constant = keyboardSize.height;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}





@end
