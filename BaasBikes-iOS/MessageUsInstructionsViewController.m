//
//  MessageUsInstructionsViewController.m
//  BaasBikes-iOS
//
//  Created by Daniel Christopher on 3/19/17.
//  Copyright © 2017 Baas, Inc. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "MenuBaseViewController.h"
#import "MessageUsInstructionsViewController.h"
#import "NonBikeFeedback.h"
#import "Mixpanel.h"
#import "ApplicationState.h"

@interface MessageUsInstructionsViewController ()

@property (nonatomic, assign, getter=hasContacted) BOOL hasContacted;

@end

@implementation MessageUsInstructionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden: YES animated:NO];
}

- (void) viewDidAppear:(BOOL)animated {
    if (self.hasContacted == true) {
        [self.navigationController.presentingViewController dismissViewControllerAnimated:true completion:^{}];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)continuePressed:(UIButton *)sender {
    [self setHasContacted:true];
    
    NonBikeFeedback *nonBikeFeedback = [[NonBikeFeedback alloc] init];
    ApplicationState *appState = [ApplicationState sharedInstance];
    [nonBikeFeedback showNonBikeFeedbackWithNavigationController:self.navigationController andMarketName:appState.currentMarket.name andMarketGUID:appState.currentMarket.guid.UUIDString.lowercaseString];
    
}
@end
