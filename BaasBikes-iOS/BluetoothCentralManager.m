//
//  BluetoothCentralManager.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BluetoothCentralManager.h"

@interface BluetoothCentralManager()
@property id<BluetoothCentralManagerDelegate> delegate;
@property (nonatomic, strong) CBCentralManager *centralManager;
@property NSString *currentLockId;
@end

@implementation BluetoothCentralManager

- (instancetype)initWithDelegate:(id<BluetoothCentralManagerDelegate>)delegate {
    self = [super init];
    if (self) {
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
        self.delegate = delegate;
    }
    return self;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(nonnull CBPeripheral *)peripheral
     advertisementData:(nonnull NSDictionary<NSString *,id> *)advertisementData
                  RSSI:(nonnull NSNumber *)RSSI {
    NSLog(@"[BluetoothCentralManager] Discovered Peripheral: %@", [peripheral name]);
    NSLog(@"[BluetoothCentralManager] w/ Advertized Name: %@", advertisementData[CBAdvertisementDataLocalNameKey]);
    if ([advertisementData[CBAdvertisementDataLocalNameKey] isEqualToString:self.currentLockId]) {
        self.currentLockId = nil;
        [self.centralManager stopScan];
        [self.delegate didFindLockPeripheral:peripheral];
    } else if ([[peripheral name] isEqualToString:self.currentLockId]) {
        self.currentLockId = nil;
        [self.centralManager stopScan];
        [self.delegate didFindLockPeripheral:peripheral];
    }
}

- (void)centralManager:(CBCentralManager *)central
  didConnectPeripheral:(nonnull CBPeripheral *)peripheral {
    NSLog(@"[BluetoothCentralManager] Connected to peripheral: %@", [peripheral name]);
    [self.delegate didConnectLockPeripheral:peripheral];
}

- (void)centralManager:(CBCentralManager *)central
didDisconnectPeripheral:(nonnull CBPeripheral *)peripheral
                 error:(nullable NSError *)error {
    NSLog(@"[BluetoothCentralManager] Disconnected from peripheral: %@", [peripheral name]);
    [self.delegate didDisconnectLockPeripheral:peripheral];
}

#pragma mark - Public

- (void)beginSearchingForLockPeripheral:(NSString *)lockId
                            withService:(CBUUID *)serviceId {
    NSLog(@"[BluetoothCentralManager] Searching for lock: %@", lockId);
    self.currentLockId = lockId;
    [self.centralManager scanForPeripheralsWithServices:nil
                                                options:nil];
}

- (void)connectLockPeripheral:(CBPeripheral *)lockPeripheral {
    NSLog(@"[BluetoothCentralManager] Connecting to Peripheral: %@", [lockPeripheral name]);
    [self.centralManager connectPeripheral:lockPeripheral options:nil];
}

- (void)disconnectLockPeripheral:(CBPeripheral *)lockPeripheral {
    NSLog(@"[BluetoothCentralManager] Disconnecting lock: %@", lockPeripheral);
    [self.centralManager cancelPeripheralConnection:lockPeripheral];
}

#pragma mark - Private

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    switch (self.centralManager.state) {
        case CBCentralManagerStatePoweredOff:
        {
            NSLog(@"[BluetoothCentralManager] State is 'Powered Off'");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bluetooth_unavailable" object:nil];
            break;
        }

        case CBCentralManagerStateUnauthorized:
        {
            // Indicate to user that the iOS device does not support BLE.
            NSLog(@"[BluetoothCentralManager] State is 'Unauthorized'");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bluetooth_unavailable" object:nil];
            break;
        }

        case CBCentralManagerStateUnknown:
        {
            // Wait for another event
            NSLog(@"[BluetoothCentralManager] State is 'Unknown'");
            break;
        }

        case CBCentralManagerStatePoweredOn:
        {
            NSLog(@"[BluetoothCentralManager] State is 'Powered On'");
            break;
        }

        case CBCentralManagerStateResetting:
        {
            NSLog(@"[BluetoothCentralManager] State is 'Resetting'");
            break;
        }
            
        case CBCentralManagerStateUnsupported:
        {
            NSLog(@"[BluetoothCentralManager] State is 'Unsupported'");
            break;
        }
            
        default:
            break;
    }
}


@end
