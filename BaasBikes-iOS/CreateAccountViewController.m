//
//  CreateAccountViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/19/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "NSString+EmailValidation.h"
#import "NetworkingController.h"
#import "ApplicationState.h"
#import "BaasPickerLabel.h"
#import "Mixpanel.h"
#import "Market.h"

#define FIRST_NAME_FIELD_TAG        602
#define LAST_NAME_FIELD_TAG         603
#define PHONE_NUMBER_FIELD_TAG      604
#define EMAIL_FIELD_TAG             600
#define PASSWORD_FIELD_TAG          601

@implementation CreateAccountViewController
- (void)viewDidLoad {
    [super viewDidLoad];

    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signUp.viewed" properties:nil];

    self.deepLinkData = [[NSUserDefaults standardUserDefaults] objectForKey:BRANCH_LINK_DATA];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];

    [self.view addGestureRecognizer:tap];

    [self initializeFormFields];
    [self showScrollerWithBlueText:@"SIGN UP"];
    [self disableCreateAccountButton];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)validateCrednetials {
    if ([self credentialsMightBeValid]) {
        [self enableCreateAccountButton];
    }
    else {
        [self disableCreateAccountButton];
    }
}

- (BOOL)credentialsMightBeValid {
    if (![_catv.emailTextField.text isValidEmail]) {
        return NO;
    }
    if ([_catv.passwordTextField.text length] < 1) {
        return NO;
    }
    return YES;
}

- (void)initializeFormFields {
    __block NSString *deepLinkAttributionCode = self.deepLinkData[@"attribution_code"];
    self.autoReportedAttributionCode = (NSString *) deepLinkAttributionCode;

    ApplicationState *appState = [ApplicationState sharedInstance];
    self.attributionOptions = appState.currentMarket.attributionChannels;

    if (deepLinkAttributionCode && self.attributionOptions.count > 0) {
        NSInteger rowToSelect = 0;
        rowToSelect = [self.attributionOptions indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *attribution_code = (NSString *) obj[@"attribution_code"];
            return [attribution_code isEqualToString:deepLinkAttributionCode];
        }];

        [_catv.attributionSelector selectRow:rowToSelect inComponent:0 animated:NO];
        self.selfReportedAttributionCode = self.attributionOptions[rowToSelect][@"attribution_code"];
    } else {
        self.selfReportedAttributionCode = self.attributionOptions[0][@"attribution_code"];
    }

    [_catv.phoneNumberTextField addTarget:self
                                   action:@selector(dismissKeysIfFull)
                         forControlEvents:UIControlEventEditingChanged];
}

- (void)disableCreateAccountButton {
    //_createAccountButton.alpha = 0.54;
    //_createAccountButton.userInteractionEnabled = NO;
}

- (void)enableCreateAccountButton {
    _createAccountButton.alpha = 1.0;
    _createAccountButton.userInteractionEnabled = YES;
}


- (void)dismissKeyboard {
    [_catv.firstNameTextField resignFirstResponder];
    [_catv.lastNameTextField resignFirstResponder];
    [_catv.emailTextField resignFirstResponder];
    [_catv.passwordTextField resignFirstResponder];
    [_catv.phoneNumberTextField resignFirstResponder];
}

- (void)dismissKeysIfFull {
    if ([_catv.phoneNumberTextField.text hasPrefix:@"1"]) {
        if (_catv.phoneNumberTextField.text.length >= 16) {
            [_catv.phoneNumberTextField resignFirstResponder];
        }
    } else {
        if (_catv.phoneNumberTextField.text.length >= 14) {
            [_catv.phoneNumberTextField resignFirstResponder];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"create_account_tv_embed"]) {
        _catv = (CreateAccountTableViewController *)[segue destinationViewController];
        _catv.textDelegate = self;
        _catv.pickerDelegate = self;
    }
}


- (IBAction)pressedXButton {
    [self dismissViewControllerAnimated:YES completion:nil];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signUp.canceled" properties:nil];
}

- (IBAction)pressedBackButton {
    [self.navigationController popViewControllerAnimated:YES];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signUp.back" properties:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField setTextColor:[UIColor blackColor]];
}

#pragma mark - UIPickerViewDelegate methods

- (void) pickerView:(UIPickerView *)pickerView
       didSelectRow:(NSInteger)row
        inComponent:(NSInteger)component {
    [self dismissKeyboard];
    self.selfReportedAttributionCode = (NSString *) self.attributionOptions[row][@"attribution_code"];
}

- (NSString *) pickerView:(UIPickerView *)pickerView
              titleForRow:(NSInteger)row
             forComponent:(NSInteger)component {
    return self.attributionOptions[row][@"descriptive_phrase"];
}

- (UIView *) pickerView:(UIPickerView *)pickerView
             viewForRow:(NSInteger)row
           forComponent:(NSInteger)component
            reusingView:(UIView *)view {
    BaasPickerLabel *tView = (BaasPickerLabel *)view;
    if (!tView) {
        tView = [[BaasPickerLabel alloc] init];
        [tView setFont:[UIFont systemFontOfSize:14.0 weight:UIFontWeightMedium]];
    }

    tView.text = self.attributionOptions[row][@"descriptive_phrase"];
    return tView;
}

#pragma mark - UIPickerViewDataSource methods

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView
 numberOfRowsInComponent:(NSInteger)component {
    return self.attributionOptions.count;
}


#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    UITableViewCell *cell = (UITableViewCell*) textField.superview.superview;
    NSIndexPath *indexPath = [_catv.tableView indexPathForCell:cell];
    
    [_catv.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    switch (textField.tag) {
        case FIRST_NAME_FIELD_TAG:
            [_catv.lastNameTextField becomeFirstResponder];
            break;
        case LAST_NAME_FIELD_TAG:
            [_catv.emailTextField becomeFirstResponder];
            break;
        case EMAIL_FIELD_TAG:
            [_catv.passwordTextField becomeFirstResponder];
            break;
        case PASSWORD_FIELD_TAG:
            [_catv.phoneNumberTextField becomeFirstResponder];
            break;
        case PHONE_NUMBER_FIELD_TAG:
            [_catv.phoneNumberTextField resignFirstResponder];
            [self createAccount];
            break;
        default:
            break;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [textField setTextColor:[UIColor blackColor]];
    
    if (textField.tag == 604) {
        NSInteger length = [self getLength:textField.text];
        
        if ([textField.text hasPrefix:@"1"]) {
            if(length == 11)
            {
                if(range.length == 0)
                    return NO;
            }
            if(length == 4)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"%@ (%@) ",[num substringToIndex:1],[num substringFromIndex:1]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:4]];
            }
            else if(length == 7)
            {
                NSString *num = [self formatNumber:textField.text];
                NSRange numRange = NSMakeRange(1, 3);
                textField.text = [NSString stringWithFormat:@"%@ (%@) %@-",[num substringToIndex:1] ,[num substringWithRange:numRange],[num substringFromIndex:4]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        } else {
            if(length == 10)
            {
                if(range.length == 0)
                    return NO;
            }
            
            if(length == 3)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"(%@) ",num];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
            }
            else if(length == 6)
            {
                NSString *num = [self formatNumber:textField.text];
                textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
                if(range.length > 0)
                    textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
            }
        }
        return YES;
    }else{
        return YES;
    }
}

-(NSString*)formatNumber:(NSString*)mobileNumber {
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    
    
    return mobileNumber;
}

-(NSInteger)getLength:(NSString*)mobileNumber {
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSInteger length = [mobileNumber length];
    
    return length;
}

- (IBAction)pressedCreateAccountButton {
    [self validateFields];
}

- (void)createAccount {
    NSLog(@"trying...");
    if ([self credentialsMightBeValid]) {
        NetworkingController *nc = [NetworkingController sharedInstance];
        [nc attemptToCreateAccountWithEmail:_catv.emailTextField.text
                                   password:_catv.passwordTextField.text
                                  firstName:_catv.firstNameTextField.text
                                   lastName:_catv.lastNameTextField.text
                                phoneNumber:_catv.phoneNumberTextField.text
                    selfReportedChannelCode:self.selfReportedAttributionCode
                    autoReportedChannelCode:self.autoReportedAttributionCode
                                    success:^{
            NSMutableDictionary *mpPersonProps = [NSMutableDictionary dictionaryWithDictionary:@{
                @"$first_name"      : _catv.firstNameTextField.text,
                @"$last_name"       : _catv.lastNameTextField.text,
                @"$email"           : _catv.emailTextField.text,
                @"$phone"           : _catv.phoneNumberTextField.text,
                @"$created"         : [NSDate new],
                @"original market"  : [ApplicationState sharedInstance].currentMarket.name}];
            if (self.selfReportedAttributionCode) {
                [mpPersonProps setObject:self.selfReportedAttributionCode forKey:@"self_reported_attribution_code"];
            }
            if (self.autoReportedAttributionCode) {
                [mpPersonProps setObject:self.autoReportedAttributionCode forKey:@"auto_reported_attribution_code"];
            }
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"account.signUp.success" properties:nil];
            [mixpanel createAlias:_catv.emailTextField.text forDistinctID:mixpanel.distinctId];
            [mixpanel identify:_catv.emailTextField.text];
            [mixpanel.people set:mpPersonProps];

            [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGGED_IN_NOTIFICATION object:nil];
            [self performSegueWithIdentifier:@"welcomesegue" sender:self];
            [self stopLoading];
            [mixpanel track:@"account.welcome.viewed" properties:nil];
        } failure:^(NSString *errorString) {
            [self stopLoading];
            [self showAlertWithTitle:@"Sign Up Failed" andDescription:@"The account failed to be created. Please make sure all information is accurate email provided is not in use."];
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"account.signUp.error" properties:nil];
        }];
    }
}


- (void)keyboardWillHide:(NSNotification *)n {
    _termsToBottomSpacing.constant = 0;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillShow:(NSNotification *)n {
    NSDictionary* userInfo = [n userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _termsToBottomSpacing.constant = keyboardSize.height + 8;
    //TODO: match this better to the actual animation curve.
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)validateFields {
    
    int invalidNumber = 0;
    
    if (_catv.firstNameTextField.text.length < 1) {
        invalidNumber++;
        [_catv.firstNameTextField setTextColor:[UIColor redColor]];
    }
    
    if (_catv.lastNameTextField.text.length < 1) {
        invalidNumber++;
        [_catv.lastNameTextField setTextColor:[UIColor redColor]];
    }
    
    if (![self validateEmail:_catv.emailTextField.text]) {
        invalidNumber++;
        [_catv.emailTextField setTextColor:[UIColor redColor]];
    }
    
    if (_catv.phoneNumberTextField.text.length < 14) {
        invalidNumber++;
        [_catv.phoneNumberTextField setTextColor:[UIColor redColor]];
    }
    
    if (_catv.passwordTextField.text.length < 6) {
        invalidNumber++;
        [_catv.passwordTextField setTextColor:[UIColor redColor]];
    }
    
    if (invalidNumber == 0) {
        
        [self showTerms];
        
    }else if (invalidNumber == 1) {
        if (_catv.firstNameTextField.text.length < 1) {
            [self showAlertWithTitle:@"Invalid Name" andDescription:@"Please provide your first name."];
        }
        
        if (_catv.lastNameTextField.text.length < 1) {
            [self showAlertWithTitle:@"Invalid Name" andDescription:@"Please provide your last name."];
        }
        
        if (![self validateEmail:_catv.emailTextField.text]) {
            [self showAlertWithTitle:@"Invalid Email" andDescription:@"Please provide a valid email address."];
        }
        
        if (_catv.phoneNumberTextField.text.length < 14) {
            [self showAlertWithTitle:@"Invalid Phone Number" andDescription:@"Please provide a valid phone number."];
        }
        
        if (_catv.passwordTextField.text.length < 6) {
            [self showAlertWithTitle:@"Invalid Password" andDescription:@"Please provide a password with a minimum of 6 characters."];
        }
    }else{
        [self showAlertWithTitle:@"Invalid Entries" andDescription:@"You have provided two or more invalid entries for account creation."];
    }
}


- (void)showTerms {
    TOSViewController *tvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TERMS OF SERVICE"];
    tvc.delegate = self;
    [self presentViewController:tvc animated:YES completion:^{
        
    }];
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark TermsDelegate methods
- (void)acceptedTerms {
    [self startLoadingWithMessage:@"Creating your account"];
    [self createAccount];
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
