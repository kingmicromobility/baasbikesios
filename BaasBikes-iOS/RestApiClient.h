//
//  RestApiClient.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define AUTH_TOKEN_HTTP_HEADER_KEY    @"Authorization"
#define AUTH_TOKEN_NSUSERDEFAULTS_KEY @"auth_token_header_key_for_nsuserdefaults"


@interface RestApiClient : NSObject

+ (instancetype)sharedInstance;

- (void)list:(NSString *)uri
      params:(NSDictionary *)params
     success:(void(^)(NSDictionary *))successBlock
       error:(void(^)(NSError *))errorBlock;

- (void)show:(NSString *)uri
        uuid:(NSUUID *)uuid
      params:(NSDictionary *)params
     success:(void(^)(NSDictionary *))successBlock
       error:(void(^)(NSError *))errorBlock;

- (void)create:(NSString *)uri
        params:(NSDictionary *)params
       success:(void(^)(NSDictionary *))successBlock
         error:(void(^)(NSError *))errorBlock;

- (void)update:(NSString *)uri
          uuid:(NSUUID *)uuid
        params:(NSDictionary *)params
       success:(void(^)(NSDictionary *))successBlock
         error:(void(^)(NSError *))errorBlock;

- (AFHTTPRequestOperationManager *)getRequestManager;
- (BOOL)isAuthenticationError:(NSInteger)statusCode;

@end
