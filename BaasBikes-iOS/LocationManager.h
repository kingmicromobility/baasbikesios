//
//  LocationManager.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 7/28/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject<CLLocationManagerDelegate>

+ (instancetype)sharedInstance;

- (CLLocation *) getCurrentLocation;

@end
