//
//  FeedbackTableViewCell.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "FeedbackTableViewCell.h"

@implementation FeedbackTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    if (_descriptionField) {
        _descriptionField.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        CGSize size = _descriptionField.contentSize;
        size.width = size.width - 20;
        [_descriptionField setContentSize:size];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
