//
//  BikeReservedViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RentalViewController.h"

@interface BikeReservedViewController : RentalViewController


@property (weak, nonatomic) IBOutlet UILabel *reservationTimeLabel;

- (IBAction)didPressCancel:(id)sender;
- (IBAction)didPressUnlock:(id)sender;

@end
