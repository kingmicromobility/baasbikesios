//
//  MyAccountViewController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/1/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "MyAccountViewController.h"
#import "Constants.h"
#import "PromptSetHelper.h"
#import "Mixpanel.h"

@interface MyAccountViewController ()

@end

@implementation MyAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"drawer.left.account" properties:nil];
    
    NSLog(@"Feedback prompts: %@", [PromptSetHelper getPromptsForSetNamed:SESSION_END_PROMPTS_KEY]);
    [self.view setBackgroundColor:ORANGE_BACKGROUND_COLOR];
    [self.myAccountLabel setTextColor:ORANGE_TEXT_COLOR];
    [self.signOutButton setBackgroundColor:ORANGE_BACKGROUND_COLOR];
    [self.backButton setTintColor:ORANGE_TEXT_COLOR];
    
    [self showScrollerWithText:@"MY ACCOUNT"];
    
    [self.backButton setTarget:self];
    [self.backButton setAction:@selector(didPushBackButton)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didPushBackButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pushedSignOut {
    [self logout];
    [self dismissViewControllerAnimated:YES completion:nil];
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"account.signOut" properties:nil];
}

@end
