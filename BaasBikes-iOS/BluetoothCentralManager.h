//
//  BluetoothCentralManager.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol BluetoothCentralManagerDelegate <NSObject>
    - (void) didFindLockPeripheral:(CBPeripheral *)peripheral;
    - (void) didConnectLockPeripheral:(CBPeripheral *)peripheral;
    - (void) didDisconnectLockPeripheral:(CBPeripheral *)peripheral;
@end

@interface BluetoothCentralManager : NSObject <CBCentralManagerDelegate>
    - (instancetype)initWithDelegate:(id<BluetoothCentralManagerDelegate>)delegate;
    - (void)beginSearchingForLockPeripheral:(NSString *)lockId
                                withService:(CBUUID *)serviceId;
    - (void)connectLockPeripheral:(CBPeripheral *)lockPeripheral;
    - (void)disconnectLockPeripheral:(CBPeripheral *)lockPeripheral;
@end