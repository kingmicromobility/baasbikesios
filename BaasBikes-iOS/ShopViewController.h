//
//  ShopViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/3/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)didPressBackButton:(id)sender;

- (IBAction)didPressSegmentNavigation:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentNavigationControl;

@end
