//
//  Lock.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/6/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lock.h"

@implementation Lock

- (instancetype) initWithBaasLockId:(NSString *)lockId
                             andUrn:(NSString *)urn
                       withDelegate:(id<LockDelegate>)delegate {
    return nil;
}

- (void) connect {}
- (void) disconnect {}
- (BOOL) isConnected { return NO; }
- (void) confirmCompatibility {}
- (BOOL) isCompatible { return NO; }
- (void) lock {}
- (void) unlock {}
- (BOOL) isLocked { return NO; }
@end