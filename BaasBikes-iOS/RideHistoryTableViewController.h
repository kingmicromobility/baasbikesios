//
//  RideHistoryTableViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideHistoryViewController.h"

@interface RideHistoryTableViewController : UITableViewController

@property NSMutableArray *datesWithRides;
@property NSMutableDictionary *ridesAtDates;
@property NSMutableArray *headerDates;
@property int pagesToGet;
@property RideHistoryViewController *rideHistoryVC;

@end
