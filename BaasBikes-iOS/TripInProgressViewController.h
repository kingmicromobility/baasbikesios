//
//  TripInProgressViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionInProgressViewController.h"

@interface TripInProgressViewController : SessionInProgressViewController

- (IBAction)didPressLock:(id)sender;

@end
