//
//  BaseViewController.m
//  BaasBikes-iOS
//
//  Created by Rahul Sundararaman on 7/23/15.
//  Copyright (c) 2015 Baas, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import "Constants.h"
#import "MenuBaseViewController.h"
#import "ApplicationState.h"
#import "KeyboardListener.h"
#import "Mixpanel.h"
#import "Session.h"

#define TEXT_HEIGHT_FOR_BACKGROUND_TEXT     143

@interface BaseViewController()
@property (strong, nonatomic) UIView *darkeningView;
@end

@implementation BaseViewController {
    UIView *scrollingView;
    NSLayoutConstraint *bottomSpacingForScrollingView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [KeyboardListener sharedInstance];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    [notiCenter addObserver:self selector:@selector(userNeedsAuth) name:USER_NEEDS_AUTH_NOTIFICATION object:nil];
}

- (void)showAlertWithTitle:(NSString *)title andDescription:(NSString *)description {
    [CustomAlertViewController showWithTitle:title
                                     message:description
                               yesButtonText:nil
                            yesButtonHandler:nil
                                noButtonText:nil
                             noButtonHandler:nil
                              dismissHandler:nil
                                 contextView:self];
}

- (void)showYesNoAlertWithTitle:(NSString *)title andDescription:(NSString *)description {
    [CustomAlertViewController showWithTitle:title
                                     message:description
                               yesButtonText:@"Yes"
                            yesButtonHandler:nil
                                noButtonText:@"No"
                             noButtonHandler:nil
                              dismissHandler:nil
                                 contextView:self];
}

- (void)showForgotPasswordAlert:(NSString *)emailText
               yesButtonHandler:(void (^)(void))yesButtonHandler
                noButtonHandler:(void (^)(void))noButtonHandler
                 dismissHandler:(void (^)(void))dismissHandler {
    [self showAlertWithType:kALERT_VIEW_TYPE_FORGOT_PASSWORD
           yesButtonHandler:yesButtonHandler
            noButtonHandler:noButtonHandler
             dismissHandler:dismissHandler];
    [_customAlertVC.textField setText:emailText];
}


- (void)showAlertWithType:(ALERT_VIEW_TYPE)alertType
         yesButtonHandler:(void (^)(void))yesButtonHandler
          noButtonHandler:(void (^)(void))noButtonHandler
           dismissHandler:(void (^)(void))dismissHandler {
    if (_darkeningView) {
        [_darkeningView removeFromSuperview];
        _darkeningView = nil;
        [_customAlertVC removeFromParentViewController];
        [_customAlertVC.view removeFromSuperview];
        _customAlertVC = nil;
    }
    
    _darkeningView = [[UIView alloc] initWithFrame:self.view.bounds];
    _darkeningView.backgroundColor = [UIColor blackColor];
    _darkeningView.alpha = 0;
    [self.view addSubview:_darkeningView];
    
    _activeAlertType = alertType;
    switch (alertType) {
        case kALERT_VIEW_TYPE_TUTORIAL:
            _customAlertVC = [[CustomAlertViewController alloc] initWithNibName:@"TutorialAlertViewController" bundle:nil];
            break;
        case kALERT_VIEW_TYPE_WELCOME:
            _customAlertVC = [[CustomAlertViewController alloc] initWithNibName:@"WelcomeAlertView" bundle:nil];
            break;
        case kALERT_VIEW_TYPE_FORGOT_PASSWORD:
            _customAlertVC = [[CustomAlertViewController alloc] initWithNibName:@"ForgotPasswordAlertView"
                                                                          title:@"Forgot Password"
                                                                        message:@"Enter the email address for your account below and we will send you instructions for resetting your password."
                                                                  yesButtonText:@"Continue"
                                                               yesButtonHandler:yesButtonHandler
                                                                   noButtonText:@"Cancel"
                                                                noButtonHandler:noButtonHandler
                                                                 dismissHandler:dismissHandler
                                                                    contextView:self];
            break;
        default:
            break;
    }
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (void)showScrollerWithBlueText:(NSString *)scrollText {
    UILabel *bigLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 143)];
    bigLabel.font = [UIFont boldSystemFontOfSize:120];
    bigLabel.textColor = BLUE_BACKGROUND_TEXT_COLOR;
    bigLabel.text = scrollText;
    float width = [self widthOfString:scrollText withFont:bigLabel.font];
    [self addScrollerWithView:bigLabel withWidth:width andHeight:TEXT_HEIGHT_FOR_BACKGROUND_TEXT];
}

- (void)showScrollerWithText:(NSString *)scrollText {
    UILabel *bigLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 143)];
    bigLabel.font = [UIFont boldSystemFontOfSize:120];
    bigLabel.textColor = ORANGE_TEXT_COLOR;
    bigLabel.text = scrollText;
    float width = [self widthOfString:scrollText withFont:bigLabel.font];
    [self addScrollerWithView:bigLabel withWidth:width andHeight:TEXT_HEIGHT_FOR_BACKGROUND_TEXT];
}

- (void)showScrollerWithImage:(UIImage *)scrollImage {
    CGSize s = scrollImage.size;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:scrollImage];
    [self addScrollerWithView:imageView withWidth:s.width andHeight:s.height];
}

- (void)addScrollerWithView:(UIView *)scroller withWidth:(float)width andHeight:(float)height {
    [scroller setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:scroller];
    [self.view sendSubviewToBack:scroller];
    
    
    float distanceFromLeft = (TEXT_HEIGHT_FOR_BACKGROUND_TEXT / 2.0) - (width / 2.0);
    
    scroller.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    NSLayoutConstraint *w = [NSLayoutConstraint constraintWithItem:scroller attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
    
    NSLayoutConstraint *h = [NSLayoutConstraint constraintWithItem:scroller attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:height];
    
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:scroller attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:width];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:scroller attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:distanceFromLeft];
    
    [self.view addConstraints:@[w, h, bottom, left]];
    bottomSpacingForScrollingView = bottom;
    scrollingView = scroller;
    [self.view layoutIfNeeded];
    scrollingView.alpha = 0.06;
    [self startAnimatingScroller];
}

- (void)startAnimatingScroller {
    bottomSpacingForScrollingView.constant = -(self.view.frame.size.height + scrollingView.frame.size.height);
    [self.view layoutIfNeeded];
    bottomSpacingForScrollingView.constant = scrollingView.frame.size.width + scrollingView.frame.size.height + 50;
    [UIView animateWithDuration:9 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self startAnimatingScroller];
    }];
}

- (float)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (void)userNeedsAuth {
    if (self.isViewLoaded && self.view.window && self.navigationController.visibleViewController == self) {
        // viewController is visible
        if ([[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY] != nil) {
            // Create a nav controller and give it the signIn/signUp view as the root but push to the signIn view before presenting it.
            UIViewController *signInUpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:signInUpVC];
            [navController setNavigationBarHidden:YES];
            BaseViewController *signInVC = [self.storyboard instantiateViewControllerWithIdentifier:@"sign_in"];
            [navController pushViewController:signInVC animated:false];
            [self presentViewController:navController animated:YES completion:^{
                [self hideMenuIfPossible];
                [signInVC showAlertWithTitle:@"Please Sign In" andDescription:@"Whoops! That action requires that you be signed in. Please sign in and try again."];
            }];
        }else{
            // Present the signIn/signUp controller
            BaseViewController *signInUpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:signInUpVC];
            [navController setNavigationBarHidden:YES];
            [self presentViewController:navController animated:YES completion:^{
                [self hideMenuIfPossible];
                [signInUpVC showAlertWithTitle:@"Please Sign Up" andDescription:@"Whoops! That action requires that you have an account. Please create one and try again."];
            }];
        }
    }
}

- (void)showSignInOrSignUpScreen {
    UIViewController *signInVC = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialVC"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:signInVC];
    [navController setNavigationBarHidden:YES];
    [self presentViewController:navController animated:YES completion:^{
        [self hideMenuIfPossible];
    }];
}

- (void)hideMenuIfPossible {
    if ([self.parentViewController isKindOfClass:[MenuBaseViewController class]]) {
        MenuBaseViewController *p = (MenuBaseViewController *)self.parentViewController;
        [p hideMenuViewController];
    }
}

- (BOOL)userIsPossiblyAuthenticated {
    return ([[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY] != nil);
}

- (void)showViewControllerWithStoryboardID:(NSString *)identifier {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    [self presentViewController:vc animated:YES completion:^{
        
    }];
}

- (void)logout {
    //TODO: invalidate the token on the server.
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_FIRST_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_LAST_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_EMAIL_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_PHONE_NUMBER_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_GUID_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_CARD_TYPE_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_CARD_LAST_FOUR_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGGED_OUT_NOTIFICATION object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)startLoading {
    [self startLoadingWithMessage:@"Loading"];
}

- (void)startLoadingWithMessage:(NSString *)message {
    self.activityIndicator = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityIndicatorVC"];
    [self.activityIndicator setMessage:message];
    [self.activityIndicator addToView:self.view];
}

- (void)updateLoadingMessage:(NSString *)message {
    [self.activityIndicator setMessage:message];
}

- (void)stopLoading{
    [self.activityIndicator removeFromView];
}

@end
