//
//  RideHistoryTableViewCell.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/21/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RideHistoryTableViewCell.h"

@implementation RideHistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
