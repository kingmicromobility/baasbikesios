//
//  HoldViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "HoldViewController.h"
#import "ApplicationState.h"
#import "MainViewController.h"
#import "Constants.h"
#import "Mixpanel.h"
#import "CustomAlertViewController.h"
#import "InstructionsViewController.h"

@interface HoldViewController ()

@end

@implementation HoldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(allowOutOfBounds) name:ALLOW_OUT_OF_BOUNDS_NOTIFICATION object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)allowOutOfBounds {
    ApplicationState *appState = [ApplicationState sharedInstance];
    appState.currentSession.allowOutsideMarket = @YES;
    [appState.currentSession endWithSuccess:^(Session *session) {
        appState.currentSession = session;
        [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_REVIEW_BIKE_VC];
        [self stopLoading];
        [[Mixpanel sharedInstance] track:@"session.hold.ended" properties:[session propertiesAsDictionary]];
        [[Mixpanel sharedInstance] track:@"session.ended" properties:[session propertiesAsDictionary]];
        [[Mixpanel sharedInstance].people increment:@{@"total spent": @(session.totalPrice)}];
    } andOutsideMarket:^(Session *session) {
        NSLog(@"Well that wasn't supposed to happen...");
    } andFailure:^(NSError *error) {
        NSLog(@"Error ending session %@", error);
        // TODO: ALERT THE USER THAT THE LOCK HAS FAILED
    }];
}

- (IBAction)didPressEndSession:(id)sender {
    [self startLoadingWithMessage:@"Ending bike rental"];
    ApplicationState *appState = [ApplicationState sharedInstance];
    [appState.currentSession endWithSuccess:^(Session *session) {
        appState.currentSession = session;
        [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_REVIEW_BIKE_VC];
        [self stopLoading];
        [[Mixpanel sharedInstance] track:@"session.hold.ended" properties:[session propertiesAsDictionary]];
        [[Mixpanel sharedInstance] track:@"session.ended" properties:[session propertiesAsDictionary]];
        [[Mixpanel sharedInstance].people increment:@{@"total spent": @(session.totalPrice)}];
    } andOutsideMarket:^(Session *session) {
        appState.currentSession = session;
        [self showOutOfBoundsFeeMessage];
    } andFailure:^(NSError *error) {
        NSLog(@"Error ending session %@", error);
        // TODO: ALERT THE USER THAT THE LOCK HAS FAILED
        [self stopLoading];
    }];
}

- (IBAction)didPressResume:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]) {
        [self showBeforeUnlockInstruction];
    } else {
        [self startLoadingWithMessage:@"Connecting to Bike"];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [appState.currentSession resumeWithSuccess:^(Session *session){
            appState.currentSession = session;
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC];
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.success" properties:[session.bike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.trip.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"trip count": @1}];
        } andFailure:^(NSError *error) {
            NSLog(@"Error resuming session %@", error);
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.fail" properties:[appState.currentBike propertiesAsDictionary]];
        }];
    }
}

- (void)showBeforeUnlockInstruction {
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"Push button on smart lock to wake it up"
                    buttonText:@"UNLOCK"
               backgroundImage:[UIImage imageNamed:@"instructions_before_unlock_seat_post"]
                     backBlock:[self backBlock]
                   actionBlock:[self firstActionBlock:instructionsVC]];
    [self.navigationController.parentViewController presentViewController:instructionsVC
                                                                 animated:YES
                                                               completion:nil];
}

- (void (^)())backBlock {
    return ^{};
}

- (void (^)())firstActionBlock:(InstructionsViewController *)currentVC {
    return ^{
        [currentVC startLoadingWithMessage:@"Connecting to bike"];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [appState.currentReservation beginSessionWithSuccess:^(Session *session) {
            appState.currentSession = session;
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC];
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.success" properties:[session.bike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.hold.ended" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.trip.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.trip.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"trip count": @1}];

            [self showAfterUnlockInstruction:currentVC];
        } andFailure:^(NSError *error) {
            // TODO: ALERT USER THAT SESSION FAILED
            NSLog(@"Error beginning session %@", error);
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.fail" properties:[appState.currentBike propertiesAsDictionary]];
        }];
    };
}

- (void)showAfterUnlockInstruction:(UIViewController *)previousVC {
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"Pull chain out and leave it at the bike rack.  Take the bike for a test ride!"
                    buttonText:@"CONTINUE"
               backgroundImage:[UIImage imageNamed:@"instructions_after_unlock_seat_post"]
                     backBlock:nil
                   actionBlock:[self secondActionBlock]];
    [previousVC presentViewController:instructionsVC
                             animated:YES
                           completion:nil];
}

- (void (^)())secondActionBlock {
    return ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    };
}


- (void)showOutOfBoundsFeeMessage {
    [CustomAlertViewController showWithTitle:@"Out Of Bounds"
                                     message:@"Ending a session outside of the boundaries will result in a $25 fee. Are you sure?"
                               yesButtonText:@"Accept Fee"
                            yesButtonHandler:^{ [self allowOutOfBounds]; [self stopLoading]; }
                                noButtonText:@"Cancel"
                             noButtonHandler:^{ [self stopLoading]; }
                              dismissHandler:^{ [self stopLoading]; }
                                 contextView:self.navigationController.parentViewController];
    Session *session = [ApplicationState sharedInstance].currentSession;
    [[Mixpanel sharedInstance] track:@"session.end.boundsFeeMessage.viewed" properties:[session propertiesAsDictionary]];
}

@end
