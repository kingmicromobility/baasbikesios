//
//  Session.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Bike;
@class Reservation;
@class Market;
@class Trip;

@interface Session : NSObject
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) Reservation *reservation;
@property (nonatomic, strong) Bike *bike;
@property (nonatomic, strong) Market *market;
@property (nonatomic, strong) NSMutableArray *trips;
@property (nonatomic, strong) NSDate *startedAt;
@property (nonatomic, strong) NSNumber *ended;
@property (nonatomic, strong) NSNumber *allowOutsideMarket;
@property NSTimeInterval elapsedTime;
@property NSInteger totalPrice;

+ (Session *)getByUUID:(NSUUID *)uuid;

- (instancetype)initWithBike:(Bike *)bike;

- (void)initWithDictionary:(NSDictionary *)sessionDictionary
                andSuccess:(void (^)(Session *))successBlock
                andFailure:(void (^)(NSError *))failureBlock;

- (void)beginWithSuccess:(void (^)(Session *))successBlock
              andFailure:(void (^)(NSError *))failureBlock;

- (void)holdWithinMarket:(void (^)(Session *))successBlock
        andOutsideMarket:(void (^)(Session *))outsideMarketBlock
              andFailure:(void (^)(NSError *))failureBlock;

- (void)endWithSuccess:(void (^)(Session *))successBlock
      andOutsideMarket:(void (^)(Session *))outsideMarketBlock
            andFailure:(void (^)(NSError *))failureBlock;

- (void)resumeWithSuccess:(void (^)(Session *))successBlock
               andFailure:(void (^)(NSError *))failureBlock;

- (Trip *)getCurrentTrip;

- (NSDictionary *)propertiesAsDictionary;

@end
