//
//  EditInfoViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "EditInfoTableViewController.h"

@interface EditInfoViewController : BaseViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *myAccountLabel;

@property (weak, nonatomic) EditInfoTableViewController *subTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitButtonBottomConstraint;

- (IBAction)didPressSubmit:(id)sender;

@end
