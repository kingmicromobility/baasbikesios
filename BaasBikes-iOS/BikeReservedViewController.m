//
//  BikeReservedViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "BikeReservedViewController.h"
#import "Reservation.h"
#import "Mixpanel.h"
#import "CustomAlertViewController.h"
#import "InstructionsViewController.h"
#import "Constants.h"

@interface BikeReservedViewController ()

@end

@implementation BikeReservedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Reservation *res = [ApplicationState sharedInstance].currentReservation;
    
    int seconds = (int)res.remainingTime % 60;
    int minutes = (int)(res.remainingTime / 60) % 60;
    
    if ((int)res.remainingTime > 0) {
        if (seconds < 10) {
            [_reservationTimeLabel setText:[NSString stringWithFormat:@"%d:0%d Remaining", minutes, seconds]];
        }else{
            [_reservationTimeLabel setText:[NSString stringWithFormat:@"%d:%d Remaining", minutes, seconds]];
        }
    }else{
        [self dismissSelfAndClearRentalState];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateLabel:)
                                                 name:@"baas.reservation.update.remainingTime"
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"baas.reservation.update.remainingTime"
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didPressCancel:(id)sender {
    [self startLoadingWithMessage:@"Canceling reservation"];
    ApplicationState *appState = [ApplicationState sharedInstance];
    [appState.currentReservation cancelWithSuccess:^(Reservation *reservation) {
        appState.currentReservation = nil;
        [self stopLoading];
        [self dismissSelfAndClearRentalState];
        [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_RESERVE_BIKE_VC];
        [[Mixpanel sharedInstance] track:@"reservation.canceled" properties:[reservation propertiesAsDictionary]];
    } andFailure:^(NSError *error) {
        // TODO: ALERT USER THAT RESERVATION CANCEL FAILED
        [self stopLoading];
        [self dismissSelfAndClearRentalState];
        [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_RESERVE_BIKE_VC];
    }];
}

- (IBAction)didPressUnlock:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:PERSON_SHOW_INSTRUCTIONS_KEY] isEqualToValue:@(YES)]) {
        [self showBeforeUnlockInstruction];
    } else {
        [self startLoadingWithMessage:@"Connecting to bike"];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [appState.currentReservation beginSessionWithSuccess:^(Session *session) {
            appState.currentSession = session;
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC];
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.success" properties:[session.bike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.ended"];
            [[Mixpanel sharedInstance] track:@"session.trip.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.trip.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"session count": @1, @"trip count": @1}];
        } andFailure:^(NSError *error) {
            // TODO: ALERT USER THAT SESSION FAILED
            NSLog(@"Error beginning session %@", error);
            [self stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.fail" properties:[appState.currentBike propertiesAsDictionary]];
        }];
    }
}

- (void)showBeforeUnlockInstruction {
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"Push button on smart lock to wake it up"
                    buttonText:@"UNLOCK"
               backgroundImage:[UIImage imageNamed:@"instructions_before_unlock_seat_post"]
                     backBlock:[self backBlock]
                   actionBlock:[self firstActionBlock:instructionsVC]];
    [self.navigationController.parentViewController presentViewController:instructionsVC
                                                                 animated:YES
                                                               completion:^{
                                                                   [[Mixpanel sharedInstance] track:@"bike.unlock.before.instructions.viewed"];
                                                               }];
}

- (void (^)())backBlock {
    return ^{
        [[Mixpanel sharedInstance] track:@"bike.unlock.before.instructions.back"];
    };
}

- (void (^)())firstActionBlock:(InstructionsViewController *)currentVC {
    return ^{
        [currentVC startLoadingWithMessage:@"Connecting to bike"];
        ApplicationState *appState = [ApplicationState sharedInstance];
        [appState.currentReservation beginSessionWithSuccess:^(Session *session) {
            appState.currentSession = session;
            [self pushRentalFlowVCForState:kRENTAL_FLOW_STATE_TRIP_IN_PROGRESS_VC];
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.success" properties:[session.bike propertiesAsDictionary]];
            [[Mixpanel sharedInstance] track:@"session.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.ended"];
            [[Mixpanel sharedInstance] track:@"session.trip.started" properties:[session propertiesAsDictionary]];
            [[Mixpanel sharedInstance] timeEvent:@"session.trip.ended"];
            [[Mixpanel sharedInstance].people increment:@{@"session count": @1, @"trip count": @1}];

            [self showAfterUnlockInstruction:currentVC];
        } andFailure:^(NSError *error) {
            // TODO: ALERT USER THAT SESSION FAILED
            NSLog(@"Error beginning session %@", error);
            [currentVC stopLoading];
            [[Mixpanel sharedInstance] track:@"bike.unlock.fail" properties:[appState.currentBike propertiesAsDictionary]];
        }];
    };
}

- (void)showAfterUnlockInstruction:(UIViewController *)previousVC {
    InstructionsViewController *instructionsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstructionsVC"];
    [instructionsVC setCaption:@"Pull chain out and leave it at the bike rack. Take the bike for a test ride!"
                    buttonText:@"CONTINUE"
               backgroundImage:[UIImage imageNamed:@"instructions_after_unlock_seat_post"]
                     backBlock:nil
                   actionBlock:[self secondActionBlock]];
    [previousVC presentViewController:instructionsVC
                             animated:YES
                           completion:^{
                               [[Mixpanel sharedInstance] track:@"bike.unlock.after.instructions.viewed"];
                           }];
}

- (void (^)())secondActionBlock {
    return ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    };
}

- (void)updateLabel:(NSNotification *)notification {
    Reservation *res = (Reservation *)notification.object;

    int seconds = (int)res.remainingTime % 60;
    int minutes = (int)(res.remainingTime / 60) % 60;
    
    if ((int)res.remainingTime > 0) {
        if (seconds < 10) {
            [_reservationTimeLabel setText:[NSString stringWithFormat:@"%d:0%d Remaining", minutes, seconds]];
        }else{
            [_reservationTimeLabel setText:[NSString stringWithFormat:@"%d:%d Remaining", minutes, seconds]];
        }
    }else{
        [self dismissSelfAndClearRentalState];
    }
}


@end
