//
//  SignUpOrLoginViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/1/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SignUpOrLoginViewController : BaseViewController <UIPageViewControllerDataSource, UIScrollViewDelegate>

- (IBAction)pressedClose;
- (IBAction)pressedNext:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headlineContainerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signUpButtonBottomDistance;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nextButtonBottomDistance;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageHeadlines;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end
