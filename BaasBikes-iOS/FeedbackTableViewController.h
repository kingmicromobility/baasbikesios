//
//  FeedbackTableViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTableViewController : UITableViewController <UITextViewDelegate>

@property NSIndexPath *selectedIndexPath;
@property int selectedRow;
@property NSArray *promptArray;
@property NSUInteger numberOfCells;

- (NSDictionary *)getDetails;

@end
