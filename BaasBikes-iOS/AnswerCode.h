//
//  AnswerCode.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 2/24/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lock.h"

@interface AnswerCode : NSObject

- (instancetype) initWithChallengeCode:(NSNumber *)challengeCode
                               andLock:(Lock *)lock;

- (void) getAnswerWithSuccess:(void (^)(NSNumber *))successBlock
                   andFailure:(void (^)(NSError *))failureBlock;

@end
