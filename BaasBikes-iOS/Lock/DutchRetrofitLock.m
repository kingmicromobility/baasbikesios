//
//  DutchRetrofitLock.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/4/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "DutchRetrofitLock.h"

@interface DutchRetrofitLock()
@property (nonatomic, strong) NSString* baasLockId;
@property (strong, nonatomic) BluetoothCentralManager *bluetoothCentralManager;
@property (strong, nonatomic) CBPeripheral *lockPeripheral;
@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL compatible;
@property (nonatomic) BOOL commandPending;
@end

@implementation DutchRetrofitLock

#pragma mark - Lifecycle

- (instancetype) initWithBaasLockId:(NSString *)lockId
                             andUrn:(NSString *)urn
                       withDelegate:(id<LockDelegate>)delegate {
    self.baasLockId = lockId;
    self.urn = urn;
    self.delegate = delegate;
    self.type = @"dutch-retrofit";
    self.commandPending = NO;
    self.bluetoothCentralManager = [[BluetoothCentralManager alloc] initWithDelegate:self];
    return self;
}


#pragma mark - Public

- (void) connect {
    NSLog(@"[DutchRetrofit Lock] Searching for lock %@", self.baasLockId);
    [self.bluetoothCentralManager beginSearchingForLockPeripheral:self.baasLockId
                                                      withService:BAAS_LOCK_SERVICE_UUID];
}

- (BOOL) isConnected {
    return self.connected;
}

- (void) disconnect {
    NSLog(@"[DutchRetrofit Lock] Disconnecting from bluetooth peripheral");
    if (self.lockPeripheral) {
        [self.bluetoothCentralManager disconnectLockPeripheral:self.lockPeripheral];
    } else {
        NSLog(@"[DutchRetrofit Lock] No peripheral to disconnect from");
    }
}

- (void) confirmCompatibility {
    NSLog(@"[DutchRetrofit Lock] Confirming compatibility");
    if (self.lockPeripheral) {
        [self.lockPeripheral discoverServices:nil];
    } else {
        self.compatible = NO;
    }
}

- (BOOL) isCompatible {
    return self.compatible;
}

- (void) unlock {
    [self withPeripheralConnection:^{
        NSLog(@"Issuing unlock command.");
        self.desiredState = @"unlocked";
        [self sendCommandToLock:@"unlock"];
    }];
}

- (void) lock {
    [self withPeripheralConnection:^{
        NSLog(@"Issuing lock command.");
        self.desiredState = @"locked";
        [self sendCommandToLock:@"lock"];
    }];
}

- (BOOL) isLocked {
    return [self.state isEqualToString:@"locked"];
}


#pragma mark - BluetoothCentralManagerDelegate

- (void) didFindLockPeripheral:(CBPeripheral *)peripheral {
    self.lockPeripheral = peripheral;
    self.lockPeripheral.delegate = self;
    [self.bluetoothCentralManager connectLockPeripheral:self.lockPeripheral];
}

- (void) didConnectLockPeripheral:(CBPeripheral *)peripheral {
    self.lockPeripheral = peripheral;
    self.connected = YES;
    if ([self.delegate respondsToSelector:@selector(didConnect:)]) {
        [self.delegate didConnect:self];
    }
    [self confirmCompatibility];
}

- (void) didDisconnectLockPeripheral:(CBPeripheral *)peripheral {
    self.lockPeripheral = nil;
    self.connected = NO;
    if ([self.delegate respondsToSelector:@selector(didDisconnect:)]) {
        [self.delegate didDisconnect:self];
    }
}


#pragma mark - CBPeripheralDelegate

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverServices:(nullable NSError *)error {
    for (CBService *service in peripheral.services) {
        NSLog(@"[DutchRetrofit Lock] Discovered service %@", service);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(nonnull CBService *)service
             error:(nullable NSError *)error {
    NSArray *characteristics = [service characteristics];
    BOOL transmitDiscovered = NO;
    BOOL receiveDiscovered = NO;

    if (peripheral != self.lockPeripheral) {
        NSLog(@"[DutchRetrofit Lock] Wrong Peripheral.\n");
        return;
    }
    if (error != nil) {
        NSLog(@"[DutchRetrofit Lock] Error %@\n", error);
        return;
    }
    for (CBCharacteristic *characteristic in characteristics) {
        NSLog(@"[DutchRetrofit Lock] Found characteristic: %@", [characteristic UUID]);
        if ([[characteristic UUID] isEqual:BAAS_TRANSMIT_CHAR_UUID]) {
            self.transmit = characteristic;
            transmitDiscovered = YES;
        }
        if ([[characteristic UUID] isEqual:BAAS_RECEIVE_CHAR_UUID]) {
            self.receive = characteristic;
            receiveDiscovered = YES;
            [self.lockPeripheral setNotifyValue:YES
                              forCharacteristic:self.receive];
        }
    }
    if (transmitDiscovered && receiveDiscovered) {
        self.compatible = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"didConfirmCompatibility"
                                                            object:nil
                                                          userInfo:nil];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic
             error:(nullable NSError *)error {
    [self setStateFromCharacteristicValue:characteristic.value];
    if (self.commandPending) {
        [self invokeDelegateAfterAttemptedStateChange];
        self.commandPending = NO;
    }
}

#pragma mark - Private

- (void)setStateFromCharacteristicValue:(NSData *)value {
    self.state = [[NSString alloc] initWithData:value
                                       encoding:NSUTF8StringEncoding];
    NSLog(@"NEW LOCK STATE: %@", self.state);
}

- (void)invokeDelegateAfterAttemptedStateChange {
    NSLog(@"Lock says '%@'. We want '%@'.", self.state, self.desiredState);
    if ([self.state isEqualToString:self.desiredState]) {
        if ([self isLocked]) {
            if ([self.delegate respondsToSelector:@selector(didLock:)]) {
                [self.delegate didLock:self];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(didUnlock:)]) {
                [self.delegate didUnlock:self];
            }
        }
    } else {
        if ([self.desiredState isEqualToString:@"locked"]) {
            if ([self.delegate respondsToSelector:@selector(failedToLock:)]) {
                [self.delegate failedToLock:self];
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(failedToUnlock:)]) {
                [self.delegate failedToUnlock:self];
            }
        }
    }
    [self disconnect];
}

- (void)withPeripheralConnection:(void (^)(void))completionBlock {
    __block NSNotificationCenter *notiCenter = [NSNotificationCenter defaultCenter];
    if (![self isConnected]) {
        __block __weak id successObserver;
        __block __weak id failureObserver;
        NSTimer *connectionTimer = [NSTimer scheduledTimerWithTimeInterval:15
                                                                    target:self
                                                                  selector:@selector(connectionTimedOut)
                                                                  userInfo:nil
                                                                   repeats:NO];
        successObserver = [notiCenter addObserverForName:@"didConfirmCompatibility"
                                                  object:nil
                                                   queue:[NSOperationQueue mainQueue]
                                              usingBlock:^(NSNotification *notification){
                                                  completionBlock();
                                                  [connectionTimer invalidate];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
                                              }];
        failureObserver = [notiCenter addObserverForName:@"didFailToConnect"
                                                  object:nil
                                                   queue:[NSOperationQueue mainQueue]
                                              usingBlock:^(NSNotification *notification){
                                                  [connectionTimer invalidate];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:failureObserver];
                                                  [[NSNotificationCenter defaultCenter] removeObserver:successObserver];
                                              }];
        [self connect];
        if ([self.delegate respondsToSelector:@selector(didBeginConnecting:)]) {
            [self.delegate didBeginConnecting:self];
        }
    } else {
        completionBlock();
    }
}

- (void)sendCommandToLock:(NSString *)message {
    NSData *data = [message dataUsingEncoding:NSASCIIStringEncoding];
    if (self.lockPeripheral && self.transmit) {
        self.commandPending = YES;
        [self.lockPeripheral writeValue:data
                      forCharacteristic:self.transmit
                                   type:CBCharacteristicWriteWithResponse];
    }
}

- (void)connectionTimedOut {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFailToConnect" object:nil];
    if ([self.delegate respondsToSelector:@selector(failedToConnect:)]) {
        [self.delegate failedToConnect:self];
    }
}

@end
