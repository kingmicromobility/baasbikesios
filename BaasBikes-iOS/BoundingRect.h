//
//  BoundingRect.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 11/24/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BoundingRect : NSObject

@property CLLocationCoordinate2D topLeft;
@property CLLocationCoordinate2D topRight;
@property CLLocationCoordinate2D bottomLeft;
@property CLLocationCoordinate2D bottomRight;
@property CLLocationCoordinate2D center;

@end
