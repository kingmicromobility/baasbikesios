//
//  EditInfoTableViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/20/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "EditInfoTableViewController.h"
#import "Constants.h"

@interface EditInfoTableViewController ()

@end

@implementation EditInfoTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    [_firstNameField setPlaceholder: [[NSUserDefaults standardUserDefaults] stringForKey:PERSON_FIRST_NAME_KEY]];
    _firstNameField.delegate = _textDelegate;
    [_lastNameField setPlaceholder: [[NSUserDefaults standardUserDefaults] stringForKey:PERSON_LAST_NAME_KEY]];
    _lastNameField.delegate = _textDelegate;
    [_emailField setPlaceholder: [[NSUserDefaults standardUserDefaults] stringForKey:PERSON_EMAIL_KEY]];
    _emailField.delegate = _textDelegate;
    [_phoneNumberField setPlaceholder: [[NSUserDefaults standardUserDefaults] stringForKey:PERSON_PHONE_NUMBER_KEY]];
    _phoneNumberField.delegate = _textDelegate;
    _currentPasswordField.delegate = _textDelegate;
    _passwordField.delegate = _textDelegate;
    _confirmNewPasswordField.delegate = _textDelegate;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section < 2) {
        return 2;
    }else{
        return 3;
    }
}

- (void)hideKeyboard {
    [_firstNameField resignFirstResponder];
    [_lastNameField resignFirstResponder];
    [_emailField resignFirstResponder];
    [_phoneNumberField resignFirstResponder];
}


@end
