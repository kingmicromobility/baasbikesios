//
//  MyAccountViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/1/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface MyAccountViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *myAccountLabel;

- (IBAction)pushedSignOut;

@property (weak, nonatomic) IBOutlet UIButton *signOutButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;



@end
