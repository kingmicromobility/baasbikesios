//
//  Person.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Person.h"
#import "RestApiClient.h"

#define URI @"people/"

@implementation Person

+ (Person *)getByUUID:(NSUUID *)uuid {
    return nil;
}

- (instancetype) initWithDictionary:(NSDictionary *)personDictionary {
    self.guid = personDictionary[@"guid"];
    return self;
}

- (void)activeReservationWithSuccess:(void (^)(Reservation *))successBlock
                          andFailure:(void (^)(NSError *))failureBlock {
    NSDictionary *params = @{@"status": @"active"};
    NSString *url = [NSString stringWithFormat:@"%@%@/reservations/",
                     URI, self.guid.UUIDString.lowercaseString];

    [[RestApiClient sharedInstance] list:url
                                  params:params
                                 success:[self onReservationSuccess:successBlock
                                                         andFailure:failureBlock]
                                   error:failureBlock];
}

- (void (^)(NSDictionary *))onReservationSuccess:(void(^)(Reservation *))block
                                      andFailure:(void(^)(NSError *))failure {
    return ^void(NSDictionary *reservationsDict) {
        if ([reservationsDict[@"results"] count] > 0) {
            [[Reservation alloc] initWithDictionary:reservationsDict[@"results"][0]
            andSuccess:^(Reservation *reservation) {
                block(reservation);
            } andFailure:failure];
        } else {
            block(nil);
        }
    };
}

- (void)activeSessionWithSuccess:(void (^)(Session *))successBlock
                      andFailure:(void (^)(NSError *))failureBlock {
    NSDictionary *params = @{@"status": @"active"};
    NSString *url = [NSString stringWithFormat:@"%@%@/sessions/",
                     URI, self.guid.UUIDString.lowercaseString];

    RestApiClient *client = [RestApiClient sharedInstance];

    [client list:url
          params:params
         success:[self onSessionSuccess:successBlock
      andFailure:failureBlock]
           error:failureBlock];
}

- (void (^)(NSDictionary *))onSessionSuccess:(void(^)(Session *))block
                                  andFailure:(void(^)(NSError *))failure {
    return ^void(NSDictionary *sessionsDict) {
        if ([sessionsDict[@"results"] count] > 0) {
            [[Session alloc] initWithDictionary:sessionsDict[@"results"][0]
             andSuccess:^(Session *session) {
                 block(session);
             } andFailure:failure];
        } else {
            block(nil);
        }
    };
}

- (void)activeTripWithSuccess:(void (^)(Trip *))successBlock
                   andFailure:(void (^)(NSError *))failureBlock {
    NSDictionary *params = @{@"status": @"active"};
    NSString *url = [NSString stringWithFormat:@"%@%@/trips/",
                     URI, self.guid.UUIDString.lowercaseString];

    [[RestApiClient sharedInstance] list:url
                                  params:params
                                 success:[self onTripSuccess:successBlock]
                                   error:failureBlock];
}

- (void (^)(NSDictionary *))onTripSuccess:(void(^)(Trip *))block {
    return ^void(NSDictionary *tripsDict) {
        if ([tripsDict[@"results"] count] > 0) {
            Trip *trip = [[Trip alloc] initWithDictionary:tripsDict[@"results"][0]];
            block(trip);
        } else {
            block(nil);
        }
    };
}

@end
