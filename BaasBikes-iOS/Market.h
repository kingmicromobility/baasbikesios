//
//  Market.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "Pusher/Pusher.h"
#import "BoundingRect.h"

@class Bike;


@interface Market : NSObject <PTPusherDelegate>
@property (nonatomic, strong) NSUUID *guid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDictionary *bikes;
@property (nonatomic, strong) NSArray *geoshape;
@property (nonatomic, strong) NSArray *attributionChannels;
@property (nonatomic, strong) NSNumber *closestBikeDistance;
@property (nonatomic, strong) Bike *closestBike;
@property CLLocationCoordinate2D center;
@property CLLocationCoordinate2D nwCorner;
@property CLLocationCoordinate2D neCorner;
@property CLLocationCoordinate2D swCorner;
@property CLLocationCoordinate2D seCorner;

+ (void)nearest:(CLLocation *)location
        success:(void (^)(Market *))successBlock
        failure:(void (^)(NSError *))failureBlock;

+ (void)getByUUID:(NSUUID *)uuid
      withSuccess:(void (^)(Market *))successBlock
       andFailure:(void (^)(NSError *))failureBlock;

- (instancetype)initWithDictionary:(NSDictionary *)marketDictionary;

- (void)location:(CLLocation *)location
        isInside:(void (^)(Market *))isInsideBlock
       isOutside:(void (^)(Market *))isOutsideBlock
       orFailure:(void (^)(NSError *))failureBlock;

- (void)addToMapView:(GMSMapView *)mapView;

- (void)focusMap:(GMSMapView *)mapView;

- (NSDictionary *)propertiesAsDictionary;

- (BoundingRect *)getBoundingRect;

@end
