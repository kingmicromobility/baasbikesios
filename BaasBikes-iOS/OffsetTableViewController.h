//
//  OffsetTableViewController.h
//  BaasBikes-iOS
//
//  Created by John Gazzini on 9/29/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuSelectionDelegate <NSObject>
- (void)selectedMenuItemWithName:(NSString *)menuItemName;
@end

@interface OffsetTableViewController : UITableViewController

@property (weak, nonatomic) id<MenuSelectionDelegate> delegate;

@end
