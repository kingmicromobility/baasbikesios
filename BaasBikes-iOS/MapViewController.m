//
//  MapViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ApplicationState.h"
#import "MapViewController.h"
#import "LocationManager.h"
#import "Constants.h"
#import "Market.h"
#import "Bike.h"
#import "BikeMapMarker.h"
#import "BoundingRect.h"
#import "WKTParser/WKTParser.h"
#import "Mixpanel.h"

@import GoogleMaps;

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _mapView.delegate = self;

    [_centerButton addTarget:self action:@selector(didPressCenterButton) forControlEvents:UIControlEventTouchUpInside];
    [_overlayButton addTarget:self action:@selector(didPressOverlayButton) forControlEvents:UIControlEventTouchUpInside];
    _mapView.myLocationEnabled = YES;
    _mapView.settings.rotateGestures = NO;
    _mapView.accessibilityLabel = @"Map";
    _mapView.alpha = 0;
    [ApplicationState sharedInstance].mainMap = _mapView;
    [self showMarket];
    [self initializeCamera];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMarket) name:DID_RESTORE_APP_STATE_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeCamera) name:DID_RESTORE_APP_STATE_NOTIFICATION object:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)didPressOverlayButton {
    [_overlayButton setImage:[UIImage imageNamed:@"BTN_Range_Orange"] forState:UIControlStateNormal];
    [_overlayButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, -3.0, 0.0)];
    [_centerButton setImage:[UIImage imageNamed:@"BTN_Self"] forState:UIControlStateNormal];
    Market *market = [ApplicationState sharedInstance].currentMarket;
    [market focusMap:_mapView];
    [[Mixpanel sharedInstance] track:@"map.market.centered"
                          properties:[market propertiesAsDictionary]];
}

- (void)didPressCenterButton {
    [_centerButton setImage:[UIImage imageNamed:@"BTN_Self_Orange"] forState:UIControlStateNormal];
    [_overlayButton setImage:[UIImage imageNamed:@"BTN_Range"] forState:UIControlStateNormal];
    [_overlayButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
    [self initializeCamera];
    [[Mixpanel sharedInstance] track:@"map.user.centered" properties:@{
        @"lat": [NSNumber numberWithFloat:_mapView.myLocation.coordinate.latitude],
        @"lng": [NSNumber numberWithFloat:_mapView.myLocation.coordinate.longitude]
    }];
}

- (void)initializeCamera {
    CLLocation *location = [[LocationManager sharedInstance] getCurrentLocation];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:AUTHORIZED_INITIAL_ZOOM];
    [_mapView animateToCameraPosition:camera];
    [UIView animateWithDuration:0.5 animations:^{
        _mapView.alpha = 1;
    }];

}

- (void)showMarket {
    Market *market = [ApplicationState sharedInstance].currentMarket;
    [market addToMapView:_mapView];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(BikeMapMarker *)marker {
    ApplicationState *appState = [ApplicationState sharedInstance];
    if (!appState.currentReservation && !appState.currentSession) {
        [marker.bike select];
        [_mainVC showRentalFlowNavigationVC];
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"bike.selected" properties:[marker.userData propertiesAsDictionary]];
        [mixpanel timeEvent:@"reservation.started"];
    }
    return YES;
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    if(gesture){
        [_overlayButton setImage:[UIImage imageNamed:@"BTN_Range"] forState:UIControlStateNormal];
        [_overlayButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
        [_centerButton setImage:[UIImage imageNamed:@"BTN_Self"] forState:UIControlStateNormal];
    }
}

@end
