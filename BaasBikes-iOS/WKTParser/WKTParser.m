//
//  WKTParser.m
//  WKTParser
//
//  Created by Joel Turnbull on 2/12/13.
//  Copyright (c) 2013 Joel Turnbull. All rights reserved.
//

#import "WKTParser.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@implementation WKTParser

#pragma mark - Public Parsing Methods

+(CLLocationCoordinate2D)coordinateForWktPoint:(NSString *)wkt
{
    NSAssert(wkt && [wkt rangeOfString: @"POINT"].location != NSNotFound, @"Must pass a WKT POINT String");
    
    CLLocation *location = [self locationForWktPoint:wkt];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    
    return coordinate;
}

+(NSArray *)polygonsForWkt:(NSString *)wkt {
    NSMutableArray *polygons = [NSMutableArray array];
    
    if ([wkt rangeOfString: @"MULTIPOLYGON"].location == NSNotFound)
    {
        NSMutableArray *coordinates = [self locationsForWkt: wkt];
        MKPolygon *polygon = [self polygonForCoordinates: coordinates];
        [polygons addObject: polygon];
    }
    else
    {
        NSArray *wkts = [self splitMultiPolygonsFromWkt: wkt];
        for(NSString *wkt in wkts)
        {
            NSMutableArray *coordinates = [self locationsForWkt: wkt];
            MKPolygon *polygon = [self polygonForCoordinates: coordinates];
            [polygons addObject: polygon];
        }
    }
    
    return [NSArray arrayWithArray: polygons];
}

+(NSArray*)coordinatesForExteriorsFromWkt:(NSString *)wkt {
    if ([wkt rangeOfString: @"MULTIPOLYGON"].location == NSNotFound)
    {
        NSMutableArray *coordinates = [self locationsForWkt: wkt];
        return @[coordinates];
    }
    else
    {
        NSMutableArray *coordinates_sets = [NSMutableArray array];
        NSArray *wkts = [self splitMultiPolygonsFromWkt: wkt];
        for(NSString *wkt in wkts)
        {
            NSMutableArray *coordinates = [self locationsForWkt: wkt];
            [coordinates_sets addObject:coordinates];
        }
        return coordinates_sets;
    }
}

#pragma mark - Private Methods

+(NSArray *)splitMultiPolygonsFromWkt:(NSString *)wkt {
    return [wkt componentsSeparatedByString: @")),(("];
}

+(MKPolygon *)polygonForWkt:(NSString *)wkt {
    NSMutableArray *locations = [self locationsForWkt: wkt];
    return [self polygonForCoordinates: locations];
}

+(NSMutableArray *)locationsForWkt:(NSString *)wkt {
    
    NSMutableArray *locations = [NSMutableArray array];
    
    NSError *error = NULL;
    
    NSArray *polygonStrings = [wkt componentsSeparatedByString: @"),("];
    
    NSString *exteriorRingString = [polygonStrings objectAtIndex: 0];
    
    NSString *coordinatesRegexString = @"([-\\d\\.]+\\s[-\\d\\.]+)";
    NSRegularExpression *coordinatesRegex = [NSRegularExpression regularExpressionWithPattern: coordinatesRegexString
                                                                                      options: NSRegularExpressionCaseInsensitive
                                                                                        error: &error];
    NSArray *coordinatesStrings = [coordinatesRegex matchesInString: exteriorRingString
                                                            options: 0
                                                              range: NSMakeRange(0, [exteriorRingString length])];
    
    for (NSTextCheckingResult *match in coordinatesStrings) {
        NSRange matchRange = [match rangeAtIndex:1];
        [locations addObject:[self parseLocationFromString: [wkt substringWithRange:matchRange]]];
    }
    
    return locations;
}

+(NSMutableArray *)stupidCoordinatesForWkt:(NSString *)wkt {
    
    NSMutableArray *coordinates = [NSMutableArray array];
    
    wkt = [wkt componentsSeparatedByString:@"((("][1];
    wkt = [wkt componentsSeparatedByString:@")))"][0];
    
    NSArray *polygonStrings = [wkt componentsSeparatedByString: @"),("];
    
    NSString *exteriorRingString = [polygonStrings objectAtIndex: 0];
    
    
    NSArray *points = [exteriorRingString componentsSeparatedByString:@", "];
    for (NSString *pointString in points) {
        
        NSArray *lonLat = [pointString componentsSeparatedByString:@" "];
        double lon = [lonLat[0] doubleValue];
        double lat = [lonLat[1] doubleValue];
        CLLocation *c = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
        [coordinates addObject:c];
    }
    return coordinates;
}

+(CLLocation *)locationForWktPoint:(NSString *)wkt
{
    NSMutableArray *locations = [NSMutableArray array];
    
    NSError *error = NULL;

    NSString *coordinatesRegexString = @"([-\\d\\.]+\\s[-\\d\\.]+)";
    NSRegularExpression *coordinatesRegex = [NSRegularExpression regularExpressionWithPattern: coordinatesRegexString
                                                                                      options: NSRegularExpressionCaseInsensitive
                                                                                        error: &error];
    NSArray *coordinatesStrings = [coordinatesRegex matchesInString: wkt
                                                            options: 0
                                                              range: NSMakeRange(0, [wkt length])];
    
    for (NSTextCheckingResult *match in coordinatesStrings) {
        NSRange matchRange = [match rangeAtIndex:1];
        [locations addObject:[self parseLocationFromString: [wkt substringWithRange:matchRange]]];
    }
    
    return locations[0];
}

+(MKPolygon *)polygonForCoordinates:(NSArray *) coordinates {
    
	NSInteger coordsLen = [coordinates count] - 1;
    
	if (coordsLen < 1) {
		return nil;
    }
	
	CLLocationCoordinate2D *coords = malloc(sizeof(CLLocationCoordinate2D) * [coordinates count]);
	for (int i=0; i < coordsLen; i++) {
		CLLocation *coordObj = [coordinates objectAtIndex:i];
		coords[i] = CLLocationCoordinate2DMake(coordObj.coordinate.latitude, coordObj.coordinate.longitude);
	}
    
    MKPolygon *newPolygon = [MKPolygon polygonWithCoordinates:coords count:coordsLen];
	free(coords);
    
	return newPolygon;
}

+(CLLocation *)parseLocationFromString:(NSString *)coordinateString {
    NSArray *points = [coordinateString componentsSeparatedByString:@" "];
    NSString *lon = [points objectAtIndex:0];
    NSString *lat = [points objectAtIndex:1];
    return [[CLLocation alloc] initWithLatitude:[lat floatValue]
                                      longitude:[lon floatValue]];
    
}


@end
