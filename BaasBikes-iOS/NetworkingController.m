//
//  NetworkingController.m
//  BaasBikes-iOS
//
//  Created by John Gazzini on 10/9/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "NetworkingController.h"
#import "NSString+LocationHelper.h"
#import "Environment.h"

@implementation NetworkingController

#pragma mark - Lifecycle

+ (instancetype)sharedInstance {
    static NetworkingController *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[NetworkingController alloc] init];
    });

    return this;
}

#pragma mark - Public Methods

- (void)attemptToCreateAccountWithEmail:(NSString *)email
                               password:(NSString *)password
                              firstName:(NSString *)firstName
                               lastName:(NSString *)lastName
                            phoneNumber:(NSString *)phoneNumber
                selfReportedChannelCode:(NSString *)selfReportedChannelCode
                autoReportedChannelCode:(NSString *)autoReportedChannelCode
                                success:(void(^)())successBlock
                                failure:(void(^)(NSString *errorString))failureBlock {
    
    NSString *urlString = [self createURLStringForURI:PERSON_URI];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
      @"first_name": firstName,
      @"last_name": lastName,
      @"phone_number": phoneNumber,
      @"user": @{
              @"email": email,
              @"password": password
              }}];
    if (selfReportedChannelCode) {
        [params setObject:selfReportedChannelCode forKey:@"self_reported_channel_code"];
    }
    if (autoReportedChannelCode) {
        [params setObject:autoReportedChannelCode forKey:@"auto_reported_channel_code"];
    }
    NSLog(@"Params: %@", params);
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        //Now that we've got the account created, we should go ahead and sign in:
        
        [self attemptToLoginWithUsername:email password:password success:successBlock failure:failureBlock];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        failureBlock(@"Server Error!");
    }];
}

- (void)updateAccountWithParams:(NSDictionary *)params success:(void(^)())successBlock failure:(void(^)(NSString *errorString))failureBlock {
    
    NSString *urlString = [self createURLStringForPersonWithSubURI:@""];

    NSLog(@"Params: %@", params);
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager PATCH:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        
        // Persist new auth token
        if (responseObject[@"token"]) {
            NSString *tokenValueToSendInTheFuture = [NSString stringWithFormat:@"Token %@", responseObject[@"token"]];
            [[NSUserDefaults standardUserDefaults] setObject:tokenValueToSendInTheFuture forKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        // After editing the account details, save the new values to user defaults
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock(@"Server Error!");
        }
    }];
}

- (void)resetPasswordWithEmail:(NSString *)email success:(void(^)())successBlock failure:(void(^)(NSString *errorString))failureBlock {
    
    NSString *urlString = [self createURLStringForURI:TEMPORARY_PASSWORD_URI];
    
    NSDictionary *params = @{@"email": email};
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        successBlock();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock(@"Server Error!");
        }
    }];
    
}

- (void)attemptToLoginWithUsername:(NSString *)email password:(NSString *)password success:(void(^)())successBlock failure:(void(^)())failureBlock {
    NSString *urlString = [self createURLStringForURI:LOGIN_USER_URI];
    
    NSDictionary *params = @{
                             @"email": email,
                             @"password": password,
                             };
    NSLog(@"Params: %@", params);
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"response: %@", responseObject);
        
        [self persistUserInfoFromAuthResponse:responseObject];
        [self getPersonPaymentWithSuccess:^(NSDictionary *responseObject) {
            
            NSLog(@"RESPONSE FOR PAY: %@", responseObject);
            
            [self persistUserPaymentInfoFromResponse:responseObject];
        } failure:^{
            NSLog(@"Payment was not stored to device.");
        }];
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        failureBlock();
    }];
}

- (void)getBikesWithLocation:(CLLocationCoordinate2D)location success:(void(^)(NSArray *bikeDicts))successBlock failure:(void(^)())failureBlock {
    NSDictionary *params = @{@"last_lat": @(location.latitude),
                             @"last_lon": @(location.longitude),
                             };
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:GET_BIKES_URI];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"BIKES WITH LOCATION JSON: %@", responseObject);
        
            successBlock(responseObject[@"results"]);
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        failureBlock();
    }];
}

- (void)getMarketWithLocation:(CLLocationCoordinate2D)location success:(void(^)(NSDictionary *marketDict))successBlock failure:(void(^)())failureBlock {
    NSDictionary *params = @{@"lat": @(location.latitude),
                             @"lon": @(location.longitude),
                             };
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:MARKETS_URI];
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if ([responseObject[@"results"] count] > 0) {
            successBlock(responseObject[@"results"][0]);
            NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
        }else{
            NSLog(@"No Markets In The Area");
            
            NSDictionary *noMarketResponse = @{@"noMarketResponse": @"noMarketResponse"};
            successBlock(noMarketResponse);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        failureBlock();
    }];
}

- (void)startReservationWithBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)(NSDictionary *reservationDict))successBlock failure:(void(^)(NSInteger statusCode))failureBlock {
    NSString *authenticatedUserPersonGuid = [self getPersonGuidOrNil];
    if (!authenticatedUserPersonGuid) {
        failureBlock(0);
    }
    NSDictionary *params = @{
                             @"location": [NSString makeStringFromCoordinate:location],
                             @"bike": bike[@"guid"],
                             //@"renter": authenticatedUserPersonGuid,
                             @"canceled": @NO
                             };
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:RESERVATION_URI];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"START RESERVATION JSON: %@", responseObject);
        successBlock(responseObject);
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Reservation Error: %@", error);
        NSLog(@"%@", operation.responseString);
        NSInteger statusCode = operation.response.statusCode;
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock(statusCode);
        }
    }];
}

- (void)cancelReservationWithGuid:(NSString *)guid success:(void(^)())successBlock failure:(void(^)())failureBlock {
    NSString *authenticatedUserPersonGuid = [self getPersonGuidOrNil];
    if (!authenticatedUserPersonGuid) {
        failureBlock();
    }
    NSDictionary *params = @{
                             @"guid": guid,
                             @"canceled": @YES
                             };
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:RESERVATION_URI];
    
    urlString = [NSString stringWithFormat:@"%@%@/", urlString, guid];      //vomit.
    
    [manager PATCH:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"CANCEL RESERVATION JSON: %@", responseObject);
        successBlock(responseObject);
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (void)unlockBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)())successBlock failure:(void(^)())failureBlock {
    [self lockEventWithBike:bike location:location eventTypeString:@"unlocked" success:successBlock failure:failureBlock];
}

// TODO: check for outside_market flag (YES is outside market) and respond to user accordingly
- (void)lockBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location success:(void(^)())successBlock failure:(void(^)())failureBlock {
    [self lockEventWithBike:bike location:location eventTypeString:@"locked" success:successBlock failure:failureBlock];
}

- (void)lockEventWithBike:(NSDictionary *)bike location:(CLLocationCoordinate2D)location eventTypeString:(NSString *)eventTypeString success:(void(^)())successBlock failure:(void(^)())failureBlock {
    
    NSDictionary *params = @{
                             @"location": [NSString makeStringFromCoordinate:location],
                             @"bike": bike[@"guid"],
                             @"event_type": eventTypeString
                             };
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:LOCK_EVENTS_URI];
    
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"LOCK EVENT JSON: %@", responseObject);
        
        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"associated_session"] forKey:SESSION_GUID_DEFAULTS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        successBlock(responseObject[@"results"]);
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (void)getBTClientTokenWithSuccess:(void(^)(NSString *token))successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:BRAINTREE_CLIENT_TOKEN_URI];

    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *appEnvironment = config[@"APP_ENVIRONMENT"];
    
    NSDictionary *params = @{
                             @"app_environment": appEnvironment
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"BT CLIENT JSON: %@", responseObject);
        successBlock(responseObject[@"token"]);
        
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}



- (void)makeNewPaymentMethodForCurrentUserWithNonce:(NSString *)nonce success:(void(^)())successBlock failure:(void(^)(NSString *error))failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:PAYMENT_METHODS_URI];

    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *appEnvironment = config[@"APP_ENVIRONMENT"];
    
    NSDictionary *params = @{
                             @"nonce": nonce,
                             @"app_environment": appEnvironment
                             };
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"NEW PAYMENT METHOD JSON: %@", responseObject);
        
        successBlock();
        
        [self getPersonPaymentWithSuccess:^(NSDictionary *responseObject) {
            
            [self persistUserPaymentInfoFromResponse:responseObject];
        } failure:^{
            NSLog(@"Payment was not stored to device.");
        }];
        
        NSLog(@"Request: %@", [[operation.request URL] absoluteString]);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"%@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock(operation.responseString);
        }
    }];
}

- (void)getPersonPaymentWithSuccess:(void(^)(NSDictionary *responseObject))successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:PAYMENT_METHODS_URI];

    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"PAYMENT JSON: %@", responseObject);
        if ([responseObject count] > 0) {
            successBlock(responseObject[0]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"RESPONSE STRING: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];

}

// If response if 402, respond to the user. Either force it closed and fine the user or keep it open.
// They say "Id like to keep it open", do nothing.
// They say "I accept with the fee", send a PATCH requesto to the server with ended == YES
// also pass end_outside_market == YES and that will force the session and charge the user
- (void)endSessionWithBikeInBounds:(BOOL)bikeInBounds success:(void(^)(NSDictionary *endSessionDict))successBlock failure:(void(^)())failureBlock {
    NSString *sessionGUID = [[NSUserDefaults standardUserDefaults] objectForKey:SESSION_GUID_DEFAULTS_KEY];
    if (!sessionGUID) {
        failureBlock();
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:SESSIONS_URI];
    urlString = [NSString stringWithFormat:@"%@%@/", urlString, sessionGUID];
    
    NSDictionary *params;
    
    if (bikeInBounds) {
        params = @{
                   @"ended":@YES
                   };
    }else{
        params = @{
                   @"ended":@YES,
                   @"end_outside_market":@YES
                   };
    }
    
    [manager PATCH:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"END SESSION JSON: %@", responseObject);
        
        NSDictionary *endSessionDict = @{
                                         @"total_price": responseObject[@"total_price"],
                                         @"started_at": responseObject[@"started_at"],
                                         @"ended_at": responseObject[@"ended_at"]
                                         };
        
        if (bikeInBounds) {
            NSLog(@"Ended session with bike in bounds.");
        }else{
            NSLog(@"Ended session with bike out of bounds.");
        }
        successBlock(endSessionDict);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"RESPONSE STRING: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (AFHTTPRequestOperationManager *)getRequestManager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *possibleToken = [[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    if (possibleToken) {
        [manager.requestSerializer setValue:possibleToken forHTTPHeaderField:AUTH_TOKEN_HTTP_HEADER_KEY];
    }
    return manager;
}

- (NSString *)createURLStringForURI:(NSString *)URI {
    NSDictionary *config = [[Environment sharedInstance] configuration];
    NSString *api_url = config[@"BAAS_API_URL"];
    return [NSString stringWithFormat:@"%@%@", api_url, URI];
}

- (NSString *)createURLStringForPersonWithSubURI:(NSString *)SubURI {
    NSString *personGUID = [[NSUserDefaults standardUserDefaults] objectForKey:PERSON_GUID_KEY];
    NSString *baseWithPersonFormat = [self createURLStringForURI:PERSON_FORMAT_URI];
    NSString *baseWithPerson = [NSString stringWithFormat:baseWithPersonFormat, personGUID];
    return [NSString stringWithFormat:@"%@%@", baseWithPerson, SubURI];
}

- (NSString *)getPersonGuidOrNil {
    return [[NSUserDefaults standardUserDefaults] objectForKey:PERSON_GUID_KEY];
}

- (void)persistUserInfoFromAuthResponse:(NSDictionary *)responseObject {
    NSString *tokenValueToSendInTheFuture = [NSString stringWithFormat:@"Token %@", responseObject[@"token"]];
    [[NSUserDefaults standardUserDefaults] setObject:tokenValueToSendInTheFuture forKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    //TODO: consider just saving the whole "person" dict instead of the params individually.
    NSDictionary *personDict = responseObject[@"person"];
    NSString *firstName = [self getStringOrEmptyStringForKey:@"first_name" fromDict:personDict];
    NSString *lastName = [self getStringOrEmptyStringForKey:@"last_name" fromDict:personDict];
    NSString *email = [self getStringOrEmptyStringForKey:@"email" fromDict:personDict[@"user"]];
    NSString *phoneNumber = [self getStringOrEmptyStringForKey:@"phone_number" fromDict:personDict];
    NSString *personGuid = [self getStringOrEmptyStringForKey:@"guid" fromDict:personDict];
    [[NSUserDefaults standardUserDefaults] setObject:firstName forKey:PERSON_FIRST_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:lastName forKey:PERSON_LAST_NAME_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:PERSON_EMAIL_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:phoneNumber forKey:PERSON_PHONE_NUMBER_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:personGuid forKey:PERSON_GUID_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)persistUserPaymentInfoFromResponse:(NSDictionary *)responseObject {
    NSString *cardType = [self getStringOrEmptyStringForKey:@"card_type" fromDict:responseObject];
    NSString *lastFour = [self getStringOrEmptyStringForKey:@"last_4" fromDict:responseObject];
    [[NSUserDefaults standardUserDefaults] setObject:cardType forKey:PERSON_CARD_TYPE_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:lastFour forKey:PERSON_CARD_LAST_FOUR_KEY];
}

- (NSString *)getStringOrEmptyStringForKey:(NSString *)key fromDict:(NSDictionary *)dict {
    NSString *returnString = dict[key];
    if ((!returnString) || (returnString == (NSString *)[NSNull null])) {   //TODO: clean up that comparison?
        return @"";
    }
    return returnString;
}

- (void)getPersonSessionHistoryWithPages:(int)pages success:(void(^)(NSArray *sessionArray))successBlock failure:(void(^)(NSString *errorString))failureBlock {
    
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:SESSIONS_URI];
    
    NSString *pagesString = [NSString stringWithFormat:@"%d", pages];
    
    NSDictionary *params = @{
                             @"status": @"ended",
                             @"page": pagesString
                             };
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        successBlock(responseObject[@"results"]);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock(@"Server Error!");
        }
    }];
}

- (void)getCurrentReservationWithSuccess:(void(^)())successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:RESERVATIONS_URI];
    
    NSDictionary *params = @{@"status":@"active"};
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:responseObject[@"results"]];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:CURRENT_RESERVATION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (void)getCurrentSessionWithSuccess:(void(^)(NSDictionary *responseObject))successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:SESSIONS_URI];
    
    NSDictionary *params = @{@"status":@"active"};
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:responseObject[@"results"]];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:CURRENT_SESSION_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        successBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (void)getSessionStatusWithSuccess:(void(^)(NSArray *tripArray))successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForPersonWithSubURI:TRIPS_URI];

    NSDictionary *params = @{@"status":@"active"};
    
    [manager GET:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *activeTrips = responseObject[@"results"];
        
        successBlock(activeTrips);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

- (void)getFeedbackPromptsWithSuccess:(void(^)())successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:FEEDBACK_PROMPTS_URI];
    [manager GET:urlString parameters:@{} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Got some questions: %@", responseObject);
        
        [[NSUserDefaults standardUserDefaults] setObject:responseObject[@"results"] forKey:ALL_PROMPTS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        failureBlock();
    }];
}

- (void)postFeedback:(NSDictionary *)feedback withSuccess:(void(^)())successBlock failure:(void(^)())failureBlock {
    AFHTTPRequestOperationManager *manager = [self getRequestManager];
    NSString *urlString = [self createURLStringForURI:FEEDBACK_EVENTS_URI];
    NSDictionary *params = feedback;
    [manager POST:urlString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Saved the feedback! %@", responseObject);
        
        successBlock();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", operation.responseString);
        if (![self setNeedsAuthentication:operation.response.statusCode]) {
            failureBlock();
        }
    }];
}

// TODO: Create a method that gets called when any networking method recieves a 403 response. This method should prompt users to relog in.
- (BOOL)setNeedsAuthentication:(NSInteger)statusCode {
    if (statusCode == 403) {
        [[NSNotificationCenter defaultCenter] postNotificationName:USER_NEEDS_AUTH_NOTIFICATION object:nil];
        return YES;
    }else{
        return NO;
    }
}




@end
