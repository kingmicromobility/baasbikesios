//
//  BaseViewController.h
//  BaasBikes-iOS
//
//  Created by Rahul Sundararaman on 7/23/15.
//  Copyright (c) 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertViewController.h"
#import "ActivityIndicatorViewController.h"

typedef enum {
    kALERT_VIEW_TYPE_TUTORIAL,
    kALERT_VIEW_TYPE_WELCOME,
    kALERT_VIEW_TYPE_BOUNDARIES,
    kALERT_VIEW_TYPE_OUT_OF_BOUNDS_END,
    kALERT_VIEW_TYPE_FORGOT_PASSWORD
} ALERT_VIEW_TYPE;

@interface BaseViewController : UIViewController {
    NSLayoutConstraint *verticalCenterConstraint;
}

@property (strong, nonatomic) CustomAlertViewController *customAlertVC;
@property (strong, nonatomic) ActivityIndicatorViewController *activityIndicator;
@property ALERT_VIEW_TYPE activeAlertType;


- (void)showAlertWithTitle:(NSString *)title andDescription:(NSString *)description;
- (void)showYesNoAlertWithTitle:(NSString *)title andDescription:(NSString *)description;
- (void)showForgotPasswordAlert:(NSString *)emailText
               yesButtonHandler:(void (^)(void))yesButtonHandler
                noButtonHandler:(void (^)(void))noButtonHandler
                 dismissHandler:(void (^)(void))dismissHandler;

- (void)showScrollerWithText:(NSString *)scrollText;
- (void)showScrollerWithBlueText:(NSString *)scrollText;
- (void)showScrollerWithImage:(UIImage *)scrollImage;

- (void)hideMenuIfPossible;
- (void)showSignInOrSignUpScreen;
- (void)showSignInScreen;

- (BOOL)userIsPossiblyAuthenticated;
- (void)userNeedsAuth;

- (void)startLoading;
- (void)startLoadingWithMessage:(NSString *)message;
- (void)updateLoadingMessage:(NSString *)message;
- (void)stopLoading;

- (void)showViewControllerWithStoryboardID:(NSString *)identifier;

- (void)logout;

@end
