//
//  ReviewBikeViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "ReviewBikeViewController.h"
#import "FeedbackViewController.h"
#import "MainViewController.h"
#import "Mixpanel.h"
#import "Constants.h"
#import "ApplicationState.h"

@interface ReviewBikeViewController ()

@end

@implementation ReviewBikeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareRatingStars];
    [self updateLabels];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareRatingStars {
    _starArray = @[_starOne, _starTwo, _starThree, _starFour, _starFive];
    for (UIButton *star in _starArray) {
        [star setTintColor:[UIColor lightGrayColor]];
        [star setAlpha:0.5];
        [star addTarget:self action:@selector(didRateRide:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)didRateRide:(id)sender {
    Session *session = [ApplicationState sharedInstance].currentSession;
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    _rating = (int)[sender tag];
    
    FeedbackViewController *feedbackVC = (FeedbackViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackVC"];
    feedbackVC.rating = _rating;

    NSMutableDictionary *mixpanelProperties = [NSMutableDictionary dictionaryWithDictionary:[session propertiesAsDictionary]];
    [mixpanelProperties addEntriesFromDictionary:@{@"feedback_rating": [NSNumber numberWithInteger:self.rating]}];
    [mixpanel track:@"session.feedback.rating" properties:mixpanelProperties];
    
    if (_rating < 5) {
        for (UIButton *star in _starArray) {
            
            if ([star tag] <= _rating) {
                [UIView animateWithDuration:0.1 animations:^{
                    star.tintColor = ORANGE_TEXT_COLOR;
                    star.alpha = 1;
                } completion:^(BOOL finished) {
                    
                }];
            }
        }
        [self performSelector:@selector(moveToFeedbackVC:) withObject:feedbackVC afterDelay:1.0];
    }else{
        for (UIButton *star in _starArray) {
            [UIView animateWithDuration:0.1 animations:^{
                star.tintColor = ORANGE_TEXT_COLOR;
                star.alpha = 1;
            } completion:^(BOOL finished) {
                
            }];
        }
        [feedbackVC sendFeedbackWithRating:_rating details:nil rideableFlag:YES];
        [self performSelector:@selector(dismissSelfAndClearRentalState) withObject:nil afterDelay:1.0];
    }
}

- (void)moveToFeedbackVC:(FeedbackViewController *)feedbackVC {
    [self.navigationController.parentViewController presentViewController:feedbackVC animated:YES completion:nil];
    [self dismissSelfAndClearRentalState];
}

- (void)updateLabels {
    Session *sess = [ApplicationState sharedInstance].currentSession;
    int hours = 0 - ((int)sess.elapsedTime / 3600.0);
    int minutes = 0 - ((int)(sess.elapsedTime / 60) % 60);
    
    if (minutes < 10) {
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour 0%d min", hours, minutes]];
    }else{
        [_rentalTimeLabel setText:[NSString stringWithFormat:@"Rental Time: %d hour %d min", hours, minutes]];
    }
    
    [_priceLabel setText:[NSString stringWithFormat:@"Price: $%ld.00",(long)sess.totalPrice]];
}


@end
