//
//  RentalViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RentalViewController.h"
#import "MainViewController.h"
#import "MapViewController.h"
#import "ApplicationState.h"
#import "Mixpanel.h"
#import "Bike.h"

@interface RentalViewController ()

@end

@implementation RentalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)pushRentalFlowVCForState:(RENTAL_FLOW_STATE)state {
    RentalFlowNavigationController *rentalNavVC = (RentalFlowNavigationController *)[self navigationController];
    [rentalNavVC pushRentalFlowVCForState:state];
}

- (IBAction)didPressRepair:(id)sender {
    UIViewController *feedbackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFeedbackVC"];
    [self.navigationController.parentViewController presentViewController:feedbackVC animated:YES completion:nil];
    Bike *bike = [ApplicationState sharedInstance].currentBike;
    [[Mixpanel sharedInstance] track:@"bike.feedback" properties:[bike propertiesAsDictionary]];
}


- (IBAction)didPressInfo:(id)sender {
    UIViewController *bikeInfoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BikeInfoVC"];
    
    UINavigationController *nav = [[UINavigationController alloc] init];
    [nav pushViewController:bikeInfoVC animated:true];
    
    [self.navigationController.parentViewController presentViewController:nav animated:YES completion:nil];
}

- (void)dismissSelfAndClearRentalState {
    MainViewController *mainVC = (MainViewController *)self.navigationController.parentViewController;
    [mainVC hideRentalFlowNavigationVC];
}

- (void)startLoading {
    RentalFlowNavigationController *rentalNav = (RentalFlowNavigationController *)self.navigationController;
    [rentalNav startLoading];
}

- (void)startLoadingWithMessage:(NSString *)message {
    RentalFlowNavigationController *rentalNav = (RentalFlowNavigationController *)self.navigationController;
    [rentalNav startLoadingWithMessage:message];
}

- (void)updateLoadingMessage:(NSString *)message {
    RentalFlowNavigationController *rentalNav = (RentalFlowNavigationController *)self.navigationController;
    [rentalNav updateLoadingMessage:message];
}

- (void)stopLoading {
    RentalFlowNavigationController *rentalNav = (RentalFlowNavigationController *)self.navigationController;
    [rentalNav stopLoading];
}

@end
