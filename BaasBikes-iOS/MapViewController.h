//
//  MapViewController.h
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BoundingRect.h"
#import "MainViewController.h"
#import "BaseViewController.h"
#import "BikeMapMarker.h"

@import GoogleMaps;

@interface MapViewController : BaseViewController <GMSMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *overlayButton;
@property (weak, nonatomic) IBOutlet UIButton *centerButton;

@property BOOL rentalViewIsShowing;
@property BOOL reportIsActive;
@property MainViewController *mainVC;

@end
