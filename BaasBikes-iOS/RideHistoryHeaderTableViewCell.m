//
//  RideHistoryHeaderTableViewCell.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "RideHistoryHeaderTableViewCell.h"

@implementation RideHistoryHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
