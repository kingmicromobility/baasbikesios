//
//  FeedbackTableViewController.m
//  BaasBikes-iOS
//
//  Created by Matthew Clevenger on 10/22/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "FeedbackTableViewController.h"
#import "FeedbackTableViewCell.h"
#import "PromptSetHelper.h"
#import "Constants.h"
#import "NSString+LocationHelper.h"
#import "FeedbackViewController.h"
#import "Mixpanel.h"

@interface FeedbackTableViewController () {
    NSString *promptSetGuid;
}

@end

@implementation FeedbackTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"bike.feedback.prompts.viewed" properties:nil];
    
    _numberOfCells = _promptArray.count;
    _selectedRow = -1;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return number of prompts recieved from the server +1 for the "other" cell
    return _numberOfCells;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _promptArray.count){
 
        FeedbackTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"feedback cell"];

            NSDictionary *questionDict = _promptArray[indexPath.row];
            [cell.promptLabel setText:questionDict[@"default"]];

        if ((_selectedIndexPath) && ([_selectedIndexPath isEqual:indexPath])) {
            [self selectCell:cell];
        }else{
            [self unSelectCell:cell];
        }
        return cell;
    }else{
        FeedbackTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"other cell"];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    _selectedRow = (int)indexPath.row;
    
    //unselect the old one:
    FeedbackTableViewCell *oldActiveCell = [tableView cellForRowAtIndexPath:_selectedIndexPath];
    [self unSelectCell:oldActiveCell];
    
    FeedbackTableViewCell *newActiveCell = [tableView cellForRowAtIndexPath:indexPath];
    [self selectCell:newActiveCell];
    
    _selectedIndexPath = indexPath;

    if (_selectedIndexPath.row < _promptArray.count - 1 && _numberOfCells ==  _promptArray.count + 1) {
        [tableView beginUpdates];
        _numberOfCells = _promptArray.count;
        int indexToRemove = (int)[_promptArray count];
        NSIndexPath *ip = [NSIndexPath indexPathForItem:indexToRemove inSection:0];
        [tableView deleteRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationMiddle];
        [tableView endUpdates];
    }else if (_selectedIndexPath.row == _promptArray.count - 1 && _numberOfCells ==  _promptArray.count){
        [tableView beginUpdates];
        _numberOfCells = _promptArray.count + 1;
        int indexToInsert = (int)[_promptArray count];
        NSIndexPath *ip = [NSIndexPath indexPathForItem:indexToInsert inSection:0];
        [tableView insertRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationMiddle];
        
        [tableView endUpdates];
        
        NSIndexPath *nIP = [NSIndexPath indexPathForItem:indexPath.row + 1 inSection:indexPath.section];
        FeedbackTableViewCell *descriptionCell = [tableView cellForRowAtIndexPath:nIP];
        
        if(descriptionCell.descriptionField){
            [descriptionCell.descriptionField becomeFirstResponder];
        }
    }
    if([newActiveCell.promptLabel.text isEqualToString:@"Bike is Broken"]){
        _promptArray = [PromptSetHelper getPromptsForSetNamed:SESSION_END_PROMPTS_KEY][@"prompts"];
        [tableView reloadData];
        FeedbackTableViewCell *oldActiveCell = [tableView cellForRowAtIndexPath:_selectedIndexPath];
        [self unSelectCell:oldActiveCell];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < _promptArray.count) {
        return 50.0;
    }else{
        return 150.0;
    }
}

- (void)unSelectCell:(FeedbackTableViewCell *)cell {
    [cell.selectionButton setImage:[UIImage imageNamed:@"unselected_feedback"]];
}

- (void)selectCell:(FeedbackTableViewCell *)cell {
    [cell.selectionButton setImage:[UIImage imageNamed:@"selected_feedback"]];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

- (NSDictionary *)getDetails {
    
    if (_selectedIndexPath.row < _promptArray.count - 1) {
        NSDictionary *selectedQuestion = _promptArray[_selectedIndexPath.row];
        //TODO: account for 'other' scenario.
        NSDictionary *returnDict = @{
                                     @"prompt": selectedQuestion[@"guid"],
                                     @"value": selectedQuestion[@"default"]
                                     };
        
        return returnDict;
    }else{
        // If cell selected is "other" cell
        Mixpanel *mixpanel = [Mixpanel sharedInstance];
        [mixpanel track:@"bike.feedback.prompts.other.viewed" properties:nil];
        
        NSDictionary *selectedQuestion = _promptArray[_selectedIndexPath.row];
        FeedbackTableViewCell *otherCell = [[self tableView]cellForRowAtIndexPath:[NSIndexPath indexPathForItem:(int)_selectedIndexPath.row + 1 inSection:0]];
        NSString *description = otherCell.descriptionField.text;
        //TODO: account for 'other' scenario.
        NSDictionary *returnDict = @{
                                     @"prompt": selectedQuestion[@"guid"],
                                     @"value": description
                                     };
        
        return returnDict;
    }
}

- (NSDictionary *)getOtherDetailsWithDescription:(NSString *)description {
    //For now, we're assuming there's only 1 selected prompt:
    NSDictionary *selectedQuestion = _promptArray[[_promptArray count] - 1];
    
    //TODO: account for 'other' scenario.
    NSDictionary *returnDict = @{
                                 @"prompt": selectedQuestion[@"guid"],
                                 @"value": description
                                 };
    return returnDict;
}

@end



