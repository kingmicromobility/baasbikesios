//
//  Bike.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "Reservation.h"
#import "Bike.h"
#import "LockManager.h"
#import "ApplicationState.h"

@interface Bike()
@property (nonatomic, strong) void (^nextLockSuccess)(LockEvent *);
@property (nonatomic, strong) void (^nextLockFailure)(NSError *);
@property (nonatomic, strong) void (^nextUnlockSuccess)(LockEvent *);
@property (nonatomic, strong) void (^nextUnlockFailure)(NSError *);
@property (nonatomic, strong) void (^nextConnectFailure)(NSError *);
@property BOOL releasingChain;
@end

@implementation Bike

+ (NSMutableDictionary *)dictionaryFromArray:(NSArray *)bikeDicts {
    return [self dictionaryFromArray:bikeDicts forMarket:nil];
}

+ (NSMutableDictionary *)dictionaryFromArray:(NSArray *)bikeDicts
                                   forMarket:(Market *)market {
    NSMutableDictionary *bikes = [[NSMutableDictionary alloc] init];
    for ( id bikeDict in bikeDicts ) {
        Bike *bike = [[Bike alloc] initWithDictionary:bikeDict andMarket:market];
        [bikes setObject:bike forKey:bike.guid];
    }
    return bikes;
}

+ (Bike *)getByUUID:(NSUUID *)uuid {
    return nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)bikeDict
                         andMarket:(Market *)market {
    self.market             = market;
    self.withinMarketBounds = @"unknown";
    self.marker             = [[BikeMapMarker alloc] initWithBike:self];
    if ([bikeDict objectForKey:@"image_set"] != [NSNull null]) {
        self.imageSet       = [[NSUUID alloc] initWithUUIDString:bikeDict[@"image_set"]];
    }
    [self updateWithDictionary:bikeDict];
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)bikeDict {
    self.guid         = [[NSUUID alloc] initWithUUIDString:bikeDict[@"guid"]];
    self.ambassador   = bikeDict[@"ambassador"];
    self.name         = bikeDict[@"name"];
    self.brand        = bikeDict[@"brand"][@"name"];
    self.style        = bikeDict[@"style"][@"name"];
    self.size         = bikeDict[@"size"][@"name"];
    self.status       = bikeDict[@"status"];
    self.renterCount  = bikeDict[@"unique_renter_count"];
    self.details      = bikeDict[@"detail_set"];
    self.bikeLock     = [[LockManager sharedInstance] getLockByURN:bikeDict[@"lock"][@"urn"]
                                                      withDelegate:self];
    self.lastLocation = [[CLLocation alloc] initWithLatitude:[bikeDict[@"last_lat"] floatValue]
                                                   longitude:[bikeDict[@"last_lon"] floatValue]];
    [self.marker refresh];
}

- (void)isWithinMarketBounds:(void (^)(Bike *))withinMarketBlock
       orOutsideMarketBounds:(void (^)(Bike *))outsideMarketBlock
                  andFailure:(void (^)(NSError *))failureBlock {
    [self.market location:self.lastLocation
                 isInside:^(Market *market) {
                     self.withinMarketBounds = @"yes";
                     withinMarketBlock(self);
                 } isOutside:^(Market *market) {
                     self.withinMarketBounds = @"no";
                     outsideMarketBlock(self);
                 } orFailure:failureBlock];
}

- (void)reserveWithSuccess:(void (^)(Reservation *))successBlock
                andFailure:(void (^)(NSError *))failureBlock {
    Reservation *reservation = [[Reservation alloc] initWithBike:self];
    [reservation beginWithSuccess:successBlock andFailure:failureBlock];
}

- (void)lockWithSuccess:(void (^)(LockEvent *))successBlock
             andFailure:(void (^)(NSError *))failureBlock {
    self.nextLockSuccess = successBlock;
    self.nextLockFailure = failureBlock;
    self.nextConnectFailure = failureBlock;
    [self.bikeLock lock];
}

- (void)unlockWithSuccess:(void (^)(LockEvent *))successBlock
               andFailure:(void (^)(NSError *))failureBlock {
    self.nextUnlockSuccess = successBlock;
    self.nextUnlockFailure = failureBlock;
    self.nextConnectFailure = failureBlock;
    [self.bikeLock unlock];
}

- (void)releaseChainWithSuccess:(void (^)(LockEvent *))successBlock
                     andFailure:(void (^)(NSError *))failureBlock {
    self.releasingChain = YES;
    self.nextUnlockSuccess = successBlock;
    self.nextUnlockFailure = failureBlock;
    self.nextConnectFailure = failureBlock;
    [self.bikeLock unlock];
}

- (void)select {
    [[ApplicationState sharedInstance] updateCurrentBike:self];
}

- (void)deselect {
    [[ApplicationState sharedInstance] updateCurrentBike:nil];
}

- (BOOL)isSelected {
    return [[ApplicationState sharedInstance].currentBike isEqual:self];
}

- (BOOL)isAvailable {
    return [self.status isEqualToString:@"available"];
}

- (NSDictionary *)propertiesAsDictionary {
    NSMutableDictionary *properties = [NSMutableDictionary dictionaryWithDictionary:
    @{
      @"bike_ambassador"    : self.ambassador,
      @"bike_name"          : self.name,
      @"bike_brand"         : self.brand,
      @"bike_style"         : self.style,
      @"bike_size"          : self.size,
      @"bike_status"        : self.status,
      @"bike_renter_count"  : self.renterCount,
      @"bike_lock_type"     : self.bikeLock.type,
      @"bike_last_lat"      : [NSNumber numberWithDouble:self.lastLocation.coordinate.latitude],
      @"bike_last_lng"      : [NSNumber numberWithDouble:self.lastLocation.coordinate.longitude],
      @"bike_within_market" : self.withinMarketBounds
      }];
    [properties addEntriesFromDictionary:self.details];
    [properties addEntriesFromDictionary:[self.market propertiesAsDictionary]];
    return properties;
}

# pragma mark - LockDelegate

- (void) didLock:(Lock *)lock {
    [LockEvent lockBike:self
            withSuccess:self.nextLockSuccess
             andFailure:self.nextLockFailure];
}

- (void) didUnlock:(Lock *)lock {
    if (self.releasingChain) {
        self.releasingChain = NO;
        self.nextUnlockSuccess(nil);
    } else {
        [LockEvent unlockBike:self
                  withSuccess:self.nextUnlockSuccess
                   andFailure:self.nextUnlockFailure];
    }
}

- (void) failedToLock:(Lock *)lock {
    NSError *error = [[NSError alloc] initWithDomain:@"baas.lock.lock.failure"
                                                code:1
                                            userInfo:nil];
    self.nextLockFailure(error);
}

- (void) failedToUnlock:(Lock *)lock {
    NSError *error = [[NSError alloc] initWithDomain:@"baas.lock.unlock.failure"
                                                code:1
                                            userInfo:nil];
    self.nextUnlockFailure(error);
    self.releasingChain = NO;
}

- (void) failedToConnect:(Lock *)lock {
    NSError *error = [[NSError alloc] initWithDomain:@"baas.lock.connect.failure"
                                                code:1
                                            userInfo:nil];
    self.nextConnectFailure(error);
    self.releasingChain = NO;
}

@end
