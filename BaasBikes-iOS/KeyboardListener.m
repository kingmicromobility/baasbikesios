//
//  KeyboardListener.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 1/4/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardListener.h"

@implementation KeyboardListener

+ (instancetype)sharedInstance {
    static KeyboardListener *this = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        this = [[KeyboardListener alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:this selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:this selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    });

    return this;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardDidShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.keyboardHeight = kbSize.height;
    self.keyboardVisible = YES;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardDidHide:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.keyboardHeight = kbSize.height;
    self.keyboardVisible = NO;
}



@end
