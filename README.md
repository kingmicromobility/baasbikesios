# Baas Bikes iOS App
This is the Baas Bikes iOS app for renters/riders.

## Development Environment Setup
_*NOTE:* These steps are incomplete_

1. Check if your system has:
   * **ruby** (The answer is yes if you're on a mac. If
   you're not on a mac, you're going to have a hard time working on an iOS
   project.) If you plan to do any significant ruby development, you may prefer
   using [rbenv](https://github.com/rbenv/rbenv) to manage ruby installations.

   * **rubygems** (probably). Click
   [here](https://rubygems.org/pages/download) install rubygems.

1.  Install Cocoapods
    ```shell
    $ sudo gem install cocoapods
    ```
    **Note:** On El Capitan (as of 12/12/2015), Cocoapods does not, by default,
    place its executable in a `$PATH`-findable location. Run the line below
    instead to avoid the problem:
    ```shell
    $ sudo gem install -n /usr/local/bin cocoapods
    ```

1.  Install Pods (this could take several minutes)
    ```shell
    $ cd /path/to/baasbikesios
    $ pod install
    ```

1.  Initialize KIF Framework submodule
    ```shell
    $ git submodule init
    $ git submodule update
    ```

1.  Launch Xcode
    ```shell
    $ open BaasBikes-iOS.xcworkspace
    ```

1.  Confirm code compiles & tests pass by hitting `⌘ + U`.


## Tests
The test suite for this app contains unit tests for individual pieces of code,
and functional tests that simulate touch interactions by a user.

*  To run the **Unit Tests**, simply hit `⌘ + U ` within Xcode. You can see the
results in the Test Navigator.

* To run the **Functional Tests**.. welp, we're still figuring this out. Stay
tuned.


## Deploying to via Fabric
Instructions coming soon.

## Development Workflow
Before adding a feature features, fixing a bug, or any making any kind source change, please branch off of the development branch and do your work off there.
When you have made progress with your work, please submit a pull request to development for review.
Every release is a merge of development into master.
