//
//  AuthenticatedMenuSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/24/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import "ApplicationStateHelper.h"
#import "AuthenticationHelper.h"
#import "LocationManager.h"
#import "RestApiClient.h"
#import "FixtureHelper.h"
#import "MockHelper.h"
#import "Mixpanel.h"

SpecBegin(AuthenticatedMenu)

describe(@"The authenticated Menu,", ^{
    __block id keyWindow = [[UIApplication sharedApplication] keyWindow];
    __block id mockMixpanel;
    __block id mockMixpanelClass;
    __block id mockRestApiClient;
    __block id mockRestApiClientClass;
    __block id mockLocationManagerClass;
    __block CLLocation *location;
    beforeAll(^{
//        [tester waitForTappableViewWithAccessibilityLabel:@"Dismiss"];
//        [tester tapViewWithAccessibilityLabel:@"Dismiss"];
//        [tester tapViewWithAccessibilityLabel:@"Dismiss"];

        location = [[CLLocation alloc] initWithLatitude:38.908912 longitude:-77.030923];
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        mockRestApiClient = OCMClassMock([RestApiClient class]);
        mockRestApiClientClass = OCMClassMock([RestApiClient class]);
        mockLocationManagerClass = OCMClassMock([LocationManager class]);
        void (^marketsMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"marketsData"]);
        };
        void (^emptyResultMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"emptyResult"]);
        };
        OCMStub([mockRestApiClient isAuthenticationError:403]).andReturn(@NO);
        OCMStub([mockLocationManagerClass currentLocation]).andReturn(location);
        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/sessions/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/reservations/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:@"markets/"
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(marketsMock);
        OCMStub([mockRestApiClientClass sharedInstance]).andReturn(mockRestApiClient);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
    });
    beforeEach(^{
        [AuthenticationHelper authenticateUser];
        if([keyWindow accessibilityElementWithLabel:@"Allow"]) {
            [tester tapViewWithAccessibilityLabel:@"Allow"];
        }
        [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
    });
    afterEach(^{
        [tester tapViewWithAccessibilityLabel:@"RIDE A BIKE"];
        [AuthenticationHelper unauthenticateUser];
    });
    afterAll(^{
        [mockLocationManagerClass stopMocking];
        [mockRestApiClientClass stopMocking];
        [mockMixpanelClass stopMocking];
    });
    it(@"should contain a 'RIDE A BIKE' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"RIDE A BIKE"];
    });
    it(@"should contain a 'MY ACCOUNT' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"MY ACCOUNT"];
    });
    it(@"should contain a 'RIDE HISTORY' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"RIDE HISTORY"];
    });
    it(@"should contain a 'CONTACT US' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"CONTACT US"];
    });
    it(@"should contain a 'HELP' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"HELP"];
    });

});

SpecEnd
