//
//  ApplicationStateHelper.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/26/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationState.h"
#import "ApplicationStateHelper.h"
#import "Constants.h"
#import "Person.h"

@implementation ApplicationStateHelper

+ (void)refreshApplicationState {
    NSString *personGuid = [[NSUserDefaults standardUserDefaults] objectForKey:PERSON_GUID_KEY];
    Person *person = [[Person alloc] init];
    person.guid = [[NSUUID alloc] initWithUUIDString:personGuid];
    [[ApplicationState sharedInstance] restoreForPerson:person
                                            withSuccess:^(ApplicationState *appState) {
                                                [[NSNotificationCenter defaultCenter] postNotificationName:DID_RESTORE_APP_STATE_NOTIFICATION object:nil];
                                            }
                                             andFailure:nil];
}

@end