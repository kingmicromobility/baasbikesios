//
//  UnauthenticatedMenuSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/14/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import "AuthenticationHelper.h"
#import "Mixpanel.h"

SpecBegin(UnauthenticatedMenu)

describe(@"Unauthenticated Menu,", ^{
    __block id keyWindow = [[UIApplication sharedApplication] keyWindow];
    __block id mockMixpanel;
    __block id mockMixpanelClass;
    beforeAll(^{
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
    });
    beforeEach(^{
        [AuthenticationHelper unauthenticateUser];
        if([keyWindow accessibilityElementWithLabel:@"Allow"]) {
            [tester tapViewWithAccessibilityLabel:@"Allow"];
        }
        [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
    });
    afterEach(^{
        [tester tapViewWithAccessibilityLabel:@"RIDE A BIKE"];
    });
    afterAll(^{
        [mockMixpanelClass stopMocking];
    });
    it(@"should contain a 'RIDE A BIKE' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"RIDE A BIKE"];
    });
    it(@"should contain a 'WHAT ARE BAAS BIKES?' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"WHAT ARE BAAS BIKES?"];
    });
    it(@"should contain a 'HELP' menu item.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"HELP"];
    });
    it(@"should have a 'SIGN UP / LOG IN' button.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
    });
    
});

SpecEnd
