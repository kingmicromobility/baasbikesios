//
//  MockHelper.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/27/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MockHelper : NSObject

+ (BOOL (^)(id))matchesPattern:(NSString *)patternString;

@end
