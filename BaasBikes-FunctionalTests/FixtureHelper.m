//
//  FixtureHelper.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/26/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "FixtureHelper.h"

@implementation FixtureHelper

+ (NSDictionary *)jsonFixture:(NSString *)filename {
    NSBundle *testBundle = [NSBundle bundleForClass:[self class]];
    NSString *filePath = [testBundle pathForResource:filename
                                              ofType:@"json"];
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    return [NSJSONSerialization JSONObjectWithData:fileData
                                           options:kNilOptions
                                             error:nil];
}

@end
