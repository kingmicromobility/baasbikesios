//
//  RentalFlowSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/24/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import "ApplicationStateHelper.h"
#import "LocationManager.h"
#import "AuthenticationHelper.h"
#import "NetworkingController.h"
#import "ApplicationState.h"
#import "PromptSetHelper.h"
#import "FixtureHelper.h"
#import "RestApiClient.h"
#import "AppDelegate.h"
#import "MockHelper.h"
#import "Mixpanel.h"

SpecBegin(RentalFlow)

describe(@"The Rental Flow,", ^{
    __block id mockMixpanel;
    __block id mockMixpanelClass;
    __block id mockRestApiClient;
    __block id mockRestApiClientClass;
    __block id mockPromptSetHelperClass;
    __block id mockNetworkingController;
    __block id mockNetworkingControllerClass;
    __block id mockLocationManagerClass;
    __block CLLocation *location = [[CLLocation alloc] initWithLatitude:38.908912 longitude:-77.030923];
    beforeAll(^{
        [tester waitForTappableViewWithAccessibilityLabel:@"Dismiss"];
        [tester tapViewWithAccessibilityLabel:@"Dismiss"];
        [tester tapViewWithAccessibilityLabel:@"Dismiss"];
        __block NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
        [AuthenticationHelper authenticateUser];
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        mockRestApiClient = OCMClassMock([RestApiClient class]);
        mockRestApiClientClass = OCMClassMock([RestApiClient class]);
        mockPromptSetHelperClass = OCMClassMock([PromptSetHelper class]);
        mockNetworkingController = OCMClassMock([NetworkingController class]);
        mockNetworkingControllerClass = OCMClassMock([NetworkingController class]);
        mockLocationManagerClass = OCMClassMock([LocationManager class]);
        void (^marketsMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"marketsData"]);
        };
        void (^emptyResultMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"emptyResult"]);
        };
        void (^createReservationMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            NSTimeInterval fifteenMins = 15 * 60;
            NSDate *reservationStart = [[NSDate alloc] init];
            NSDate *reservationExpires = [reservationStart dateByAddingTimeInterval:fifteenMins];
            NSDictionary *fixtureDict = [FixtureHelper jsonFixture:@"reservationData"];
            NSMutableDictionary *reservationDict = [[NSMutableDictionary alloc] initWithDictionary:fixtureDict];
            [reservationDict setObject:[dateFormatter stringFromDate:reservationStart]
                                forKey:@"started_at"];
            [reservationDict setObject:[dateFormatter stringFromDate:reservationExpires]
                                forKey:@"expires_at"];
            [invocation getArgument:&successBlock atIndex:4];
            successBlock(reservationDict);
        };
        void (^cancelReservationMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            NSDate *reservationCanceled = [[NSDate alloc] init];
            NSDictionary *fixtureDict = [FixtureHelper jsonFixture:@"reservationData"];
            NSMutableDictionary *reservationDict = [[NSMutableDictionary alloc] initWithDictionary:fixtureDict];
            [reservationDict setObject:[dateFormatter stringFromDate:reservationCanceled]
                                forKey:@"canceled_at"];
            [invocation getArgument:&successBlock atIndex:5];
            successBlock(reservationDict);
        };
        void (^createLockEventMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            NSDictionary *fixtureDict = [FixtureHelper jsonFixture:@"lockEventData"];
            [invocation getArgument:&successBlock atIndex:4];
            successBlock(fixtureDict);
        };
        void (^withinMarketBoundsMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock(@{@"within_market": @YES});
        };
        void (^endSessionMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:5];
            successBlock(@{});
        };
        void (^postFeedbackMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:3];
            successBlock();
        };

        OCMStub([mockLocationManagerClass currentLocation]).andReturn(location);

        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/sessions/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/reservations/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:@"markets/"
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(marketsMock);
        OCMStub([mockRestApiClient create:@"reservations/"
                                   params:[OCMArg any]
                                  success:[OCMArg any]
                                    error:[OCMArg any]]).andDo(createReservationMock);
        OCMStub([mockRestApiClient update:@"reservations/"
                                     uuid:[OCMArg any]
                                   params:[OCMArg any]
                                  success:[OCMArg any]
                                    error:[OCMArg any]]).andDo(cancelReservationMock);
        OCMStub([mockRestApiClient create:@"lock_events/"
                                   params:[OCMArg any]
                                  success:[OCMArg any]
                                    error:[OCMArg any]]).andDo(createLockEventMock);
        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"markets/[\\w\\d\\-]+/contains/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(withinMarketBoundsMock);
        OCMStub([mockRestApiClient update:@"sessions/"
                                     uuid:[OCMArg any]
                                   params:[OCMArg any]
                                  success:[OCMArg any]
                                    error:[OCMArg any]]).andDo(endSessionMock);
        OCMStub([mockNetworkingController postFeedback:[OCMArg any]
                                           withSuccess:[OCMArg any]
                                               failure:[OCMArg any]]).andDo(postFeedbackMock);
        OCMStub([mockPromptSetHelperClass getPromptsForSetNamed:[OCMArg any]]).andReturn(@{@"guid":@"1234abcd"});
        OCMStub([mockNetworkingControllerClass sharedInstance]).andReturn(mockNetworkingController);
        OCMStub([mockRestApiClientClass sharedInstance]).andReturn(mockRestApiClient);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
        [ApplicationStateHelper refreshApplicationState];
        [tester waitForTimeInterval:4.0];
    });
    beforeEach(^{
        [tester tapViewWithAccessibilityLabel:@"Center on Market"];
    });
    afterAll(^{
        [mockLocationManagerClass stopMocking];
        [mockNetworkingControllerClass stopMocking];
        [mockPromptSetHelperClass stopMocking];
        [mockRestApiClientClass stopMocking];
        [mockMixpanelClass stopMocking];
        [AuthenticationHelper unauthenticateUser];
    });
    it(@"should show the market boundaries.", ^{
        [tester waitForViewWithAccessibilityLabel:@"Market Boundaries"];
    });
    it(@"should show bike markers on the map.", ^{
        [tester waitForViewWithAccessibilityLabel:@"DC Test Bike 5"];
        [tester waitForViewWithAccessibilityLabel:@"DC Test Bike 6"];
    });

    describe(@"when a bike marker is selected,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Deselect Bike"];
        });
        it(@"should turn into a large bike marker", ^{
            [tester waitForViewWithAccessibilityLabel:@"Selected Bike: DC Test Bike 6"];
        });
        it(@"should allow the user to reserve the bike", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Reserve Bike"];
        });
        it(@"should allow the user to deselect the bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Deselect Bike"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
    });

    describe(@"when a bike is reserved,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Cancel Reservation"];
        });
        it(@"should allow the user to cancel the reservation.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Cancel Reservation"];
        });
        it(@"should allow the user to unlock the bike.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Unlock Bike"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
        it(@"should show a countdown of the time remaining.", ^{
            [tester waitForViewWithAccessibilityLabel:@"14:57 Remaining"];
            [tester waitForViewWithAccessibilityLabel:@"14:56 Remaining"];
            [tester waitForViewWithAccessibilityLabel:@"14:55 Remaining"];
        });
    });

    describe(@"when a bike reservation is canceled,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Cancel Reservation"];
        });
        it(@"should return the bike marker to the map.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"DC Test Bike 6"];
        });
    });

    describe(@"when a bike reservation expires,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            ApplicationState *appState = [ApplicationState sharedInstance];
            appState.currentReservation.expiresAt = [[NSDate new] dateByAddingTimeInterval:3];
        });
        it(@"should return the bike marker to the map.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"DC Test Bike 6"];
        });
    });

    describe(@"when a bike is unlocked,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should allow the user to lock the bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Lock Bike"];
        });
        it(@"should allow the user to release the chain.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Release Chain"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
        it(@"should display elapsed session time.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Rental Time: 0 hour 00 min"];
        });
        it(@"should display session price.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Price: $1.00"];
        });
    });

    describe(@"when a bike is unlocked and 'Hold Bike' is pressed,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should have a first step.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Step One"];
        });
        it(@"should tell the user to place the bike in a rack.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Chain bike to rack."];
        });
        it(@"should show an image of a bike in a rack.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Image of bike in rack"];
        });
        it(@"should have a second step.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Step Two"];
        });
        it(@"should tell the user to insert the chain in the lock.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Insert chain."];
        });
        it(@"should show an image of the chain inserted into the lock.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Image of chain inserted into lock"];
        });
        it(@"should have a third step.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Step Three"];
        });
        it(@"should tell the user to push the lock's knob down.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Push knob down."];
        });
        it(@"should show an image of how to push knob down.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Image of how to push knob down"];
        });
        it(@"should allow the user to lock the bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
        });
        it(@"should allow the user to go back.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"left arrow blue"];
        });
    });

    describe(@"when a rental session is held,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should allow the user to end the rental session.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"End Rental Session"];
        });
        it(@"should allow the user to resume the rental session.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Unlock Bike"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
        it(@"should display elapsed session time.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Rental Time: 0 hour 00 min"];
        });
        it(@"should display session price.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Price: $1.00"];
        });
    });

    describe(@"when a rental session becomes 2 hours old,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
            ApplicationState *appState = [ApplicationState sharedInstance];
            NSTimeInterval minus90Minutes = -90 * 60;
            NSDate *twoHoursAgo = [appState.currentSession.startedAt dateByAddingTimeInterval:minus90Minutes];
            appState.currentSession.startedAt = twoHoursAgo;
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should display elapsed session time.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Rental Time: 1 hour 30 min"];
        });
        it(@"should have a price of $2.00.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Price: $2.00"];
        });
    });

    describe(@"when a rental session is resumed,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should allow the user to lock the bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Lock Bike"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
        it(@"should display elapsed session time.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Rental Time: 0 hour 00 min"];
        });
        it(@"should display session price.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Price: $1.00"];
        });
    });

    describe(@"when a rental session is ended,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Unlock Bike"];
            [tester tapViewWithAccessibilityLabel:@"Lock Bike"];
            [tester tapViewWithAccessibilityLabel:@"LOCK + HOLD BIKE"];
            [tester tapViewWithAccessibilityLabel:@"End Rental Session"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Five Stars"];
        });
        it(@"should allow the user to rate the bike as 'One Star'.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"One Star"];
        });
        it(@"should allow the user to rate the bike as 'Two Stars'.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Two Stars"];
        });
        it(@"should allow the user to rate the bike as 'Three Stars'.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Three Stars"];
        });
        it(@"should allow the user to rate the bike as 'Four Stars'.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Four Stars"];
        });
        it(@"should allow the user to rate the bike as 'Five Stars'.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Five Stars"];
        });
    });

});

SpecEnd
