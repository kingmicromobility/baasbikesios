//
//  MockHelper.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/27/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "MockHelper.h"

@implementation MockHelper

+ (BOOL (^)(id))matchesPattern:(NSString *)patternString {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:patternString
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    return ^BOOL(id argument) {
        NSArray *matches = [regex matchesInString:argument
                                          options:0
                                            range:NSMakeRange(0, [argument length])];
        return [matches count] == 1;
    };
}

@end
