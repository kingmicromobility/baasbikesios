//
//  AuthenticationHelper.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/24/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import "AuthenticationHelper.h"
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import <CoreLocation/CoreLocation.h>
#import "NetworkingController.h"


@implementation AuthenticationHelper

+ (void)authenticateUser {
    NSString *fakeToken = [NSString stringWithFormat:@"Token %@", @"FakeJWTToken"];
    NSString *fakePersonGuid = @"50c7f1d8-cafd-4aac-8089-e6e5b06e7785";
    [[NSUserDefaults standardUserDefaults] setObject:fakeToken
                                              forKey:AUTH_TOKEN_NSUSERDEFAULTS_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:fakePersonGuid
                                              forKey:PERSON_GUID_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGGED_IN_NOTIFICATION
                                                        object:nil];

}

+ (void)unauthenticateUser {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AUTH_TOKEN_HTTP_HEADER_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PERSON_GUID_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:USER_LOGGED_OUT_NOTIFICATION
                                                        object:nil];
}

@end
