//
//  CreateAccountSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 1/1/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import <CoreLocation/CoreLocation.h>
#import "NetworkingController.h"
#import "LocationManager.h"
#import "AuthenticationHelper.h"
#import "Mixpanel.h"

SpecBegin(CreateAccount)

describe(@"Authentication,", ^{
    __block CLLocation *location = [[CLLocation alloc] initWithLatitude:38.900199 longitude:-77.027949];
    __block id keyWindow = [[UIApplication sharedApplication] keyWindow];
    __block id mockLocationManagerClass;
    __block id mockNetworkingControllerClass;
    __block id mockNetworkingController;
    __block id mockMixpanelClass;
    __block id mockMixpanel;
    beforeAll(^{
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
    });
    beforeEach(^{
        [AuthenticationHelper unauthenticateUser];
        mockNetworkingController = OCMClassMock([NetworkingController class]);
        mockNetworkingControllerClass = OCMClassMock([NetworkingController class]);
        mockLocationManagerClass = OCMClassMock([LocationManager class]);
        void (^createAccountMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:7];
            successBlock();
        };
        void (^createPaymentMethodMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:3];
            successBlock();
        };

        OCMStub([mockLocationManagerClass currentLocation]).andReturn(location);
        OCMStub([mockNetworkingController getMarketWithLocation:location.coordinate
                                                        success:[OCMArg any]
                                                        failure:[OCMArg any]]);
        OCMStub([mockNetworkingController attemptToCreateAccountWithEmail:[OCMArg any]
                                                                 password:[OCMArg any]
                                                                firstName:[OCMArg any]
                                                                 lastName:[OCMArg any]
                                                              phoneNumber:[OCMArg any]
                                                                  success:[OCMArg any]
                                                                  failure:[OCMArg any]]).andDo(createAccountMock);
        OCMStub([mockNetworkingController makeNewPaymentMethodForCurrentUserWithNonce:[OCMArg any]
                                                                              success:[OCMArg any]
                                                                              failure:[OCMArg any]]).andDo(createPaymentMethodMock);
        OCMStub([mockNetworkingControllerClass sharedInstance]).andReturn(mockNetworkingController);


        if([keyWindow accessibilityElementWithLabel:@"Allow"]) {
            [tester tapViewWithAccessibilityLabel:@"Allow"];
        }
    });
    afterEach(^{
        [mockLocationManagerClass stopMocking];
        [mockNetworkingControllerClass stopMocking];
        [mockNetworkingController stopMocking];
        [AuthenticationHelper unauthenticateUser];
    });
    afterAll(^{
        [mockMixpanelClass stopMocking];
    });

    describe(@"when accessed via the menu", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Dismiss"];
            [tester waitForTappableViewWithAccessibilityLabel:@"Map"];
        });
        it(@"should have a first name filed.", ^{
            [tester waitForViewWithAccessibilityLabel:@"First Name Field"];
        });
        it(@"should have a last name filed.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Last Name Field"];
        });
        it(@"should have an email entry field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Email Field"];
        });
        it(@"should have a password entry field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Password Field"];
        });
        it(@"should allow the user to dismiss the screen.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Dismiss"];
        });
        it(@"should allow the user to go back.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Back"];
        });
        it(@"should allow the user to submit their credentials", ^{
            [tester waitForViewWithAccessibilityLabel:@"SIGN UP"];
        });
    });

    describe(@"when the user lands on the Terms of Service screen.", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester enterText:@"Jane" intoViewWithAccessibilityLabel:@"First Name Field"];
            [tester enterText:@"Doe" intoViewWithAccessibilityLabel:@"Last Name Field"];
            [tester enterText:@"testuser@baasbikes.com" intoViewWithAccessibilityLabel:@"Email Field"];
            [tester enterText:@"baasbikes" intoViewWithAccessibilityLabel:@"Password Field"];
            [tester enterText:@"(555) 555-5555" intoViewWithAccessibilityLabel:@"Phone Number Field"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"CANCEL"];
            [tester tapViewWithAccessibilityLabel:@"Dismiss"];
            [tester waitForTappableViewWithAccessibilityLabel:@"Map"];
        });
        it(@"should have the title, 'Terms of Service'.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Terms of Service"];
        });
        it(@"should allow the user to go back.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"left arrow blue"];
        });
        it(@"should allow the user to cancel.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"CANCEL"];
        });
        it(@"should allow the user to accept.", ^{
            [tester waitForViewWithAccessibilityLabel:@"ACCEPT"];
        });
    });

    describe(@"when the user lands on the Payment Information screen,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester enterText:@"Jane" intoViewWithAccessibilityLabel:@"First Name Field"];
            [tester enterText:@"Doe" intoViewWithAccessibilityLabel:@"Last Name Field"];
            [tester enterText:@"testuser@baasbikes.com" intoViewWithAccessibilityLabel:@"Email Field"];
            [tester enterText:@"baasbikes" intoViewWithAccessibilityLabel:@"Password Field"];
            [tester enterText:@"(555) 555-5555" intoViewWithAccessibilityLabel:@"Phone Number Field"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester tapViewWithAccessibilityLabel:@"ACCEPT"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Cancel"];
            [tester waitForTappableViewWithAccessibilityLabel:@"Map"];
        });
        it(@"should allow the user to cancel.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Cancel"];
        });
        it(@"should allow the user to save their payment info.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Save"];
        });
        it(@"should have a card number field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Card Number"];
        });
        it(@"should have a month and year field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"MM/YY"];
        });
        it(@"should have a month and year field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"CVV"];
        });
    });

    describe(@"when the user completes the account creation process successfully,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester enterText:@"Jane" intoViewWithAccessibilityLabel:@"First Name Field"];
            [tester enterText:@"Doe" intoViewWithAccessibilityLabel:@"Last Name Field"];
            [tester enterText:@"testuser@baasbikes.com" intoViewWithAccessibilityLabel:@"Email Field"];
            [tester enterText:@"baasbikes" intoViewWithAccessibilityLabel:@"Password Field"];
            [tester enterText:@"(555) 555-5555" intoViewWithAccessibilityLabel:@"Phone Number Field"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester tapViewWithAccessibilityLabel:@"ACCEPT"];
            [tester enterText:@"4111111111111111" intoViewWithAccessibilityLabel:@"Card Number"];
            [tester enterText:@"11/20" intoViewWithAccessibilityLabel:@"MM/YY"];
            [tester enterText:@"123" intoViewWithAccessibilityLabel:@"CVV"];
            [tester tapViewWithAccessibilityLabel:@"Save"];
        });
        it(@"should arrive back at the map.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Map"];
        });
    });
    
});

SpecEnd
