//
//  FixtureHelper.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/26/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FixtureHelper : NSObject

+ (NSDictionary *)jsonFixture:(NSString *)filename;

@end
