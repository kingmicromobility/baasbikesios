//
//  AuthenticatedMenuSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/14/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import <CoreLocation/CoreLocation.h>
#import "NetworkingController.h"
#import "LocationManager.h"
#import "AuthenticationHelper.h"
#import "Mixpanel.h"

SpecBegin(Authentication)

describe(@"Authentication,", ^{
    __block CLLocation *location = [[CLLocation alloc] initWithLatitude:38.900199 longitude:-77.027949];
    __block id keyWindow = [[UIApplication sharedApplication] keyWindow];
    __block id mockNetworkingControllerClass;
    __block id mockNetworkingController;
    __block id mockLocationManagerClass;
    __block id mockMixpanelClass;
    __block id mockMixpanel;
    beforeAll(^{
        [AuthenticationHelper unauthenticateUser];
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
    });
    beforeEach(^{
        mockNetworkingController = OCMClassMock([NetworkingController class]);
        mockNetworkingControllerClass = OCMClassMock([NetworkingController class]);
        mockLocationManagerClass = OCMClassMock([LocationManager class]);
        void (^loginMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock();
        };

        OCMStub([mockLocationManagerClass currentLocation]).andReturn(location);
        OCMStub([mockNetworkingController getMarketWithLocation:location.coordinate
                                                        success:[OCMArg any]
                                                        failure:[OCMArg any]]);
        OCMStub([mockNetworkingController attemptToLoginWithUsername:[OCMArg any]
                                                            password:[OCMArg any]
                                                             success:[OCMArg any]
                                                             failure:[OCMArg any]]).andDo(loginMock);
        OCMStub([mockNetworkingControllerClass sharedInstance]).andReturn(mockNetworkingController);
        

        if([keyWindow accessibilityElementWithLabel:@"Allow"]) {
            [tester tapViewWithAccessibilityLabel:@"Allow"];
        }
    });
    afterEach(^{
        [mockLocationManagerClass stopMocking];
        [mockNetworkingControllerClass stopMocking];
        [mockNetworkingController stopMocking];
    });
    afterAll(^{
        [mockMixpanelClass stopMocking];
    });

    describe(@"when accessed via the menu", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"(Already have an account?)"];

        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Dismiss"];
            [tester waitForTappableViewWithAccessibilityLabel:@"Map"];
        });
        it(@"should have an email entry field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Email Field"];
        });
        it(@"should have a password entry field.", ^{
            [tester waitForViewWithAccessibilityLabel:@"Password Field"];
        });
        it(@"should allow the user to dismiss the screen.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Dismiss"];
        });
        it(@"should allow the user to go back.", ^{
            [tester waitForViewWithAccessibilityLabel:@"left arrow"];
        });
        it(@"should allow the user to submit their credentials", ^{
            [tester waitForViewWithAccessibilityLabel:@"SIGN IN"];
        });
    });

    describe(@"when accessed via the menu, credentials are provided, and SIGN IN is pressed,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP / LOG IN"];
            [tester tapViewWithAccessibilityLabel:@"(Already have an account?)"];
            [tester enterText:@"testuser@baasbikes.com" intoViewWithAccessibilityLabel:@"Email Field"];
            [tester enterText:@"baasbikes" intoViewWithAccessibilityLabel:@"Password Field"];
            [tester tapViewWithAccessibilityLabel:@"SIGN IN"];
        });
        it(@"should should bring the user back to the map.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Map"];
        });
    });

});

SpecEnd
