//
//  RideHistorySpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/13/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <KIF/KIF.h>

SpecBegin(RideHistory)

describe(@"Ride History,", ^{
    beforeEach(^{
        [tester tapViewWithAccessibilityLabel:@"hamburger copy"];
    });
    afterEach(^{
        [tester tapViewWithAccessibilityLabel:@"Ride A Bike"];
    });
    it(@"should be found under the main menu.", ^{
        [tester waitForTappableViewWithAccessibilityLabel:@"Ride History"];
    });

    describe(@"when opened", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"Ride History"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Close"];
        });
        it(@"should list the 10 most recent rides.", ^{
            NSLog(@"Hey, we tried.");
        });
    });

});

SpecEnd
