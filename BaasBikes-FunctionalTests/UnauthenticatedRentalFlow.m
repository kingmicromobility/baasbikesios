//
//  UnauthenticatedRentalFlow.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 1/1/16.
//  Copyright © 2016 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <OCMock/OCMock.h>
#import <KIF/KIF.h>
#import "ApplicationStateHelper.h"
#import "LocationManager.h"
#import "AuthenticationHelper.h"
#import "NetworkingController.h"
#import "PromptSetHelper.h"
#import "FixtureHelper.h"
#import "RestApiClient.h"
#import "AppDelegate.h"
#import "MockHelper.h"
#import "Mixpanel.h"

SpecBegin(UnauthenticatedRentalFlow)

describe(@"The Unauthenticated Rental Flow,", ^{
    __block id mockMixpanel;
    __block id mockMixpanelClass;
    __block id mockRestApiClient;
    __block id mockRestApiClientClass;
    __block id mockPromptSetHelperClass;
    __block id mockNetworkingController;
    __block id mockNetworkingControllerClass;
    __block id mockLocationManagerClass;
    __block CLLocation *location = [[CLLocation alloc] initWithLatitude:38.908912 longitude:-77.030923];
    beforeAll(^{
//        [tester waitForTappableViewWithAccessibilityLabel:@"Dismiss"];
//        [tester tapViewWithAccessibilityLabel:@"Dismiss"];
//        [tester tapViewWithAccessibilityLabel:@"Dismiss"];
        __block NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"];
        [AuthenticationHelper unauthenticateUser];
        mockMixpanel = OCMClassMock([Mixpanel class]);
        mockMixpanelClass = OCMClassMock([Mixpanel class]);
        mockRestApiClient = OCMClassMock([RestApiClient class]);
        mockRestApiClientClass = OCMClassMock([RestApiClient class]);
        mockPromptSetHelperClass = OCMClassMock([PromptSetHelper class]);
        mockNetworkingController = OCMClassMock([NetworkingController class]);
        mockNetworkingControllerClass = OCMClassMock([NetworkingController class]);
        mockLocationManagerClass = OCMClassMock([LocationManager class]);
        void (^marketsMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"marketsData"]);
        };
        void (^emptyResultMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:4];
            successBlock([FixtureHelper jsonFixture:@"emptyResult"]);
        };
        void (^createReservationMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            [[NSNotificationCenter defaultCenter] postNotificationName:USER_NEEDS_AUTH_NOTIFICATION
                                                                object:nil];
        };
        void (^createAccountMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:7];
            successBlock();
        };
        void (^createPaymentMethodMock)(NSInvocation *) = ^(NSInvocation *invocation) {
            void (^successBlock)() = nil;
            [invocation getArgument:&successBlock atIndex:3];
            successBlock();
        };

        OCMStub([mockLocationManagerClass currentLocation]).andReturn(location);

        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/sessions/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:[OCMArg checkWithBlock:[MockHelper matchesPattern:@"people/[\\w\\d\\-]+/reservations/"]]
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(emptyResultMock);
        OCMStub([mockRestApiClient list:@"markets/"
                                 params:[OCMArg any]
                                success:[OCMArg any]
                                  error:[OCMArg any]]).andDo(marketsMock);
        OCMStub([mockRestApiClient create:@"reservations/"
                                   params:[OCMArg any]
                                  success:[OCMArg any]
                                    error:[OCMArg any]]).andDo(createReservationMock);
        OCMStub([mockNetworkingController attemptToCreateAccountWithEmail:[OCMArg any]
                                                                 password:[OCMArg any]
                                                                firstName:[OCMArg any]
                                                                 lastName:[OCMArg any]
                                                              phoneNumber:[OCMArg any]
                                                                  success:[OCMArg any]
                                                                  failure:[OCMArg any]]).andDo(createAccountMock);
        OCMStub([mockNetworkingController makeNewPaymentMethodForCurrentUserWithNonce:[OCMArg any]
                                                                              success:[OCMArg any]
                                                                              failure:[OCMArg any]]).andDo(createPaymentMethodMock);
        OCMStub([mockPromptSetHelperClass getPromptsForSetNamed:[OCMArg any]]).andReturn(@{@"guid":@"1234abcd"});
        OCMStub([mockNetworkingControllerClass sharedInstance]).andReturn(mockNetworkingController);
        OCMStub([mockRestApiClientClass sharedInstance]).andReturn(mockRestApiClient);
        OCMStub([mockMixpanelClass sharedInstance]).andReturn(mockMixpanel);
        [ApplicationStateHelper refreshApplicationState];
        [tester waitForTimeInterval:2.0];
    });
    beforeEach(^{
        [tester tapViewWithAccessibilityLabel:@"Center on Market"];
    });
    afterAll(^{
        [mockLocationManagerClass stopMocking];
        [mockNetworkingControllerClass stopMocking];
        [mockPromptSetHelperClass stopMocking];
        [mockRestApiClientClass stopMocking];
        [mockMixpanelClass stopMocking];
        [AuthenticationHelper unauthenticateUser];
    });
    it(@"should show the market boundaries.", ^{
        [tester waitForViewWithAccessibilityLabel:@"Market Boundaries"];
    });
    it(@"should show bike markers on the map.", ^{
        [tester waitForViewWithAccessibilityLabel:@"DC Test Bike 5"];
        [tester waitForViewWithAccessibilityLabel:@"DC Test Bike 6"];
    });

    describe(@"when a bike marker is selected,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
        });
        afterEach(^{
            [tester tapViewWithAccessibilityLabel:@"Deselect Bike"];
        });
        it(@"should turn into a large bike marker", ^{
            [tester waitForViewWithAccessibilityLabel:@"Selected Bike: DC Test Bike 6"];
        });
        it(@"should allow the user to reserve the bike", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Reserve Bike"];
        });
        it(@"should allow the user to deselect the bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Deselect Bike"];
        });
        it(@"should allow the user to see bike information.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Bike information"];
        });
        it(@"should allow the user to report a problem.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Report a problem"];
        });
    });

    describe(@"when a user creates an account and returns to the map,", ^{
        beforeEach(^{
            [tester tapViewWithAccessibilityLabel:@"DC Test Bike 6"];
            [tester tapViewWithAccessibilityLabel:@"Reserve Bike"];
            [tester tapViewWithAccessibilityLabel:@"Dismiss"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester enterText:@"Jane" intoViewWithAccessibilityLabel:@"First Name Field"];
            [tester enterText:@"Doe" intoViewWithAccessibilityLabel:@"Last Name Field"];
            [tester enterText:@"testuser@baasbikes.com" intoViewWithAccessibilityLabel:@"Email Field"];
            [tester enterText:@"baasbikes" intoViewWithAccessibilityLabel:@"Password Field"];
            [tester enterText:@"(555) 555-5555" intoViewWithAccessibilityLabel:@"Phone Number Field"];
            [tester tapViewWithAccessibilityLabel:@"SIGN UP"];
            [tester tapViewWithAccessibilityLabel:@"ACCEPT"];
            [tester enterText:@"4111111111111111" intoViewWithAccessibilityLabel:@"Card Number"];
            [tester enterText:@"11/20" intoViewWithAccessibilityLabel:@"MM/YY"];
            [tester tapViewWithAccessibilityLabel:@"Save"];
        });
        it(@"should allow the user to reserve a bike.", ^{
            [tester waitForTappableViewWithAccessibilityLabel:@"Reserve Bike"];
        });
    });
});

SpecEnd
