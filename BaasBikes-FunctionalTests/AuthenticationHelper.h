//
//  AuthenticationHelper.h
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/24/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthenticationHelper : NSObject

+ (void)authenticateUser;

+ (void)unauthenticateUser;

@end
