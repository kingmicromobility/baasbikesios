//
//  MarketSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "Market.h"

SpecBegin(Market)

describe(@"The Market Class,", ^{

    describe(@"when provided with a location,", ^{
        __block CLLocationCoordinate2D location;
        beforeEach(^{
            location = CLLocationCoordinate2DMake(38.908912, -77.030923);
        });
        it(@"returns the nearest market", ^{
//            expect([Market nearest:location]).to.beAnInstanceOf([Market class]);
        });

    });
});

describe(@"A Market instance,", ^{

});

SpecEnd
