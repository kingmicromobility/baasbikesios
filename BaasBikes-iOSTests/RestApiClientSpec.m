//
//  RestApiClientSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "RestApiClient.h"

SpecBegin(RestApiClient)

describe(@"The RestApiClient Class,", ^{

    describe(@"when obtained as a singleton,", ^{
        __block RestApiClient *apiClient;
        beforeEach(^{
            apiClient = [RestApiClient sharedInstance];
        });
        it(@"should always be the same instance.", ^{
            expect(apiClient).to.beIdenticalTo([RestApiClient sharedInstance]);
        });

        describe(@"and the list method is invoked with a valid request.", ^{
            __block NSString *uri;
            __block NSDictionary *params;
            __block NSDictionary *listDict;
            __block NSError *listError;
            __block id mockManager;
            __block id partialApiClient;
            beforeEach(^{
                mockManager = OCMClassMock([AFHTTPRequestOperationManager class]);
                void (^fakeSuccess)(NSInvocation *) = ^void(NSInvocation *invocation) {
                    void (^successBlock)(NSDictionary *);
                    id dict = (@{@"one":@"valueOne", @"two":@"valueTwo"});
                    [invocation getArgument:&successBlock atIndex:4];
                    successBlock(dict);
                };
                OCMStub([mockManager GET:[OCMArg any]
                              parameters:[OCMArg any]
                                 success:[OCMArg any]
                                 failure:[OCMArg any]]).andDo(fakeSuccess);
                partialApiClient = OCMPartialMock(apiClient);
                OCMStub([partialApiClient getRequestManager]).andReturn(mockManager);

                uri = @"resource/";
                params = @{@"paramOne": @"valueOne", @"paramTwo": @"valueTwo"};
                [apiClient list:uri
                         params:params
                        success:^(NSDictionary *results){listDict = results;}
                          error:^(NSError *error){listError = error;}];
            });
            afterEach(^{
                [partialApiClient stopMocking];
                [NSThread sleepForTimeInterval:5.0f];
            });
            it(@"should use AFNetworking to make a GET request with the right URL.", ^{
                OCMVerify([mockManager GET:[OCMArg any]
                                parameters:[OCMArg any]
                                   success:[OCMArg any]
                                   failure:[OCMArg any]]);
            });
            it(@"should call the success block with an NSDictionary.", ^{
                expect(listDict[@"one"]).to.equal(@"valueOne");
                expect(listDict[@"two"]).to.equal(@"valueTwo");
            });
        });

        describe(@"and the list method is invoked with a problematic request.", ^{
            __block NSString *uri;
            __block NSDictionary *params;
            __block NSDictionary *listDict;
            __block NSError *listError;
            __block id mockManager;
            __block id partialApiClient;
            beforeEach(^{
                mockManager = OCMClassMock([AFHTTPRequestOperationManager class]);
                void (^fakeFailure)(NSInvocation *) = ^void(NSInvocation *invocation){
                    void (^failBlock)(NSDictionary *dict);
                    id error = [[NSError alloc] initWithDomain:@"HTTP" code:404 userInfo:nil];
                    [invocation getArgument:&failBlock atIndex:5];
                    failBlock(error);
                };
                OCMStub([mockManager GET:[OCMArg any]
                              parameters:[OCMArg any]
                                 success:[OCMArg any]
                                 failure:[OCMArg any]]).andDo(fakeFailure);
                partialApiClient = OCMPartialMock(apiClient);
                OCMStub([partialApiClient getRequestManager]).andReturn(mockManager);

                uri = @"http://my.test.domain/";
                params = @{@"paramOne": @"valueOne", @"paramTwo": @"valueTwo"};
                [apiClient list:uri
                         params:params
                        success:^(NSDictionary *results){listDict = results;}
                          error:^(NSError *error){listError = error;}];
            });
            afterEach(^{
                [partialApiClient stopMocking];
                [NSThread sleepForTimeInterval:5.0f];
            });
            it(@"should use AFNetworking to make a GET request with the right URL.", ^{
                OCMVerify([mockManager GET:[OCMArg any]
                                parameters:[OCMArg any]
                                   success:[OCMArg any]
                                   failure:[OCMArg any]]);
            });
            it(@"should call the error block with an NSError.", ^{
                expect(listError.domain).to.equal(@"HTTP");
            });
        });
    });
});

SpecEnd
