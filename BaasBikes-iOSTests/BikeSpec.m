//
//  BikeSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/15/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "Lock.h"
#import "Bike.h"

SpecBegin(Bike)

describe(@"The Bike class,", ^{

});

describe(@"A Bike instance,", ^{

    describe(@"when initialized with a complete dictionary and market,", ^{
        __block Bike *bike;
        __block NSDictionary *bikeDictionary;
        __block id market;
        beforeEach(^{
            market = OCMClassMock([Market class]);
            bikeDictionary = @{
                               @"guid":      @"dfe28600-30d2-4f31-b85f-b423c89c694e",
                               @"name":      @"Test Bike",
                               @"status":    @"available",
                               @"image_uri": @"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",
                               @"last_lat":  @25.7233532,
                               @"last_lon":  @-80.2525847,
                               @"brand":     @{@"name":      @"Priority"},
                               @"style":     @{@"name":      @"Cruiser"},
                               @"size":      @{@"name":      @"5'8\" - 6'2\""},
                               @"detail_set":@{@"detailOne": @"valueOne",
                                               @"detailTwo": @"valueTwo"},
                               @"lock":      @{@"urn":       @"baas.lock.fake.1"}
                               };
            bike = [[Bike alloc] initWithDictionary:bikeDictionary
                                          andMarket:market];
        });
        it(@"should have a guid.", ^{
            expect(bike.guid).to.equal([[NSUUID alloc] initWithUUIDString:bikeDictionary[@"guid"]]);
        });
        it(@"should have a market.", ^{
            expect(bike.market).to.beIdenticalTo(market);
        });
        it(@"should have a name.", ^{
            expect(bike.name).to.equal(bikeDictionary[@"name"]);
        });
        it(@"should have a brand.", ^{
            expect(bike.brand).to.equal(bikeDictionary[@"brand"][@"name"]);
        });
        it(@"should have a style.", ^{
            expect(bike.style).to.equal(bikeDictionary[@"style"][@"name"]);
        });
        it(@"should have a size.", ^{
            expect(bike.size).to.equal(bikeDictionary[@"size"][@"name"]);
        });
        it(@"should have a status.", ^{
            expect(bike.status).to.equal(bikeDictionary[@"status"]);
        });
        it(@"should have details.", ^{
            expect(bike.details).to.equal(bikeDictionary[@"detail_set"]);
        });
        it(@"should have an image.", ^{
            NSData *image = [NSData dataWithContentsOfURL:[NSURL URLWithString:bikeDictionary[@"image_uri"]]];
            expect(bike.image).to.equal(image);
        });
        it(@"should have a bikeLock.", ^{
            expect(bike.bikeLock).to.beAKindOf([Lock class]);
        });
        it(@"should have a lastLocation.", ^{
            CLLocationCoordinate2D location =
            CLLocationCoordinate2DMake([bikeDictionary[@"last_lat"] floatValue],
                                       [bikeDictionary[@"last_lon"] floatValue]);
            expect(bike.lastLocation).to.equal(location);
        });
    });

    describe(@"when initialized with an incomplete dictionary and no market,", ^{
        __block Bike *bike;
        beforeEach(^{
            bike = [[Bike alloc] initWithDictionary:@{} andMarket:nil];
        });
        it(@"should have a nil guid.", ^{
            expect(bike.guid).to.beNil;
        });
        it(@"should have a nil market.", ^{
            expect(bike.market).to.beNil;
        });
        it(@"should have a nil name.", ^{
            expect(bike.name).to.beNil;
        });
        it(@"should have a nil brand.", ^{
            expect(bike.brand).to.beNil;
        });
        it(@"should have a nil style.", ^{
            expect(bike.style).to.beNil;
        });
        it(@"should have a nil size.", ^{
            expect(bike.size).to.beNil;
        });
        it(@"should have a nil status.", ^{
            expect(bike.status).to.beNil;
        });
        it(@"should have nil details.", ^{
            expect(bike.details).to.beNil;
        });
        it(@"should have a nil image.", ^{
            expect(bike.image).to.beNil;
        });
        it(@"should have a nil bikeLock.", ^{
            expect(bike.bikeLock).to.beNil;
        });
        it(@"should have a nil lastLocation.", ^{
            expect(bike.lastLocation).to.beNil;
        });
    });
});

SpecEnd