//
//  FakeLockSpec.m
//  FakeLockSpec
//
//  Created by Justin Molineaux on 12/10/15.
//  Copyright (c) 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "FakeLock.h"

SpecBegin(FakeLock)

describe(@"FakeLock,", ^{
    it(@"should be a subclass of Lock.", ^{
        expect([FakeLock class]).to.beSubclassOf([Lock class]);
    });

    describe(@"when initialized with a lockid and a LockDelegate,", ^{
        __block id<LockDelegate> mockLockDelegate;
        __block FakeLock *fakeLock;
        beforeEach(^{
            mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
            fakeLock = [[FakeLock alloc] initWithBaasLockId:@"baas-A1:B2:C3"
                                               withDelegate:mockLockDelegate];
        });
        it(@"should have an initial state of disconnected.", ^{
            expect([fakeLock isConnected]).to.beFalsy;
        });
        it(@"should have an initial state of incompatible.", ^{
            expect([fakeLock isCompatible]).to.beFalsy;
        });
        it(@"should have an initial state of unlocked.", ^{
            expect([fakeLock isLocked]).to.beFalsy;
        });

        describe(@"and connect is invoked,", ^{
            beforeEach(^{
                [fakeLock connect];
            });
            it(@"should appear as though it's connected.", ^{
                expect([fakeLock isConnected]).to.beTruthy;
            });
            it(@"should call didConnect on lockDelegate.", ^{
                OCMVerify([mockLockDelegate didConnect:fakeLock]);
            });
        });

        describe(@"and confirmCompatibility is invoked,", ^{
            beforeEach( ^{
                [fakeLock confirmCompatibility];
            });
            it(@"should appear as though it's compatible.", ^{
                expect([fakeLock isCompatible]).to.beTruthy;
            });
            it(@"should call didConfirmCompatibility on lockDelegate.", ^{
                OCMVerify([mockLockDelegate didConfirmCompatibility:fakeLock]);
            });
        });

        describe(@"and disconnect is invoked,", ^{
            beforeEach(^{
                [fakeLock connect];
                [fakeLock disconnect];
            });
            it(@"should appear as though it's disconnected.", ^{
                expect([fakeLock isConnected]).to.beFalsy;
            });
            it(@"should call didDisconnect on lockDelegate.", ^{
                OCMVerify([mockLockDelegate didDisconnect:fakeLock]);
            });
        });

        describe(@"and unlocked is invoked,", ^{
            beforeEach(^{
                [fakeLock unlock];
            });
            it(@"should appear as though it's unlocked.", ^{
                expect([fakeLock isLocked]).to.beFalsy;
            });
            it(@"should call didUnlock on lockDelegate.", ^{
                OCMVerify([mockLockDelegate didUnlock:fakeLock]);
            });
        });

        describe(@"and locked is invoked,", ^{
            beforeEach(^{
                [fakeLock lock];
            });
            it(@"should appear as though it's locked", ^{
                expect([fakeLock isLocked]).to.beTruthy;
            });
            it(@"should call didLock on lockDelegate.", ^{
                OCMVerify([mockLockDelegate didLock:fakeLock]);
            });
        });

    });

    describe(@"when initialized with a lockid and no delegate,", ^{
        __block FakeLock *fakeLock;
        beforeEach(^{
            fakeLock = [[FakeLock alloc] initWithBaasLockId:@"baas-A1:B2:C3"
                                               withDelegate:nil];
        });
        it(@"should have an initial state of disconnected.", ^{
            expect([fakeLock isConnected]).to.beFalsy;
        });
        it(@"should have an initial state of incompatible.", ^{
            expect([fakeLock isCompatible]).to.beFalsy;
        });
        it(@"should have an initial state of unlocked.", ^{
            expect([fakeLock isLocked]).to.beFalsy;
        });

        describe(@"and connect is invoked,", ^{
            beforeEach(^{
                [fakeLock connect];
            });
            it(@"should appear as though it's connected.", ^{
                expect([fakeLock isConnected]).to.beTruthy;
            });
        });

        describe(@"and confirmCompatibility is invoked,", ^{
            beforeEach( ^{
                [fakeLock confirmCompatibility];
            });
            it(@"should appear as though it's compatible.", ^{
                expect([fakeLock isCompatible]).to.beTruthy;
            });
        });

        describe(@"and disconnect is invoked,", ^{
            beforeEach(^{
                [fakeLock connect];
                [fakeLock disconnect];
            });
            it(@"should appear as though it's disconnected.", ^{
                expect([fakeLock isConnected]).to.beFalsy;
            });
        });

        describe(@"and unlocked is invoked,", ^{
            beforeEach(^{
                [fakeLock unlock];
            });
            it(@"should appear as though it's unlocked", ^{
                expect([fakeLock isLocked]).to.beFalsy;
            });
        });

        describe(@"and locked is invoked,", ^{
            beforeEach(^{
                [fakeLock lock];
            });
            it(@"should appear as though it's locked", ^{
                expect([fakeLock isLocked]).to.beTruthy;
            });
        });

    });

});

SpecEnd
