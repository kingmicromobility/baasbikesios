//
//  DutchRetrofitLockSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/11/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "BluetoothCentralManager.h"
#import "Lock.h"
#import "DutchRetrofitLock.h"

SpecBegin(DutchRetrofitLock)

describe(@"DutchRetrofitLock,", ^{
    it(@"should be a subclass of Lock.", ^{
        expect([DutchRetrofitLock class]).to.beSubclassOf([Lock class]);
    });

    describe(@"when initialized with a lockid and a LockDelegate,", ^{
        __block id<BluetoothCentralManagerDelegate> mockBtCentralManagerDelegate;
        __block id<LockDelegate> mockLockDelegate;
        __block id mockBtCentralManagerClass;
        __block id mockBtCentralManager;
        __block DutchRetrofitLock *dutchRetrofitLock;
        beforeEach(^{
            mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
            mockBtCentralManager = OCMClassMock([BluetoothCentralManager class]);
            mockBtCentralManagerClass = OCMClassMock([BluetoothCentralManager class]);
            mockBtCentralManagerDelegate = OCMProtocolMock(@protocol(BluetoothCentralManagerDelegate));

            OCMStub([mockBtCentralManagerClass alloc]).andReturn(mockBtCentralManager);
            OCMStub([mockBtCentralManager initWithDelegate:[OCMArg isNotNil]]).andReturn(mockBtCentralManager);

            dutchRetrofitLock = [[DutchRetrofitLock alloc] initWithBaasLockId:@"baas-A1:B2:C3"
                                                                 withDelegate:mockLockDelegate];
        });
        afterEach(^{
            [mockBtCentralManagerClass stopMocking];
            [mockBtCentralManager stopMocking];
        });
        it(@"should have an initial state of disconnected.", ^{
            expect([dutchRetrofitLock isConnected]).to.beFalsy;
        });
        it(@"should have an initial state of incompatible.", ^{
            expect([dutchRetrofitLock isCompatible]).to.beFalsy;
        });
        it(@"should have an initial state of unlocked.", ^{
            expect([dutchRetrofitLock isLocked]).to.beFalsy;
        });

        describe(@"and connect is invoked,", ^{
            beforeEach(^{
                [dutchRetrofitLock connect];
            });
            it(@"should begin searching for a bluetooth peripheral.", ^{
                OCMVerify([mockBtCentralManager beginSearchingForLockPeripheral:[OCMArg isNotNil]
                                                                    withService:[OCMArg isNotNil]]);
            });
        });

        describe(@"and a lock peripheral has been discovered,", ^{
            __block id mockPeripheral;
            beforeEach(^{
                mockPeripheral = OCMClassMock([CBPeripheral class]);
                [dutchRetrofitLock didFindLockPeripheral:mockPeripheral];
            });
            it(@"should begin connecting to the discovered lock.", ^{
                OCMVerify([mockBtCentralManager connectLockPeripheral:mockPeripheral]);
            });
        });

        pending(@"and a lock peripheral was not discovered,");

        describe(@"and a lock peripheral has been connected,", ^{
            __block id mockPeripheral;
            beforeEach(^{
                mockPeripheral = OCMClassMock([CBPeripheral class]);
                dutchRetrofitLock = OCMPartialMock(dutchRetrofitLock);
                OCMStub([dutchRetrofitLock confirmCompatibility]);
                [dutchRetrofitLock didConnectLockPeripheral:mockPeripheral];
            });
            it(@"should have a new state of connected.", ^{
                expect([dutchRetrofitLock isConnected]).to.beTruthy;
            });
            it(@"should invoke didConnect on the lockDelegate.", ^{
                OCMVerify([mockLockDelegate didConnect:[OCMArg isNotNil]]);
            });
            it(@"should begin confirming compatibility.", ^{
                OCMVerify([dutchRetrofitLock confirmCompatibility]);
            });

            describe(@"and then unlock is invoked,", ^{
                beforeEach(^{
                    dutchRetrofitLock.transmit = OCMClassMock([CBCharacteristic class]);
                    [dutchRetrofitLock unlock];
                });
                it(@"should have a desiredState of 'unlocked'.", ^{
                    expect(dutchRetrofitLock.desiredState).to.equal(@"unlocked");
                });
                it(@"should write a command to the bluetooth perpheral.", ^{
                    OCMVerify([mockPeripheral writeValue:[OCMArg isNotNil]
                                       forCharacteristic:[OCMArg isNotNil]
                                                    type:CBCharacteristicWriteWithResponse]);
                });

                describe(@"and the lock reports its state as 'unlocked'", ^{
                    beforeEach(^{
                        dutchRetrofitLock.state = @"unlocked";
                        [dutchRetrofitLock invokeDelegateAfterAttemptedStateChange];
                    });
                    it(@"should assume the unlocked state.", ^{
                        expect([dutchRetrofitLock isLocked]).to.beFalsy;
                    });
                    it(@"should invoke didUnlock on the lockDelegate.", ^{
                        OCMVerify([mockLockDelegate didUnlock:[OCMArg isNotNil]]);
                    });
                });
            });

            describe(@"and then lock is invoked,", ^{
                beforeEach(^{
                    dutchRetrofitLock.transmit = OCMClassMock([CBCharacteristic class]);
                    [dutchRetrofitLock lock];
                });
                it(@"should have a desiredState of 'locked'.", ^{
                    expect(dutchRetrofitLock.desiredState).to.equal(@"locked");
                });
                it(@"should write a command to the bluetooth perpheral.", ^{
                    OCMVerify([mockPeripheral writeValue:[OCMArg isNotNil]
                                       forCharacteristic:[OCMArg isNotNil]
                                                    type:CBCharacteristicWriteWithResponse]);
                });

                describe(@"and the lock reports its state as 'locked'", ^{
                    beforeEach(^{
                        dutchRetrofitLock.state = @"locked";
                        [dutchRetrofitLock invokeDelegateAfterAttemptedStateChange];
                    });
                    it(@"should assume the locked state.", ^{
                        expect([dutchRetrofitLock isLocked]).to.beTruthy;
                    });
                    it(@"should invoke didLock on the lockDelegate.", ^{
                        OCMVerify([mockLockDelegate didLock:[OCMArg isNotNil]]);
                    });
                });
            });

            describe(@"and then disconnect is invoked,", ^{
                beforeEach(^{
                    [dutchRetrofitLock disconnect];
                });
                it(@"should disconnect from the bluetooth peripheral.", ^{
                    [mockBtCentralManager disconnectLockPeripheral:mockPeripheral];
                });
            });

            describe(@"and then didDisconnect is invoked,", ^{
                beforeEach(^{
                    [dutchRetrofitLock didDisconnectLockPeripheral:mockPeripheral];
                });
                it(@"should assume the disconnected state.", ^{
                    expect([dutchRetrofitLock isConnected]).to.beFalsy;
                });
                it(@"should invoke didDisconnect on the lockDelegate.", ^{
                    OCMVerify([mockLockDelegate didDisconnect:[OCMArg isNotNil]]);
                });
            });
        });

        pending(@"and didNotConnectToLockPeripheral is invoked,");

        describe(@"and confirmCompatibility is invoked,", ^{
            __block id mockPeripheral;
            beforeEach(^{
                mockPeripheral = OCMClassMock([CBPeripheral class]);
                [dutchRetrofitLock didConnectLockPeripheral:mockPeripheral];
                [dutchRetrofitLock confirmCompatibility];
            });
            it(@"should begin discovering the peripheral's services.", ^{
                OCMVerify([mockPeripheral discoverServices:nil]);
            });
        });

        pending(@"and didConfirmIncompatibility,");

        describe(@"and confirmCompatibility is called without a peripheral,", ^{
            beforeEach(^{
                [dutchRetrofitLock confirmCompatibility];
            });
            it(@"should not assume the compatible state.", ^{
                expect([dutchRetrofitLock isCompatible]).to.beFalsy;
            });
        });
    });
});

SpecEnd
