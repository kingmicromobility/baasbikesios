//
//  LockManagerSpec.m
//  BaasBikes-iOS
//
//  Created by Justin Molineaux on 12/11/15.
//  Copyright © 2015 Baas, Inc. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>
#import "Lock.h"
#import "FakeLock.h"
#import "DutchRetrofitLock.h"
#import "LockManager.h"

SpecBegin(LockManager)

describe(@"LockManager", ^{

    describe(@"when obtained as a singleton,", ^{
        __block LockManager *lockManager;
        beforeEach(^{
            lockManager = [LockManager sharedInstance];
        });
        it(@"should always be the same instance.", ^{
            expect(lockManager).to.beIdenticalTo([LockManager sharedInstance]);
        });

        describe(@"and getLockByURN:... is invoked for a FakeLock,", ^{
            __block id<LockDelegate> mockLockDelegate;
            __block Lock *lock;
            beforeEach(^{
                mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
                lock = [lockManager getLockByURN:@"baas.lock.fake.1"
                                    withDelegate:mockLockDelegate];
            });
            it(@"should return a subtype of Lock.", ^{
                expect(lock).to.beKindOf([Lock class]);
            });
            it(@"should return an instance of FakeLock.", ^{
                expect(lock).to.beInstanceOf([FakeLock class]);
            });
            it(@"should set the lock's delegate", ^{
                expect(lock.delegate).to.beIdenticalTo(mockLockDelegate);
            });
        });

        describe(@"and getLockByURN:... is invoked for a DutchRetrofitLock,", ^{
            __block id<LockDelegate> mockLockDelegate;
            __block Lock *lock;
            beforeEach(^{
                mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
                lock = [lockManager getLockByURN:@"baas.lock.dutch-retrofit.baas-A1:B2:C3"
                                    withDelegate:mockLockDelegate];
            });
            it(@"should return a subtype of Lock.", ^{
                expect(lock).to.beKindOf([Lock class]);
            });
            it(@"should return an instance of DutchRetrofitLock.", ^{
                expect(lock).to.beInstanceOf([DutchRetrofitLock class]);
            });
            it(@"should set the lock's delegate", ^{
                expect(lock.delegate).to.beIdenticalTo(mockLockDelegate);
            });
        });

        describe(@"and getLockOfType:... is invoked for a FakeLock", ^{
            __block id<LockDelegate> mockLockDelegate;
            __block Lock *lock;
            beforeEach(^{
                mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
                lock = [lockManager getLockOfType:@"fake"
                                       withLockId:@"1"
                                     withDelegate:mockLockDelegate];
            });
            it(@"should return a subtype of Lock.", ^{
                expect(lock).to.beKindOf([Lock class]);
            });
            it(@"should return an instance of FakeLock.", ^{
                expect(lock).to.beInstanceOf([FakeLock class]);
            });
            it(@"should set the lock's delegate", ^{
                expect(lock.delegate).to.beIdenticalTo(mockLockDelegate);
            });
        });

        describe(@"and getLockOfType:... is invoked for a DutchRetrofitLock,", ^{
            __block id<LockDelegate> mockLockDelegate;
            __block Lock *lock;
            beforeEach(^{
                mockLockDelegate = OCMProtocolMock(@protocol(LockDelegate));
                lock = [lockManager getLockOfType:@"dutch-retrofit"
                                       withLockId:@"baas-A1:B2:C3"
                                     withDelegate:mockLockDelegate];
            });
            it(@"should return a subtype of Lock.", ^{
                expect(lock).to.beKindOf([Lock class]);
            });
            it(@"should return an instance of DutchRetrofitLock.", ^{
                expect(lock).to.beInstanceOf([DutchRetrofitLock class]);
            });
            it(@"should set the lock's delegate", ^{
                expect(lock.delegate).to.beIdenticalTo(mockLockDelegate);
            });
        });

    });

});

SpecEnd
